FROM odoo:14.0

COPY ./entrypoint.sh /
COPY ./odoo.conf /etc/odoo/odoo.conf.tmpl
COPY wait-for-psql.py /usr/local/bin/wait-for-psql.py

USER root
RUN chmod +x /entrypoint.sh && chmod +x /usr/local/bin/wait-for-psql.py
RUN apt update \
 && apt install git nginx -y

RUN  mkdir /odoo-addons && chown odoo:odoo /odoo-addons

# install rust required by crypto # TODO better way
RUN apt install gettext-base build-essential libssl-dev libffi-dev cargo python3-dev -y
#RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | bash -s -- -y
#ENV PATH="/root/.cargo/bin:${PATH}"


ENV CRYPTOGRAPHY_DONT_BUILD_RUST=1

# install required python libs
RUN pip3 install --upgrade pip
RUN pip3 install fintech cryptography cachetools paramiko==2.7.2 python-jose pysftp openupgradelib
#force 2.7.2 until six update server or i find why it's not working with 2.9.2 (BC change, see changelog)

# need to define an odoo.conf for tests (overriden at startup in entrypoint.sh)
RUN envsubst < /etc/odoo/odoo.conf.tmpl | tee /etc/odoo/odoo.conf

ADD  ./agepoly_modules /odoo/agepoly_modules
ADD  ./tier_modules /odoo/tier_modules
ADD  ./enable_modules.sh /

RUN /enable_modules.sh 

ADD ./nginx.conf /etc/nginx/sites-available/default

CMD ["odoo", "-i all", "-u all", "--without-demo=all"]
