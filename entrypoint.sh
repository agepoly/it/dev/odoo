#!/bin/bash

set -e

# set the postgres database host, port, user and password according to the environment
# and pass them as arguments to the odoo process if not present in the config file
: ${DB_HOST:=${DB_PORT_5432_TCP_ADDR:='db'}}
: ${DB_PORT:=${DB_PORT_5432_TCP_PORT:='5432'}}
: ${DB_USER:=${DB_ENV_POSTGRES_USER:=${POSTGRES_USER:='odoo'}}}
: ${DB_PASSWORD:=${DB_ENV_POSTGRES_PASSWORD:=${POSTGRES_PASSWORD:='odoo'}}}
: ${SINGLE_DB_NAME:=${DB_NAME}}

DB_ARGS=()
function check_config() {
    param="$1"
    value="$2"
    if grep -q -E "^\s*\b${param}\b\s*=" "$ODOO_RC" ; then
        value=$(grep -E "^\s*\b${param}\b\s*=" "$ODOO_RC" |cut -d " " -f3|sed 's/["\n\r]//g')
    fi;

    DB_ARGS+=("--${param}")
    if [[ ! -z "$2" ]]; then
       DB_ARGS+=("${value}")
    fi
}
check_config "db_host" "$DB_HOST"
check_config "db_port" "$DB_PORT"
check_config "db_user" "$DB_USER"
check_config "db_password" "$DB_PASSWORD"

if [[ ! -z "${SINGLE_DB_NAME}" ]]; then
  check_config "database" "$SINGLE_DB_NAME"
  check_config "no-database-list" ""
fi

envsubst < /etc/odoo/odoo.conf.tmpl | tee /etc/odoo/odoo.conf

nginx -g "daemon off;" &

case "$1" in
    -- | odoo)
        shift
        if [[ "$1" == "scaffold" ]] ; then
            exec odoo "$@"
        else
            wait-for-psql.py ${DB_ARGS[@]} --timeout=30
            echo "running cmd : odoo $@ ${DB_ARGS[@]}"
            exec odoo "$@" "${DB_ARGS[@]}"
        fi
        ;;
    -*)
        wait-for-psql.py ${DB_ARGS[@]} --timeout=30
        echo "running cmd : odoo $@ ${DB_ARGS[@]}"
        exec odoo "$@" "${DB_ARGS[@]}"
        ;;
    *)
        echo "running cmd : $@"
        exec "$@"
esac

exit 1
