ODOO_BASE=$(pwd)
mkdir -p $ODOO_BASE/odoo-addons
cd $ODOO_BASE/tier_modules
for d in */ ; do
  cd $ODOO_BASE/tier_modules/$d
  for e in */ ; do
    if [ "$d" != "setup/" ]; then
      if [ "$e" != "setup/" ]; then
        ln -s $ODOO_BASE/tier_modules/$d$e $ODOO_BASE/odoo-addons/
      fi
    fi
  done
done

cd $ODOO_BASE/agepoly_modules
for d in */ ; do
    ln -s $ODOO_BASE/agepoly_modules/$d $ODOO_BASE/odoo-addons/
done

cd $ODOO_BASE



