# -*- coding: utf-8 -*-
#################################################################################
# Author      : PIT Solutions AG. (<https://www.pitsolutions.ch/>)
# Copyright(c): 2019 - Present PIT Solutions AG.
# License URL : https://www.webshopextension.com/en/licence-agreement/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.webshopextension.com/en/licence-agreement/>
#################################################################################

{
    "name": "Datatrans Payment Acquirer",
    "category": "Accounting/Payment Acquirers",
    "summary": """Payment Acquirer: Datatrans Implementation""",
    "description": """Datatrans Payment Acquirer""",
    "version": '14.0.0.1.0',
    'author': 'PIT Solutions AG',
    "license": "OPL-1",
    'currency': 'EUR',
    'price': 150,
    "website": "http://www.pitsolutions.ch/en/",
    'live_test_url': 'http://saas.dev.displayme.net/demo-register/?technical_name=pits_payment_datatrans&version=14.0&access_key=A78-uWo-AKu',
    "depends": [
        'payment',
    ],
    "data": [
        'security/ir.model.access.csv',
        'views/payment_datatrans_templates.xml',
        'data/datatrans_acquirer_config_data.xml',
        'data/payment_icon_data.xml',
        'data/payment_acquirer_data.xml',
        'data/ir_cron_data.xml',
        'views/payment_acquirer_log.xml',
        'views/datatrans_acquirer_config_view.xml',
        'views/payment_views.xml',
        'views/res_currency_views.xml',
    ],
    "images": ['static/description/banner.png'],
    'post_init_hook': 'create_missing_journal_for_acquirers',
    "installable": True,
}
