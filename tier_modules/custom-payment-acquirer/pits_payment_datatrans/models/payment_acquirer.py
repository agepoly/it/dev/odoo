# -*- coding: utf-8 -*-
#################################################################################
# Author      : PIT Solutions AG. (<https://www.pitsolutions.ch/>)
# Copyright(c): 2019 - Present PIT Solutions AG.
# License URL : https://www.webshopextension.com/en/licence-agreement/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.webshopextension.com/en/licence-agreement/>
#################################################################################
import logging
import requests
import base64
import binascii
import hmac
import hashlib
from werkzeug import urls
from odoo import api, models, fields, tools, _
from odoo.addons.payment.models.payment_acquirer import ValidationError
from odoo.addons.pits_payment_datatrans.controllers.main import DatatransController
from odoo.tools.float_utils import float_round
from odoo.addons.payment.models.payment_acquirer import _partner_split_name
from xml.dom import minidom
from odoo import exceptions

_logger = logging.getLogger(__name__)


class PaymentAcquirerDatatrans(models.Model):
    _inherit = 'payment.acquirer'

    @api.depends('provider')
    def _compute_allowed_datatrans_payment_icon_ids(self):
        if self.provider == 'datatrans':
            self.allowed_datatrans_payment_icon_ids = self.env['payment.icon'].search([('in_datatrans','=',True)])
        else:
            self.allowed_datatrans_payment_icon_ids = self.env['payment.icon'].search([])

    provider = fields.Selection(selection_add=[
        ('datatrans', 'Datatrans')], ondelete={'datatrans': 'set default'})
    datatrans_config_id = fields.Many2one('datatrans.acquirer.config', string='Datatrans Config', ondelete='restrict',
                                          domain="['|', ('company_id', '=', company_id), ('company_id', '=', False)]",
                                          required_if_provider='datatrans', groups='base.group_user')
    payment_mode = fields.Selection([
        ('forceRedirect', ' Redirect Mode'),
        ('lightbox', 'Lightbox Mode'),
    ], default="forceRedirect",
        help="Select the payment mode to use in order to process this payment method")
    allowed_datatrans_payment_icon_ids = fields.Many2many('payment.icon', string='Allowed Datatrans Payment Icons',
                                                          compute='_compute_allowed_datatrans_payment_icon_ids')

    @api.model
    def get_eligible_acquirers(self, acquirers, currency_data={}):
        """Remove acquirers which do not support the currency from the acquirer listing in shop page"""
        currency_id = currency_data.get('currency_id')
        datatrans_acquirers = [acq for acq in acquirers if acq.provider == 'datatrans']
        payment_acquirers = acquirers
        for datatrans_acquirer in datatrans_acquirers:
            if len(datatrans_acquirer.payment_icon_ids) == 1:
                currency_list = datatrans_acquirer.payment_icon_ids.currency_ids.ids
                if currency_id and currency_id not in currency_list:
                    try:
                        payment_acquirers.remove(datatrans_acquirer)
                    except Exception as e:
                        try:
                            payment_acquirers -= datatrans_acquirer
                        except Exception as e:
                            pass
        return payment_acquirers

    @api.model
    def _get_datatrans_urls(self, state):
        """ Datatrans URLS """
        if state == 'enabled':
            return {
                'datatrans_form_url': urls.url_join(self.datatrans_config_id.datatrans_base_url,
                                                DatatransController._redirect_url),
            }
        else:
            return {
                'datatrans_form_url': urls.url_join(self.datatrans_config_id.datatrans_test_base_url,
                                                DatatransController._redirect_url),
            }

    def datatrans_form_generate_values(self, values):
        """Method that generates the values used to render the form button template"""
        self.ensure_one()
        base_url = self.get_base_url()
        payment_method_codes = [values[0].datatrans_code for values in self.payment_icon_ids]
        amount = int(float_round(values['amount'] * 100, 2))
        currency = self.env['res.currency'].search([('id', '=', values['currency'].id)], limit=1).name
        if self.datatrans_config_id.is_dynamic:
            string_to_sign = "%s%s%s%s" % (
                 self.datatrans_config_id.datatrans_merchantId, amount, currency,
                 values['reference'])
            sign = hmac.new(binascii.a2b_hex(
                self.datatrans_config_id.datatrans_sign.encode('ascii')), string_to_sign.encode('utf-8'),
                hashlib.sha256).hexdigest()
        else:
            sign = self.datatrans_config_id.datatrans_sign

        temp_values = {
            'merchantId': self.datatrans_config_id.datatrans_merchantId,
            'refno': values['reference'],
            'amount': amount,
            'currency': currency,
            'theme': 'DT2015',
            'sign': sign,
            'language': values['partner_lang'][:2] if values['partner_lang'] else self.datatrans_config_id.language,
            'mode': self.payment_mode,
            'paymentmethods': payment_method_codes,
            'success_url': urls.url_join(base_url, DatatransController._success_url),
            'cancel_url': urls.url_join(base_url, DatatransController._cancel_url),
            'error_url': urls.url_join(base_url, DatatransController._error_url),
            'themeConfiguration': {"brandColor": self.datatrans_config_id.brand_color,
                                   "textColor": self.datatrans_config_id.text_color,
                                   "logoType": self.datatrans_config_id.logo_type,
                                   "logoBorderColor": self.datatrans_config_id.logo_border_color,
                                   "brandButton": self.datatrans_config_id.brand_button}
        }
        values.update(temp_values)

        return values

    def datatrans_get_form_action_url(self):
        """Method returns the url of the button form"""
        return self._get_datatrans_urls(self.state)['datatrans_form_url']

    def cron_update_datatrans_state(self):
        datatrans_transactions = self.env['payment.transaction'].search(
            [('provider', '=', 'datatrans'), ('datatrans_transactionId', '!=', False),
             ('datatrans_merchantId', '!=', False)])
        for tx in datatrans_transactions:
            tx.update_datatrans_state()

    def action_datatrans_payment_acquirer_log(self):
        self.ensure_one()
        context = self.env.context.copy()
        action = self.env.ref('pits_payment_datatrans.action_datatrans_payment_acquirer_log').read()[0]
        if self.payment_icon_ids.ids:
            action['domain'] = [('payment_method', 'in', self.payment_icon_ids.ids)]
        else:
            action['domain'] = [('origin','=','datatrans')]
        return action


class PaymentTransaction(models.Model):
    _name = 'payment.transaction'
    _inherit = ['payment.transaction', 'mail.thread']

    _datatrans_valid_tx_res_code = ['01', '02', '03', '21']
    _datatrans_pending_tx_res_code = ['0', '05', '11', '12', '13', '15']
    _datatrans_cancel_tx_res_code = ['06', '07', '08', '09', '10']
    _datatrans_error_tx_res_code = ['04', '21']
    _datatrans_reject_tx_res_code = ['14', '20', '30']
    _datatrans_s2s_error_code = ['-999', '-99', '99', '-98', '-97', '97', '-89', '-100', '1001', '1002', '1003', '1004',
                                 '1006', '1007', '1008', '1009', '1010', '1012', '1400', '1402', '1403', '1404', '1407',
                                 '1411', '1412']
    _datatrans_update_transaction_success_code = [1, 2, 3, 21, 0, 5, 11, 12, 13, 15]

    datatrans_state = fields.Char(readonly=True, track_visibility='onchange')
    datatrans_acq_auth_code = fields.Char("Authorization Code (Acquirer)")
    datatrans_auth_code = fields.Char("Authorization Code")
    datatrans_pmethod = fields.Many2one('payment.icon', string='Payment Method')
    datatrans_reqtype = fields.Char("Request Type")
    datatrans_merchantId = fields.Char("Merchant ID")
    datatrans_transactionId = fields.Char("Transaction Id")
    datatrans_masked_cc = fields.Char("Masked CC")
    # Status API Response
    datatrans_transaction_date = fields.Char("Transaction Date")
    datatrans_transaction_time = fields.Char("Transaction Time")
    datatrans_transaction_type = fields.Char("Transaction Type")
    datatrans_response_code = fields.Integer("Response Code")

    @api.model
    def _datatrans_form_get_tx_from_data(self, data):
        """ Given a data dict coming from datatrans, verify it and find the related transaction record. """
        data = dict(data)
        reference, datatrans_transactionId = data.get('refno'), data.get('uppTransactionId')
        if not reference or not datatrans_transactionId:
            error_msg = _(
                'Datatrans: received data with missing reference (%s) or transaction_id (%s)') % (
                            reference, datatrans_transactionId)
            _logger.info(error_msg)
            raise ValidationError(error_msg)
        tx = self.search([('reference', '=', reference)])
        if not tx or len(tx) > 1:
            error_msg = _('Datatrans: received data for reference')
            if not tx:
                error_msg += _(';no order found')
            else:
                error_msg += _(';multiple order found')
            _logger.info(error_msg)
            raise ValidationError(error_msg)

        return tx

    def _datatrans_form_get_invalid_parameters(self, data):
        """To get invalid parameters obtained from datatrans as response"""
        invalid_parameters = []

        config = self.acquirer_id.datatrans_config_id
        datatrans_merchantId = config.datatrans_merchantId
        if datatrans_merchantId and int(data.get('merchantId')) != int(datatrans_merchantId):
            invalid_parameters.append(('merchantId', data.get('merchantId'), datatrans_merchantId))

        if data.get("currency").upper() != self.currency_id.name:
            invalid_parameters.append(
                ("Currency", data.get("currency"), self.currency_id.name)
            )

        return invalid_parameters

    def _datatrans_form_validate(self, data):
        """Handle the response returned from datatrans based on response code"""
        payment_icon_id = False
        status_code = data.get('responseCode')
        if not status_code:
            status_code = data.get('errorCode')
        if data.get('pmethod'):
            payment_icon_id = self.env['payment.icon'].search([('datatrans_code', '=', data.get('pmethod'))], limit=1).id
        log_vals = {
            'name': data.get('refno'),
            'payment_method': payment_icon_id,
            'response': data,
            'status': data.get('status'),
            'message': data.get('responseMessage') if data.get('responseMessage') else data.get('errorDetail')
        }
        self.env['payment.acquirer.log']._post_log(log_vals)
        message = "Datatrans: %s" % (data.get('responseMessage'))
        values = {
            'acquirer_reference': data.get('acqAuthorizationCode'),
            'datatrans_state': data.get('status'),
            'state_message': message,
            'datatrans_acq_auth_code': data.get('acqAuthorizationCode'),
            'datatrans_auth_code': data.get('authorizationCode'),
            'datatrans_pmethod': payment_icon_id,
            'datatrans_reqtype': data.get('reqtype'),
            'datatrans_transactionId': data.get('uppTransactionId'),
            'datatrans_merchantId': data.get('merchantId'),
            'datatrans_response_code': data.get('responseCode'),
            'date': fields.Datetime.now()
        }
        self.write(values)
        if status_code in self._datatrans_valid_tx_res_code:
            self._set_transaction_done()
            self.write({
                'state_message': message,
            })
            return True
        elif status_code in self._datatrans_pending_tx_res_code:
            self._set_transaction_pending()
            self.write({
                'state_message': message,
            })
            return True
        elif status_code in self._datatrans_cancel_tx_res_code:
            self._set_transaction_cancel()
            self.write({
                'state_message': message,
            })
            return True
        elif status_code in self._datatrans_error_tx_res_code or status_code in self._datatrans_s2s_error_code:
            error = "Datatrans: %s" % (data.get('errorDetail'))
            self._set_transaction_error(msg=error)
            self.write({
                'state_message': error,
            })
            return False
        else:
            error = 'Datatrans: feedback error'
            _logger.info(error)
            self.write({
                'state_message': error,
            })
            self._set_transaction_cancel()
            return False

    def update_datatrans_state(self):
        try:
            api_url = self.acquirer_id.datatrans_config_id.datatrans_api_url if self.acquirer_id.state == 'enabled' else self.acquirer_id.datatrans_config_id.datatrans_test_api_url
            api_url_status = urls.url_join(api_url, '/upp/jsp/XML_status.jsp')
            xml_data = '''<statusService version="3"> 
      <body merchantId='{merchantId}'>
        <transaction>
          <request>
            <uppTransactionId>%s</uppTransactionId>
            <reqtype>STX</reqtype> 
          </request>
        </transaction>
      </body>
    </statusService>'''.format(merchantId=self.datatrans_merchantId) % self.datatrans_transactionId
            to_base_64 = "%s:%s" % (self.datatrans_merchantId, self.acquirer_id.datatrans_config_id.datatrans_password)
            r = requests.post(api_url_status,
                              data=xml_data,
                              headers={'Content-Type': 'application/xml', 'Authorization': "Basic %s" % (base64.b64encode(bytes(to_base_64, 'utf-8')).decode())})


            xml_response = minidom.parseString(r.content)
            if r.headers['Content-Type'] == 'text/xml;charset=UTF-8':
                status = xml_response.getElementsByTagName('body')[0].getAttribute('status')
                if status != 'error':
                    trxStatus = xml_response.getElementsByTagName('transaction')[0].getAttribute('trxStatus')
                    if trxStatus and trxStatus == 'response':
                        responseCode = xml_response.getElementsByTagName('responseCode')[0].childNodes[0].nodeValue
                        if responseCode in self._datatrans_update_transaction_success_code:
                            responseMessage = xml_response.getElementsByTagName('responseMessage')[0].childNodes[
                                0].nodeValue
                            settledAmount = xml_response.getElementsByTagName('settledAmount')[0].childNodes[0].nodeValue
                            self.write({'datatrans_state': responseMessage})
                            self._set_transaction_done()
                else:
                    errorCode = xml_response.getElementsByTagName('errorCode')[0].childNodes[0].nodeValue;
                    errorMessage = xml_response.getElementsByTagName('errorMessage')[0].childNodes[0].nodeValue;
                    errorDetail = xml_response.getElementsByTagName('errorDetail')[0].childNodes[0].nodeValue;
                    self._set_transaction_cancel()
                    error_msg = _(
                        'Datatrans: returned error (%s)- (%s) with error detail (%s)') % (
                                    errorCode, errorMessage, errorDetail)
                    _logger.info(error_msg)
            return
        except Exception as e:
            _logger.error('A error encountered : %s ' % e)
            raise exceptions.ValidationError(e)

    def action_datatrans_deferred_settlement(self, data):
        try:
            partner = self[0].partner_id
            vals = {}
            vals.update({
                'amount': data.get('amount'),
                'currency_id': data.get('currency').id,
                'partner_id': partner.id,
                'acquirer_id': data.get('acquirer_id').id,
                'invoice_ids': [(6, 0, self.ids)],
                'type': 'form',
                'is_datatrans_refund': True,
            })
            transaction = self.env['payment.transaction'].sudo().create(vals)
            api_url = self.acquirer_id.datatrans_config_id.datatrans_api_url if self.acquirer_id.state == 'enabled' else self.acquirer_id.datatrans_config_id.datatrans_test_api_url
            if not api_url:
                error_msg = _('Datatrans: URL missing in configuration')
                _logger.info(error_msg)
                raise exceptions.ValidationError(error_msg)
            api_url_deffered = urls.url_join(api_url, '/upp/jsp/XML_processor.jsp')

            xml_data = '''<paymentService version="1">
          <body merchantId='{merchantId}'>
            <transaction refno='{refno}'>
              <request>
                <amount>%s</amount>
                <currency>%s</currency>
                <uppTransactionId>%s</uppTransactionId>
                <transtype>%s</transtype>
              </request>
            </transaction>
          </body>
        </paymentService>'''.format(merchantId=data.get('merchantId'), refno=data.get('refno')) % (
                int(float_round(data.get('amount') * 100, 2)),
                data.get('currency').name,
                data.get('uppTransactionId'),
                data.get('transtype'))
            to_base_64 = "%s:%s" % (self.datatrans_merchantId, self.acquirer_id.datatrans_config_id.datatrans_password)
            r = requests.post(api_url_deffered,
                              data=xml_data,
                              headers={'Content-Type': 'application/xml', 'Authorization': "Basic %s" % (base64.b64encode(bytes(to_base_64, 'utf-8')).decode())})
            xml_response = minidom.parseString(r.content)
            if not r.headers['Content-Type'] == 'text/html;charset=UTF-8':
                status = xml_response.getElementsByTagName('body')[0].getAttribute('status')
                trxStatus = xml_response.getElementsByTagName('transaction')[0].getAttribute('trxStatus')
                if trxStatus and trxStatus == 'response':
                    merchantId = xml_response.getElementsByTagName('body')[0].getAttribute('merchantId')
                    responseCode = xml_response.getElementsByTagName('responseCode')[0].childNodes[0].nodeValue
                    responseMessage = xml_response.getElementsByTagName('responseMessage')[0].childNodes[0].nodeValue
                    uppTransactionId = xml_response.getElementsByTagName('uppTransactionId')[0].childNodes[0].nodeValue
                    authorizationCode = xml_response.getElementsByTagName('authorizationCode')[0].childNodes[0].nodeValue
                    acqAuthorizationCode = xml_response.getElementsByTagName('acqAuthorizationCode')[0].childNodes[
                        0].nodeValue
                    transaction.write({'acquirer_reference': acqAuthorizationCode,
                                       'datatrans_state': status,
                                       'datatrans_acq_auth_code': acqAuthorizationCode,
                                       'datatrans_auth_code': authorizationCode,
                                       'datatrans_transactionId': uppTransactionId,
                                       'datatrans_merchantId': merchantId,
                                       'date': fields.Datetime.now()})
                    transaction._set_transaction_done()
                else:
                    errorCode = xml_response.getElementsByTagName('errorCode')[0].childNodes[0].nodeValue;
                    errorMessage = xml_response.getElementsByTagName('errorMessage')[0].childNodes[0].nodeValue;
                    errorDetail = xml_response.getElementsByTagName('errorDetail')[0].childNodes[0].nodeValue;
                    transaction._set_transaction_cancel()
                    error_msg = _(
                        'Datatrans: returned error (%s)- (%s) with error detail (%s)') % (
                                    errorCode, errorMessage, errorDetail)
                    _logger.info(error_msg)
                    raise ValidationError(error_msg)
            else:
                raise ValidationError('Unauthorized')
            return
        except Exception as e:
            _logger.error('A error encountered : %s ' % e)
            raise exceptions.ValidationError(e)


class PaymentIcon(models.Model):
    _inherit = 'payment.icon'

    DATATRANS_CODE = [
        ('AMX', _('AMX')),
        ('CUP', _('CUP')),
        ('DIN', _('DIN')),
        ('DIS', _('DIS')),
        ('ECA', _('ECA')),
        ('VIS', _('VIS')),
        ('UAP', _('UAP')),
        ('MAU', _('MAU')),
        ('GEP', _('GEP')),
        ('MYO', _('MYO')),
        ('PAP', _('PAP')),
        ('PFC', _('PFC')),
        ('MFG', _('MFG')),
        ('MDP', _('MDP')),
        ('SWP', _('SWP')),
        ('JCB', _('JCB')),
        ('TWI', _('TWI')),
        ('DIB', _('DIB')),
    ]

    in_datatrans = fields.Boolean(default=False)
    datatrans_code = fields.Selection(DATATRANS_CODE, domain=[('in_datatrans', '=', True)])
    reqtype = fields.Selection([('SCN', 'SCN'), ('NOA', 'NOA')], default="SCN",
                               help="Use SCN if only a pre-screening request should be done. "
                                    "When using NOA, a screening and authorization request will be done")
    currency_ids = fields.Many2many('res.currency', string="Allowed currencies")

    support_authorization = fields.Boolean('Authorization', default=True, invisible=True)
    support_deferred_settlement = fields.Boolean('Deferred Settlement', default=False, invisible=True)
    support_refund = fields.Boolean('Refund', default=False, invisible=True)
    support_cancel = fields.Boolean('Cancel', default=False, invisible=True)
    support_alias = fields.Boolean('Alias', default=False, invisible=True)

    _sql_constraints = [
        ('datatrans_code_uniq', 'unique(datatrans_code)', 'The datatrans code of the payment method must be unique !'),
    ]
