# -*- coding: utf-8 -*-
#################################################################################
# Author      : PIT Solutions AG. (<https://www.pitsolutions.ch/>)
# Copyright(c): 2019 - Present PIT Solutions AG.
# License URL : https://www.webshopextension.com/en/licence-agreement/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.webshopextension.com/en/licence-agreement/>
#################################################################################

from odoo import api, models, fields, _


class DatatransAcquirerConfig(models.Model):
    _name = "datatrans.acquirer.config"
    _description = "Datatrans acquirer configuration details"

    name = fields.Char(required=True, translate=True, copy=False)
    description = fields.Html(translate=True)    
    acquirer_ids = fields.One2many('payment.acquirer', 'datatrans_config_id', 'Payment Acquirers')
    active = fields.Boolean(default=True)
    environment = fields.Selection([('test', 'Test'), ('prod', 'Production')], default='test', required=True)
    datatrans_password = fields.Char('Password', groups='base.group_user')
    datatrans_merchantId = fields.Char(required_if_provider='datatrans', string='MerchantID',
                                          groups='base.group_user')
    is_dynamic = fields.Boolean("Enable Dynamic Signature")
    datatrans_sign = fields.Char(required_if_provider='datatrans', string='Sign', groups='base.group_user', copy=False)
    language = fields.Selection(
        [('de', 'German'), ('en', 'English'), ('fr', 'French'), ('it', 'Italian'), ('es', 'Spanish'), ('el', 'Greek'),
         ('no', 'Norwegian'), ('da', 'Danish'), ('pl', 'Polish'), ('pt', 'Portuguese')]
        , help="Language in which the payment page should be presented to the cardholder", default='en')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id,
                                 copy=False, ondelete='restrict')
    datatrans_base_url = fields.Text(string="Datatrans Url", groups="base.group_no_one")
    datatrans_api_url = fields.Text(string="Datatrans API Url", groups="base.group_no_one")
    datatrans_test_base_url = fields.Text(string="Datatrans Test Url", groups="base.group_no_one")
    datatrans_test_api_url = fields.Text(string="Datatrans API Test Url", groups="base.group_no_one")
    # Styling
    brand_color = fields.Char(help="The color of your brand")
    text_color = fields.Selection([('white', 'White'), ('black', 'Black')],
                                  help="The color of the text in the header bar if no logo is given")
    logo_type = fields.Selection([('circle', 'Circle'), ('rectangle', 'Rectangle'), ('none', 'None')]
                                 , help="The header logo's display style")
    logo_border_color = fields.Char(help="whether the logo shall be styled with a border around it")
    brand_button = fields.Char(help="Decides if the pay button should have the same color as the brandColor.")
    logo_src = fields.Binary("Image")

    def toggle_environment_value(self):
        prod = self.filtered(lambda datatrans_config: datatrans_config.environment == 'prod')
        prod.write({'environment': 'test'})
        (self - prod).write({'environment': 'prod'})

    def action_view_payment_acquirers(self):
        self.ensure_one()
        context = dict(self.env.context or {})
        action = self.env.ref('payment.action_payment_acquirer')
        result = action.read()[0]
        acquirer_ids = self.mapped('acquirer_ids')
        if not acquirer_ids or len(acquirer_ids) > 1:
            result['domain'] = "[('id','in',%s)]" % acquirer_ids.ids
        elif len(acquirer_ids) == 1:
            res = self.env.ref('payment.acquirer_form', False)
            result['views'] = [(res and res.id or False, 'form')]
            result['res_id'] = acquirer_ids.id
        result.update({'context': context})
        return result
