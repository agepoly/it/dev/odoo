# -*- coding: utf-8 -*-
#################################################################################
# Author      : PIT Solutions AG. (<https://www.pitsolutions.ch/>)
# Copyright(c): 2019 - Present PIT Solutions AG.
# License URL : https://www.webshopextension.com/en/licence-agreement/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.webshopextension.com/en/licence-agreement/>
#################################################################################

import datetime
import logging
from odoo import fields, models, api, _

from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

_logger = logging.getLogger(__name__)


class PaymentAcquirerLog(models.Model):
    _name = "payment.acquirer.log"
    _description = "Payment acquirer log details"
    _order = "id desc"

    name = fields.Char(string="Description", required=True)
    origin = fields.Char(default='datatrans', readonly=True)
    payment_method = fields.Many2one('payment.icon', ondelete='set null',
                                     groups='base.group_user', readonly=True)
    status = fields.Selection([('success', 'Success'), ('Pending', 'Pending'), ('error', 'Error'), ('cancel', 'Cancel')], string="Result",
                              readonly=True, required=True)
    message = fields.Char(readonly=True)
    response = fields.Text(readonly=True)


    @api.model
    def clean_old_logging(self, days=90):
        """
        Function called by a cron to clean old loggings.
        @return: True
        """
        last_days = datetime.datetime.now() + \
                    datetime.timedelta(days=-days)
        domain = [
            ('create_date', '<', last_days.strftime(
                DEFAULT_SERVER_DATETIME_FORMAT))
        ]
        logs = self.search(domain)
        logs.unlink()
        message = " %d logs are deleted" % (len(logs))
        return self._post_log({'name': message})

    @api.model
    def _post_log(self, vals):
        self.create(vals)
