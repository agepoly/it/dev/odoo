# -*- coding: utf-8 -*-
#################################################################################
# Author      : PIT Solutions AG. (<https://www.pitsolutions.ch/>)
# Copyright(c): 2019 - Present PIT Solutions AG.
# License URL : https://www.webshopextension.com/en/licence-agreement/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.webshopextension.com/en/licence-agreement/>
#################################################################################

import logging
import werkzeug

from odoo import http
from odoo.http import request
from pprint import pformat

_logger = logging.getLogger(__name__)


class DatatransController(http.Controller):
    _success_url = '/payment/datatrans/success'
    _cancel_url = '/payment/datatrans/cancel'
    _error_url = '/payment/datatrans/error'
    _redirect_url = '/upp/jsp/upStart.jsp'

    @http.route([_success_url, _error_url, _redirect_url, _cancel_url], type='http', auth='public', methods=['GET', 'POST'], csrf=False, website=True)
    def datatrans_success_form_feedback(self, txnId=None, **post):
        """ Handles the data coming from the acquirer after the transaction.
        It will generally receives data posted by the acquirer after the transaction. """

        _logger.info(
            'Datatrans: entering form_feedback with txnId: %s\n, post data: %s', pformat(txnId), pformat(post))
        request.env['payment.transaction'].sudo().form_feedback(post, 'datatrans')
        return werkzeug.utils.redirect('/payment/process')



