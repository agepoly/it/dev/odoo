# -*- coding: utf-8 -*-
#################################################################################
# Author      : PIT Solutions AG. (<https://www.pitsolutions.ch/>)
# Copyright(c): 2019 - Present PIT Solutions AG.
# License URL : https://www.webshopextension.com/en/licence-agreement/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.webshopextension.com/en/licence-agreement/>
#################################################################################

import logging
from pprint import pformat
import werkzeug
from werkzeug import urls

from odoo import http, _
from odoo.http import request
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from datetime import datetime, timedelta
from odoo.addons.account.controllers.portal import PortalAccount
from odoo.addons.payment.controllers.portal import PaymentProcessing
from odoo.addons.payment.controllers.portal import WebsitePayment
from odoo.addons.portal.controllers.portal import CustomerPortal, pager as portal_pager
from odoo.osv import expression
from odoo.addons.http_routing.models.ir_http import unslug, slug
from odoo.exceptions import AccessError, MissingError
from collections import OrderedDict


_logger = logging.getLogger(__name__)


class SaferpayPaymentController(PaymentProcessing):
        
    def _get_safe_tkntxns(self, payment_tx_ids):
        Transaction = request.env['payment.transaction'].sudo()   
        safe_tkntxns = Transaction.search([
                                            ('id', 'in', payment_tx_ids),
                                            ('payment_token_id', '!=', False),
                                            #('type', '=', 'form_save'),
                                            ('pay_card', '=', True),
                                            ('form_validate', '=', False),                                                                            
                                            ('state', 'in', ['draft']),
                                        ])
        return safe_tkntxns
    
    @http.route()
    def payment_status_page(self, **kwargs):
        res = super(SaferpayPaymentController, self).payment_status_page(**kwargs)
        values = res.qcontext
        Transaction = request.env['payment.transaction'].sudo()  
        payment_tx_ids = values.get('payment_tx_ids')
        payment_txns = Transaction.search([('id', 'in', payment_tx_ids)])
        if payment_txns:
            safe_tkntxns = self._get_safe_tkntxns(payment_tx_ids)
            last_safe_tkntxn_id = safe_tkntxns and max(safe_tkntxns.ids) or 0
            last_payment_tx_id = max(payment_txns.ids)
            last_safe_tkntxn = Transaction.browse(last_safe_tkntxn_id).exists()
            if last_safe_tkntxn and last_safe_tkntxn_id >= last_payment_tx_id:
                trans_id = last_safe_tkntxn.id
                render_values = {
                    'partner_id': last_safe_tkntxn.partner_id and last_safe_tkntxn.partner_id.id or False,
                }
                trans_interface = last_safe_tkntxn.acquirer_id.sudo().render(last_safe_tkntxn.reference, float(last_safe_tkntxn.amount), int(last_safe_tkntxn.currency_id), values=render_values)
                vals = {
                    'trans_interface': trans_interface,
                    'trans_id': trans_id,
                }   
                return request.render('payment_saferpay.saferpay_payment_token_trans', vals)            
        return res
    
    @http.route()
    def payment_status_poll(self):       
        tx_ids_list = self.get_payment_transaction_ids()
        Transaction = request.env['payment.transaction']
        saferpay_pending_states = Transaction._saferpay_pending_states
        payment_transactions = Transaction.sudo().search([
            ('id', 'in', list(tx_ids_list)),
            ('date', '>=', (datetime.now() - timedelta(days=1)).strftime(DEFAULT_SERVER_DATETIME_FORMAT)),
            ('acquirer_id.provider','=', 'saferpay'), 
            ('saferpay_state','in', saferpay_pending_states)
        ])
        for tx in payment_transactions:
            tx._saferpay_form_validate(data={})        
        return super(SaferpayPaymentController, self).payment_status_poll()
    
    @http.route(['/payment/saferpay/s2s/create_json_3ds'], type='json', auth='public', csrf=False)
    def saferpay_s2s_create_json_3ds(self, verify_validity=False, **kwargs):
        if not kwargs.get('partner_id'):
            kwargs = dict(kwargs, partner_id=request.env.user.partner_id.id)
        token = request.env['payment.acquirer'].browse(int(kwargs.get('acquirer_id'))).s2s_process(kwargs)

        if not token:
            res = {
                'result': False,
            }
            return res

        res = {
            'result': True,
            'id': token.id,
            'short_name': token.short_name,
            '3d_secure': False,
            'verified': False,
        }

        if verify_validity != False:
            token.validate()
            res['verified'] = token.verified

        return res


class SaferpayPortalAccount(PortalAccount):

    @http.route(['/my/invoices/<int:invoice_id>/refund'], type='http', auth="public", website=True)
    def portal_my_invoice_detail_refund(self, invoice_id, access_token=None, **kwargs):
        try:
            invoice_sudo = self._document_check_access('account.move', invoice_id, access_token)
        except (AccessError, MissingError):
            return request.redirect('/my')
        values = self._invoice_get_page_view_values(invoice_sudo, access_token, **kwargs)
        invoice = values.get('invoice')

        acquirers = list(request.env['payment.acquirer'].search([
            ('state', 'in', ['enabled','test']), 
            ('provider', '=', 'saferpay'),
            ('is_support_refund','=', True),
            ('support_refund_type','in',['direct','both']),
            ('company_id', '=', request.env.user.company_id.id)
        ]))        
        partner = request.env.user.partner_id
        payment_tokens = partner.payment_token_ids.filtered(lambda r: r.acquirer_id.is_support_refund == True and r.acquirer_id.support_refund_type in ['direct','both'])
        payment_tokens |= partner.commercial_partner_id.sudo().payment_token_ids.filtered(lambda r: r.acquirer_id.is_support_refund == True and r.acquirer_id.support_refund_type in ['direct','both'])
        return_url = invoice.get_portal_url()
        vals = {
            'pms': payment_tokens,
            'acquirers': acquirers,
            'error_message': [kwargs['error']] if kwargs.get('error') else False,
            'return_url': return_url,
            'bootstrap_formatting': True,
            'partner_id': partner.id,
            'invoice': invoice
        }    
        return request.render('payment_saferpay.portal_invoice_refund_page', vals)


    @http.route(['/my/invoices', '/my/invoices/page/<int:page>'], type='http', auth="user", website=True)
    def portal_my_invoices(self, page=1, date_begin=None, date_end=None, sortby=None, filterby=None, **kw):
        values = self._prepare_portal_layout_values()
        AccountInvoice = request.env['account.move']

        domain = [('move_type', 'in', ('out_invoice', 'out_refund', 'in_invoice', 'in_refund', 'out_receipt', 'in_receipt'))]

        searchbar_sortings = {
            'date': {'label': _('Date'), 'order': 'invoice_date desc'},
            'duedate': {'label': _('Due Date'), 'order': 'invoice_date_due desc'},
            'name': {'label': _('Reference'), 'order': 'name desc'},
            'state': {'label': _('Status'), 'order': 'state'},
        }
        # default sort by order
        if not sortby:
            sortby = 'date'
        order = searchbar_sortings[sortby]['order']

        searchbar_filters = {
            'all': {'label': _('All'), 'domain': [('move_type', 'in', ['in_invoice', 'out_invoice', 'out_refund'])]},
            'invoices': {'label': _('Invoices'), 'domain': [('move_type', '=', 'out_invoice')]},
            'bills': {'label': _('Bills'), 'domain': [('move_type', '=', 'in_invoice')]},
            'credit_notes': {'label': _('Credit Notes'), 'domain': [('move_type', '=', 'out_refund')]},
        }
        # default filter by value
        if not filterby:
            filterby = 'all'
        domain += searchbar_filters[filterby]['domain']

        if date_begin and date_end:
            domain += [('create_date', '>', date_begin), ('create_date', '<=', date_end)]

        # count for pager
        invoice_count = AccountInvoice.search_count(domain)
        # pager
        pager = portal_pager(
            url="/my/invoices",
            url_args={'date_begin': date_begin, 'date_end': date_end, 'sortby': sortby},
            total=invoice_count,
            page=page,
            step=self._items_per_page
        )
        # content according to pager and archive selected
        invoices = AccountInvoice.search(domain, order=order, limit=self._items_per_page, offset=pager['offset'])
        request.session['my_invoices_history'] = invoices.ids[:100]

        values.update({
            'date': date_begin,
            'invoices': invoices,
            'page_name': 'invoice',
            'pager': pager,
            'default_url': '/my/invoices',
            'searchbar_sortings': searchbar_sortings,
            'sortby': sortby,
            'searchbar_filters': OrderedDict(sorted(searchbar_filters.items())),
            'filterby':filterby,
        })
        return request.render("account.portal_my_invoices", values)


class SaferpayPayment(WebsitePayment):

    @http.route()
    def payment_method(self, **kwargs):
        res = super(SaferpayPayment, self).payment_method(**kwargs)
        values = res.qcontext
        acquirers = list(request.env['payment.acquirer'].search([
                ('state', 'in', ['enabled', 'test']), ('provider', '=', 'saferpay'),
                ('save_token', 'in', ['ask','always']), ('payment_flow', '!=', 's2s'), 
#                 ('interface_type', '!=', 'saferpay_fields'),
                ('company_id', '=', request.env.user.company_id.id)
            ])) + values['acquirers']
        values.update({'acquirers': acquirers})
        return res


class SaferpayController(http.Controller):
    
    _success_interface_url = '/payment/saferpay/success/interface'    
    _failed_interface_url = '/payment/saferpay/failed/interface'
    _success_url = '/payment/saferpay/success'
    _failed_url = '/payment/saferpay/failed'
    _saferpay_redirect_url = '/payment/saferpay/redirect'
    _notify_url = '/payment/saferpay/notify'
    _alias_sucess_url = '/payment/saferpay/alias/success'
    _alias_failed_url = '/payment/saferpay/alias/failed'
    _unexpected_url = '/payment/saferpay/unexpected'
        

    @http.route([_saferpay_redirect_url], type='http', auth='public', csrf=False)
    def saferpay_form_redirect(self, **post):
        _logger.info(
            'Saferpay: entering saferpay_form_redirect with post data %s', pformat(post))
        saferpay_txn_url = post.get('saferpay_txn_url', self._failed_url)
        request.session.update({'saferpay_req_token': post.get('saferpay_req_token'),
                                'saferpay_request_id': post.get('saferpay_request_id'),
                                'acquirer' : post.get('acquirer'),
                                'saferpay_customer_id' : post.get('saferpay_customer_id'),
                                'saferpay_retry_indicator': post.get('saferpay_retry_indicator')})
        return werkzeug.utils.redirect(saferpay_txn_url)
    
    @http.route([_notify_url], type='http', auth='public', methods=['GET'], csrf=False)
    def saferpay_notify(self, txnId=None, **post):
        _logger.info('Beginning Saferpay saferpay_notify with txnId: %s\n, post data: %s', pformat(txnId), pformat(post))  # debug
        try:
            txn = request.env['payment.transaction'].sudo().browse(int(txnId))
            saferpay_request_id = txn.reference
            txn.sudo().write({'invoke_notify_url': True})
            post.update({
                'txn_id': txnId,
                'saferpay_request_id': saferpay_request_id,
            })
            request.env['payment.transaction'].sudo().form_feedback(post, 'saferpay')
        except Exception as e:
            _logger.exception('Unable to validate the Saferpay payment %s' % (e,))
        return ''
    
    @http.route([_success_url], type='http', auth='public', methods=['GET'], csrf=False, website=True)
    def saferpay_success_form_feedback(self, txnId=None, **post):
        _logger.info(
            'Saferpay: entering form_feedback with txnId: %s\n, post data: %s', pformat(txnId), pformat(post))
        try:
            txn = request.env['payment.transaction'].sudo().browse(int(txnId))
            saferpay_request_id = txn.reference
            post.update({
                'txn_id': txnId,
                'saferpay_request_id': saferpay_request_id,
            })
            request.env['payment.transaction'].sudo().form_feedback(post, 'saferpay')
        except Exception as e:
            _logger.exception('Unable to validate the Saferpay payment %s' % (e,))
        return werkzeug.utils.redirect('/payment/process')
    
    @http.route([_failed_url], type='http', auth='public', methods=['GET'], csrf=False, website=True)
    def saferpay_fail_form_feedback(self, txnId=None, **post):
        _logger.info(
            'Saferpay: entering fail_form_feedback with txnId: %s\n, post data: %s', pformat(txnId), pformat(post))
        message = ''
        try:
            txn = request.env['payment.transaction'].sudo().browse(int(txnId))
            saferpay_request_id = txn.reference
            post.update({
                'txn_id': txnId,
                'saferpay_request_id': saferpay_request_id,
            })
            message = txn.state_message
            request.env['payment.transaction'].sudo().form_feedback(post, 'saferpay')
        except Exception as e:
            _logger.info('Unable to validate the Saferpay payment %s' % (e,))
            return werkzeug.utils.redirect(self._unexpected_url)    
        return werkzeug.utils.redirect('/payment/process')

    @http.route([_unexpected_url], type='http', auth='public', csrf=False, website=True)
    def saferpay_unexpected_form_feedback(self, **post):
        _logger.info(
            'Saferpay: entering saferpay_unexpected_form_feedback with post data %s', pformat(post))
        return request.render("payment_saferpay.saferpay_payment_unexpected_error", {})

    @http.route([_success_interface_url], type='http', auth='public', csrf=False, website=True)
    def saferpay_success_interface_redirect(self, txnId=None, **post):
        _logger.info(
            'Saferpay: entering saferpay_success_interface_redirect with txnId: %s\n, post data: %s', pformat(txnId), pformat(post))
        base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url')
        success_url = "".join([self._success_url, '?txnId=', str(txnId)])
        return request.render('payment_saferpay.payment_saferpay_interface_redirect', {
            'return_url': urls.url_join(base_url, success_url)
        })

    @http.route([_failed_interface_url], type='http', auth='public', csrf=False, website=True)
    def saferpay_failed_interface_redirect(self, txnId=None, **post):
        _logger.info(
            'Saferpay: entering saferpay_failed_interface_redirect with txnId: %s\n, post data: %s', pformat(txnId), pformat(post))
        base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url')
        failed_url = "".join([self._failed_url, '?txnId=', str(txnId)])
        return request.render('payment_saferpay.payment_saferpay_interface_redirect', {
            'return_url': urls.url_join(base_url, failed_url)
        })
    
    @http.route([_alias_sucess_url, _alias_failed_url], type='http', auth='public', csrf=False, website=True)
    def saferpay_alias_feedback(self, **post):
        _logger.info(
            'Saferpay: entering form_feedback with post data %s', pformat(post))
        request.env['payment.token'].sudo().alias_feedback()
        return werkzeug.utils.redirect('/my/payment_method')
    
    @http.route('/payment/saferpay/trans_url', type='json', auth='public', csrf=False, website=True)
    def get_saferpay_transaction_interface_url(self, **post):
        _logger.info(
            'Saferpay: entering  get_saferpay_transaction_interface_url')
        txn_vals = post.get('txn_vals')
        form_data = post.get('form_data')
        fields_token = post.get('fields_token')
        save_card_token = post.get('save_card_token')
        return request.env['payment.acquirer'].sudo().saferpay_build_transaction_interface_url(txn_vals, form_data, fields_token, save_card_token)
    
