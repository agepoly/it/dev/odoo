# -*- coding: utf-8 -*-
#################################################################################
# Author      : PIT Solutions AG. (<https://www.pitsolutions.ch/>)
# Copyright(c): 2019 - Present PIT Solutions AG.
# License URL : https://www.webshopextension.com/en/licence-agreement/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.webshopextension.com/en/licence-agreement/>
#################################################################################

{
  "name"                 :  "Saferpay Payment Acquirer Plugin From PIT Solutions",
  "category"             :  "Accounting",
  "summary"              :  """
                                Payment Acquirer: Saferpay Implementation
                            """,
  "description"          :  """
                                Saferpay Payment Acquirer
                            """,
  "version"              :  "1.0",
  "author"               :  "PIT Solutions AG.",
  "license"              :  "Other proprietary",
  "website"              :  "https://www.pitsolutions.ch",
  "live_test_url"        :  "http://odemo12.dev.displayme.net/web/login",
  "depends"              :  [
                             'payment',
                            ],
  "data"                 :  [
                                'wizard/payment_refund_wizard.xml',
                                'views/payment_saferpay_templates.xml',
                                'data/ideal_issuerid_config_data.xml',
                                'data/saferpay_acquirer_config_data.xml',
                                'data/payment_icon_data.xml',
                                'data/payment_acquirer_data.xml',
                                'data/ir_cron_data.xml',
                                'data/res_currency_data.xml',
                                'security/ir.model.access.csv',
                                'security/payment_saferpay_security.xml',
                                'views/ideal_issuerid_config_view.xml',
                                'views/saferpay_acquirer_log.xml',
                                'views/saferpay_acquirer_config_view.xml',
                                'views/payment_views.xml',
                                # 'views/payment_txn_refund_views.xml',
                                'views/payment_templates.xml',
                                'views/payment_refund_template.xml',
                                'views/multipart_capture_views.xml',
                            ],
  "images"               :  ['static/description/icon.png'],
  "price"                :  150,
  "currency"             :  "EUR",
  "installable"          :  True,
  "post_init_hook"       :  "create_missing_journal_for_acquirers",
  "uninstall_hook"       :  "uninstall_hook",
}

