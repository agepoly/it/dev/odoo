odoo.define('payment_saferpay.saferpay_interface', function(require) {
 'use strict';
 	var SaferpayTxnInterface = require('payment_saferpay.saferpay_txn_interface');
 	var saferpayTxnInterfaceWidget = new SaferpayTxnInterface(null, {});
 	saferpayTxnInterfaceWidget.attachTo($('.o_saferpay_interface'));
});