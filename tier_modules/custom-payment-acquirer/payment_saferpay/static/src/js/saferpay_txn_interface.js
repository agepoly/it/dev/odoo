odoo.define('payment_saferpay.saferpay_txn_interface', function (require) {
    'use strict';

    var Widget = require('web.Widget');
    var Ajax = require('web.ajax');
    var Rpc = require("web.rpc");
    var Dom = require('web.dom');
    var Dialog = require("web.Dialog");
    var Config = require('web.config');
    var Core = require('web.core');
    var Qweb = Core.qweb;
    var _t = Core._t;

    $.blockUI.defaults.css.border = '0';
    $.blockUI.defaults.css["background-color"] = '';
    $.blockUI.defaults.overlayCSS["opacity"] = '0.9';

    return Widget.extend({        
        _saferpay_payment_method: null,        
        xmlDependencies: ['/payment_saferpay/static/src/xml/saferpay_txn_interface.xml'],
        init: function (parent, saferpay_payment_method) {
            this._super.apply(this, arguments);
            this._saferpay_payment_method = saferpay_payment_method;
        },
        start: function() {
            this.showLoading();
            try { 
            	this.showSaferpayInterface();
            } catch (err) {
                console.error(err);
                //return;
            } 
            return this._super.apply(this, arguments);
        },
        disableButton: function ($button, loader) {
            loader = loader || undefined;
            $button.attr('disabled', true);
            if (loader){
                $button.children('.fa-lock').removeClass('fa-lock');
                $button.prepend('<span class="o_loader"><i class="fa fa-refresh fa-spin"></i>&nbsp;</span>');
            }
        },
        enableButton: function ($button) {
            $button.attr('disabled', false);
            $button.children('.fa').addClass('fa-lock');
            $button.find('span.o_loader').remove();
        },
        showSaferpayInterface: function () {
            var self = this;
            if ($.blockUI) {
                var parsed_provider_form = $('form[provider="saferpay"]');
                var saferpay_txn_interface_url = parsed_provider_form.find('input[name="saferpay_txn_interface_url"]').val();
                var referrer_path = parsed_provider_form.find('input[name="referrer_path"]').val();
                var content = '<iframe width="100%" height="100%" frameborder="0" src="'+saferpay_txn_interface_url+'"></iframe>';
                self.dialog = new Dialog(self, {
                    title: _t("Powered By Saferpay Interface"),
                    subtitle: _t(""),
                    $content: content,
                    dialogClass: 'saferpay-payment-form-modal',
                    fullscreen: true,
                });
                self.dialog.opened().then(function () {
                    self.dialog.$modal.find('.modal-content').css({
                        'height': 600,
                    });
                    self.dialog.$modal.find('.modal-footer .btn-primary').hide();
                    var parsed_provider_form = $('form[provider="saferpay"]');
                    var saferpay_txn_interface_url = parsed_provider_form.find('input[name="saferpay_txn_interface_url"]').val();
                    $.getScript(saferpay_txn_interface_url, function(data, textStatus, jqxhr) {
                        console.log( data ); // Data returned
                        console.log( textStatus ); // Success
                        console.log( jqxhr.status ); // 200
                        console.log( "Load was performed." );
                    }).done(function( script, textStatus ) {
                        console.log("textStatus",textStatus ); 
                        var containerId = 'saferpay-payment-form';
                    }).fail(function( jqxhr, settings, exception ) {
                        self.dialog.$('ul.saferpay-payment-errors').text( "Triggered ajaxError handler." );
                    });
                    self.dialog.$modal.find('button.close').on('click', function(e){
                        e.preventDefault();
                        e.stopPropagation();
                        e.stopImmediatePropagation();
                        if (referrer_path){
                            window.location.href = referrer_path;
                        }else{
                            location.reload();
                        }
                    });
                });
                $.unblockUI();
                self.dialog.open();                
            }
        },        
        showContent: function (xmlid, render_values) {
            var html = Qweb.render(xmlid, render_values);
            this.$el.find('.o_saferpay_interface_content').html(html);
        },
        showLoading: function () {
            var msg = _t("We are processing your payments, please wait ...");
            $.blockUI({
                'message': '<h2 class="text-white"><img src="/web/static/src/img/spin.png" class="fa-pulse"/>' +
                    '    <br />' + msg +
                    '</h2>'
            });
        },
    });
});
