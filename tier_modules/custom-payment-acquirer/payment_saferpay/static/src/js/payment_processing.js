odoo.define('payment_saferpay.payment_processing', function (require) {
    'use strict';
    
    var publicWidget = require('web.public.widget');    
    var saferpay_payment_processing = publicWidget.registry.PaymentProcessing;
    saferpay_payment_processing.include({
        xmlDependencies: saferpay_payment_processing.prototype.xmlDependencies.concat(
            ['/payment_saferpay/static/src/xml/payment.xml']
        ),
    });
});
