odoo.define('payment_saferpay.saferpay_fields_interface', function (require) {
    'use strict';

    var Widget = require('web.Widget');
    var Rpc = require("web.rpc");
    var ajax = require('web.ajax');
    var Dialog = require("web.Dialog");
    var Core = require('web.core');
    var Qweb = Core.qweb;
    var _t = Core._t;

    $.blockUI.defaults.css.border = '0';
    $.blockUI.defaults.css["background-color"] = '';
    $.blockUI.defaults.overlayCSS["opacity"] = '0.9';

    return Widget.extend({                
        xmlDependencies: ['/payment_saferpay/static/src/xml/saferpay_fields_interface.xml'],
        init: function (parent) {
            this._super.apply(this, arguments);
            this.saferpayfieldshandler = null;
        },
        start: function() {
            this.showLoading();
            this.showSaferpayInterface();
            return this._super.apply(this, arguments);
        },
        disableButton: function ($button, loader) {
            loader = loader || undefined;
            $button.attr('disabled', true);
            if (loader){
                $button.children('.fa-lock').removeClass('fa-lock');
                $button.prepend('<span class="o_loader"><i class="fa fa-refresh fa-spin"></i>&nbsp;</span>');
            }
        },
        enableButton: function ($button) {
            $button.attr('disabled', false);
            $button.children('.fa').addClass('fa-lock');
        },
        
        get_place_holder: function (type) {
            var place_holder = ''
            if ((type === 'sample1') || (type === 'sample2')){
                place_holder = {
                                holdername: 'holder name',
                                cardnumber: '0000 0000 0000 0000',
                                expiration: 'MM/YYYY',
                                cvc: '000'
                                }
            }
            else if (type === 'sample3'){
                place_holder = {
                                holdername: 'Karteninhaber',
                                cardnumber: '0000 0000 0000 0000',
                                expiration: 'MM/JJJJ',
                                cvc: '000'
                                  }
            }
            return place_holder
        },
        get_template_style: function (type) {
            var template_style = '';
            if (type === 'sample1'){
                template_style = {
                                    'body': 'background-color: transparent',
                                    '.logo': 'right: 2em;',
                                    'input.form-control': ' border-color:#CFCFCF;border-radius:8px;height:46px;padding-right:0px;',
                                    '.form-control.is-valid, .was-validated .form-control:valid': 'border-color: #28a745;padding-right: calc(1.5em + .75rem); background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHZpZXdCb3g9JzAgMCA4IDgnPjxwYXRoIGZpbGw9JyMyOGE3NDUnIGQ9J00yLjMgNi43M0wuNiA0LjUzYy0uNC0xLjA0LjQ2LTEuNCAxLjEtLjhsMS4xIDEuNCAzLjQtMy44Yy42LS42MyAxLjYtLjI3IDEuMi43bC00IDQuNmMtLjQzLjUtLjguNC0xLjEuMXonLz48L3N2Zz4=);background-repeat: no-repeat; background-position: 98% 50%; background-size: calc(.75em + .375rem) calc(.75em + .375rem);',
                                    '.form-control.is-invalid, .was-validated .form-control:invalid': '  border-color: #dc3545;    padding-right: calc(1.5em + .75rem);background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIGZpbGw9JyNkYzM1NDUnIHZpZXdCb3g9Jy0yIC0yIDcgNyc+PHBhdGggc3Ryb2tlPScjZGMzNTQ1JyBkPSdNMCAwbDMgM20wLTNMMCAzJy8+PGNpcmNsZSByPScuNScvPjxjaXJjbGUgY3g9JzMnIHI9Jy41Jy8+PGNpcmNsZSBjeT0nMycgcj0nLjUnLz48Y2lyY2xlIGN4PSczJyBjeT0nMycgcj0nLjUnLz48L3N2Zz4=);background-repeat: no-repeat;background-position: 98% 50%;    background-size: calc(.75em + .375rem) calc(.75em + .375rem);',
                                    '.form-control.is-valid.is-pristine': 'background-image: unset; border-color:#CFCFCF;'
                                  }
            }
            else if (type === 'sample2'){
                template_style = {
                                    'body': 'background-color: transparent',
                                    '.logo': 'right: 2em;',
                                    'input.form-control': ' border-color:#CFCFCF;border-radius:8px;height:46px;padding-right:0px;',
                                    '.form-control.is-valid, .was-validated .form-control:valid': 'border-color: #28a745;padding-right: calc(1.5em + .75rem); background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHZpZXdCb3g9JzAgMCA4IDgnPjxwYXRoIGZpbGw9JyMyOGE3NDUnIGQ9J00yLjMgNi43M0wuNiA0LjUzYy0uNC0xLjA0LjQ2LTEuNCAxLjEtLjhsMS4xIDEuNCAzLjQtMy44Yy42LS42MyAxLjYtLjI3IDEuMi43bC00IDQuNmMtLjQzLjUtLjguNC0xLjEuMXonLz48L3N2Zz4=);background-repeat: no-repeat; background-position: 98% 50%; background-size: calc(.75em + .375rem) calc(.75em + .375rem);',
                                    '.form-control.is-invalid, .was-validated .form-control:invalid': '  border-color: #dc3545;    padding-right: calc(1.5em + .75rem);background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIGZpbGw9JyNkYzM1NDUnIHZpZXdCb3g9Jy0yIC0yIDcgNyc+PHBhdGggc3Ryb2tlPScjZGMzNTQ1JyBkPSdNMCAwbDMgM20wLTNMMCAzJy8+PGNpcmNsZSByPScuNScvPjxjaXJjbGUgY3g9JzMnIHI9Jy41Jy8+PGNpcmNsZSBjeT0nMycgcj0nLjUnLz48Y2lyY2xlIGN4PSczJyBjeT0nMycgcj0nLjUnLz48L3N2Zz4=);background-repeat: no-repeat;background-position: 98% 50%;    background-size: calc(.75em + .375rem) calc(.75em + .375rem);',
                                    '.form-control.is-valid.is-pristine': 'background-image: unset; border-color:#CFCFCF;'
                                  }
            }
            else if (type === 'sample3'){
                template_style = {
                                    ".form-control": "background: transparent; border: none; border-radius: unset; padding-left: 0; padding-right: 0; color: #fff; font-weight: 500",
                                    ".card-number": "padding-left: 45px",
                                    ".expiration": "text-align: center",
                                    ".cvc": "text-align: right",
                                    ".logo": "right: unset; left: 0",
                                    "::placeholder": "color: #99ccdd",
                                    '.form-control.is-valid, .was-validated .form-control:valid': 'border-color: #28a745;padding-right: calc(1.5em + .75rem); background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHZpZXdCb3g9JzAgMCA4IDgnPjxwYXRoIGZpbGw9JyMyOGE3NDUnIGQ9J00yLjMgNi43M0wuNiA0LjUzYy0uNC0xLjA0LjQ2LTEuNCAxLjEtLjhsMS4xIDEuNCAzLjQtMy44Yy42LS42MyAxLjYtLjI3IDEuMi43bC00IDQuNmMtLjQzLjUtLjguNC0xLjEuMXonLz48L3N2Zz4=);background-repeat: no-repeat; background-position: 98% 50%; background-size: calc(.75em + .375rem) calc(.75em + .375rem);',
                                    '.form-control.is-invalid, .was-validated .form-control:invalid': '  border-color: #dc3545;    padding-right: calc(1.5em + .75rem);background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIGZpbGw9JyNkYzM1NDUnIHZpZXdCb3g9Jy0yIC0yIDcgNyc+PHBhdGggc3Ryb2tlPScjZGMzNTQ1JyBkPSdNMCAwbDMgM20wLTNMMCAzJy8+PGNpcmNsZSByPScuNScvPjxjaXJjbGUgY3g9JzMnIHI9Jy41Jy8+PGNpcmNsZSBjeT0nMycgcj0nLjUnLz48Y2lyY2xlIGN4PSczJyBjeT0nMycgcj0nLjUnLz48L3N2Zz4=);background-repeat: no-repeat;background-position: 98% 50%;    background-size: calc(.75em + .375rem) calc(.75em + .375rem);',
                                    '.form-control.is-valid.is-pristine': 'background-image: unset; border-color:#CFCFCF;'
                                  }
            }
            return template_style;
        },
        
        showSaferpayInterface: function () {
            var self = this;
            if ($.blockUI) {
                var parsed_provider_form = $('form[provider="saferpay"]');
                var saferpay_fields_form_type = parsed_provider_form.find('input[name="saferpay_fields_form_type"]').val();
                if (saferpay_fields_form_type === 'sample1'){
                    var content = $(Qweb.render("saferpay_interface.saferpay_fields_interface_simple"));
                }
                if (saferpay_fields_form_type === 'sample2'){
                    var content = $(Qweb.render("saferpay_interface.saferpay_fields_interface_form"));
                }
                if (saferpay_fields_form_type === 'sample3'){
                    var content = $(Qweb.render("saferpay_interface.saferpay_fields_interface_card"));
                }
                var form_data = this.getFormData(parsed_provider_form);
                var txn_vals = {}
                var fields_token = null;
                var save_card_token = null;
                self.dialog = new Dialog(self, {
                    title: _t("Powered By Saferpay"),
                    subtitle: _t(""),
                    $content: content,
                    dialogClass: 'saferpay-payment-form-modal',
                    fullscreen: true,
                });
                self.dialog.opened().then(function () {
                    self.dialog.$modal.find('.modal-content').css({
                        'height': 600,
                    });
                    self.dialog.$modal.find('.modal-footer .btn-primary').hide();
                    self.dialog.$modal.find('#token').hide();
                    self.dialog.$modal.find('span').hide();
                    var saferpay_fields_url = parsed_provider_form.find('input[name="saferpay_fields_url"]').val();
                    var saferpay_fields_api_key = parsed_provider_form.find('input[name="saferpay_fields_api_key"]').val();
                    var saferpay_fields_api_url = parsed_provider_form.find('input[name="saferpay_fields_api_url"]').val();
                    var saferpay_fields_css_url = parsed_provider_form.find('input[name="saferpay_fields_css_url"]').val();
                    var saferpay_fields_form_type = parsed_provider_form.find('input[name="saferpay_fields_form_type"]').val();
                    var saferpay_fields_form_type = parsed_provider_form.find('input[name="saferpay_fields_form_type"]').val();
                    var payment_method_code = parsed_provider_form.find('input[name="saferpay_payment_method_code"]').val();
                    var saferpay_payment_method_code = payment_method_code.split(" ");
                    var referrer_path = parsed_provider_form.find('input[name="referrer_path"]').val();
                    var labels = {
              			  cardnumber: 'Card number' ,
              			  holdername: 'Name on card' ,
              			  expiration: 'Valid thru' ,
              			  cvc: 'Security number',
              			  pay: 'Pay now',
              			  invalid: 'Invalid input',
              			  empty: 'Input cannot be empty',
              			  unsupported: 'The card is not permitted',
              			  expired: 'The card is expired'
              			};
                    
                    $.getScript(saferpay_fields_url, function(data, textStatus, jqxhr) {
                    }).done(function( script, textStatus ) {
                    		
                    		/*var lang = 'de';
                    		
                    		function setLanguage(l) {
                  			  lang = l;
                  			  Array.from(document.querySelectorAll('[data-i18n]')).forEach(function (e) {
                  			  	var key = e.getAttribute('data-i18n');
                  			    e.innerHTML = labels[key][lang];
                  			  });
                  			  Array.from(document.querySelectorAll('[data-lang]')).forEach(function(a) {
                  			  	a.classList.toggle('active', a.getAttribute('data-lang') === lang);
                  			  });
                  			}
                    		setLanguage('de');
                    		Array.from(document.querySelectorAll('[data-lang]')).forEach(function(a) {
	                   			 console.log(a);
	                   				a.onclick = function () { setLanguage(a.getAttribute('data-lang')); };
	                   		});*/
    
                            try {

                                self.saferpayfieldshandler = SaferpayFields.init({
                                    apiKey: saferpay_fields_api_key,
                                    url: saferpay_fields_api_url,
                                    paymentMethods: saferpay_payment_method_code,
                                    placeholders: self.get_place_holder(saferpay_fields_form_type),
                                    style : self.get_template_style(saferpay_fields_form_type),
                                    cssUrl: saferpay_fields_css_url,
                                    onSuccess: function() {
                                        var saferpay_fields_button = document.getElementById('submit');
                                        saferpay_fields_button.removeAttribute('disabled');
                                    },
                                    
                                    onValidated: function(evt) {
                                    	console.log('onValidated', evt);
                                        var elemIds = {
                                        		cardnumber: 'card-number-help',
                                        		expiration: 'expiration-help',
                                        		holdername: 'holder-name-help',
                                        		cvc: 'cvc-help'
                                        }

                                        if (elemIds[evt.fieldType]) {
                                        	var elem = document.getElementById(elemIds[evt.fieldType]);
                                        	if (evt.isValid) {
                                        		elem.innerHTML = '';
                                        	} else {
                                        		var key = evt.reason || 'invalid';
                                        		elem.setAttribute('data-i18n', key);
                                        		elem.innerHTML = labels[key];
                                        	}
                                        }
                                    },
                                    onFocus: function(evt) {
                                        console.log("Focus in " + evt.fieldType + " field...");
                                        if (saferpay_fields_form_type ===  'sample3'){
                                            var pictureCard = document.querySelector("#picture .card");
                                            if (evt.fieldType === 'cvc') {
                                              pictureCard.classList.add('show-back');
                                              pictureCard.classList.remove('cardnumber');
                                              pictureCard.classList.remove('expiration');
                                              pictureCard.classList.add('cvc');
                                            } else if (evt.fieldType === 'cardnumber') {
                                              pictureCard.classList.remove('show-back');
                                              pictureCard.classList.add('cardnumber');
                                              pictureCard.classList.remove('expiration');
                                              pictureCard.classList.remove('cvc');
                                            } else if (evt.fieldType === 'expiration') {
                                              pictureCard.classList.remove('show-back');
                                              pictureCard.classList.remove('cardnumber');
                                              pictureCard.classList.add('expiration');
                                              pictureCard.classList.remove('cvc');
                                            }
                                        }
                                      },
                                      onBlur: function (evt) {
                                        if (saferpay_fields_form_type ===  'sample3'){
                                            console.log("Blur in " + evt.fieldType + " field...");
                                        }
                                      },
                                      onError: function(evt) {
                                          var error_message = evt.message;
                                          self.dialog.$('ul.saferpay-payment-errors').css({'color':'red', 'margin-top': '-10%', 'font-size': '14px'});
                                          self.dialog.$('ul.saferpay-payment-errors').text(error_message);
                                          console.log('Error in initialization: ' + error_message);
                                      },
                                });
                            } catch (error) {
                                self.dialog.$('ul.saferpay-payment-errors').css({'color':'red', 'margin-top': '-10%', 'font-size': '14px'});
                                self.dialog.$('ul.saferpay-payment-errors').text(error);
                                console.log('Error in initialization: ' + error);
                            }



                        try {
                            self.dialog.$('button.saferpay-submit').click(_.bind(function(e) {
                                SaferpayFields.submit({
                                        onSuccess: function(evt) {
                                          fields_token = evt.token;                                      
                                          if(fields_token){
                                              		ajax.jsonRpc("/payment/saferpay/trans_url", 'call', {
                                              			txn_vals: txn_vals,
                                              			form_data: form_data,
                                              			fields_token: fields_token,
                                              			save_card_token: save_card_token,
                                              		})
                                                    .then(function (result) {
                                                    	console.log("result----->>",result);
                                                        if (result) {
                                                            var redirect_url = result['redirect_url']
                                                            
                                                            if (redirect_url){
                                                                content = '<iframe width="100%" height="100%" frameborder="0" src="' + redirect_url + '"></iframe>'; 
                                                                if (saferpay_fields_form_type === 'sample1') {
                                                                    self.dialog.$content.find(".saferpay_fields_interface_simple").html(content);
                                                                }                      
                                                                if (saferpay_fields_form_type === 'sample2') {
                                                                    self.dialog.$content.find(".saferpay_fields_interface_form").html(content);
                                                                }
                                                                if (saferpay_fields_form_type === 'sample3') {
                                                                    self.dialog.$content.find(".saferpay_fields_interface_card").html(content);
                                                                }
                                                            } 
                                                            else{
                                                            	console.log("result['direct_url']----->>",result['direct_url']);
                                                                self.dialog.close();
                                                                window.location = result['direct_url'];
                                                            }
                                                        }
                                                    }, function () {
                                                    	content = "<li>" + evt + "</li>"
                                                    	console.log("ee---->>",e);
                                                    	console.log("evt------>>",evt);
                                                    	self.dialog.$('ul.saferpay-payment-errors').text(e);
                                                        /*self.displayError(
                                                            _t('Server Error'),
                                                            _t("We are not able to add your payment method at the moment.")
                                                        );*/
                                                    })/*.fail(function (error, event) {
                                                    	self.dialog.$('ul.saferpay-payment-errors').css({
                                                            'color': "red",
                                                        });
                                                    	self.dialog.$('ul.saferpay-payment-errors').text(error.data.message);
                                                    })*/;
                                            }
                                        
                                        },
                                        onError: function(evt) {
                                            self.dialog.$('ul.saferpay-payment-errors').css({'color':'red', 'margin-top': '-10%', 'font-size': '14px'});
                                            self.dialog.$('ul.saferpay-payment-errors').text(evt.message);
                                            console.log('Error in submit: ' + evt.message);
                                        }
                                    }); 
                            }, this));
                        } catch (error) {
                            self.dialog.$('ul.saferpay-payment-errors').css({'color':'red', 'margin-top': '-10%', 'font-size': '14px'});
                            self.dialog.$('ul.saferpay-payment-errors').text(error);
                            console.log('Error in submit: ' + error);

                        }
                        self.dialog.$modal.find('button.close').on('click', function(e){
                            e.preventDefault();
                            e.stopPropagation();
                            e.stopImmediatePropagation();
                            if (referrer_path){
                                window.location.href = referrer_path;
                            }else{
                                location.reload();
                            }
                        });

                    }).fail(function( jqxhr, settings, exception ) {
                        self.dialog.$('ul.saferpay-payment-errors').text( "Triggered ajaxError handler." );
                    });
                });
                $.unblockUI();
                self.dialog.open();                
            }
        },        
        getFormData: function ($form) {
            var unindexed_array = $form.serializeArray();
            var indexed_array = {};

            $.map(unindexed_array, function (n, i) {
                indexed_array[n.name] = n.value;
            });
            return indexed_array;
        },
        showContent: function (xmlid, render_values) {
            var html = Qweb.render(xmlid, render_values);
            this.$el.find('.o_saferpay_interface_content').html(html);
        },
        showLoading: function () {
            var msg = _t("We are processing your payments, please wait ...");
            $.blockUI({
                'message': '<h2 class="text-white"><img src="/web/static/src/img/spin.png" class="fa-pulse"/>' +
                    '    <br />' + msg +
                    '</h2>'
            });
        },
    });
});
