odoo.define('payment_saferpay.payment_form', function (require) {
    "use strict";
    
    var core = require('web.core');
    var ajax = require('web.ajax');
    var rpc = require("web.rpc");
    var Dialog = require("web.Dialog");
    var payment_form = require('payment.payment_form');
    var saferpay_fields = require('payment_saferpay.saferpay_fields_interface');
    var Qweb = core.qweb;
    var _t = core._t;
    
    payment_form.include({    
        xmlDependencies: ['/payment_saferpay/static/src/xml/saferpay_update_alias_expiry.xml', '/payment_saferpay/static/src/xml/saferpay_fields_interface.xml'],
        events: _.extend({
            'click #o_saferpay_refund_card_data_form': 'saferpay_refund_card_data',
            'click button[name="update_pm"]': 'updatePmEvent',
            }, payment_form.prototype.events),        
        getAcquirerName: function (element) {
            return $(element).data('provider');
        },
        getSaferpayCode: function (element) {
            return element.dataset.saferpayCode;
        },    
        getAcquirerId: function (element) {
            return $(element).context['dataset']['acquirerId'];
        },      
        getSaferpaySaveCard: function (element) {
            return $(element).context['dataset']['save_card'];
        },         
        getAcquirerIdInterfaceType: function (element) {
            return $(element).data('interface-type');
        },
        getSaferpayFieldsApiKey: function (element) {
            return $(element).data('api-key');
        },
        getSaferpayFieldsApiUrl: function (element) {
            return $(element).data('api-url');
        },
        getSaferpayFieldsCssUrl: function (element) {
            return $(element).data('css-url');
        },
        getSaferpayFieldsFormType: function (element) {
            return $(element).data('form-type');
        },
        getPaymentMethodCode: function (element) {
            return $(element).data('method-code');
        },
        getSaferpayFieldsUrl: function (element) {
            return $(element).data('fields_url');
        },
        getAcquirerIdPaymentFlow: function (element) {
            return $(element).data('payment-flow');
        },
        
        get_place_holder: function () {
            var place_holder = {
                                holdername: 'holder name',
                                cardnumber: '0000 0000 0000 0000',
                                expiration: 'MM/YYYY',
                                cvc: '000'
                                }
            return place_holder
        },
        get_template_style: function () {
            var template_style = {
                            'body': 'background-color: transparent',
                            '.logo': 'right: 2em;',
                            'input.form-control': ' border-color:#CFCFCF;border-radius:8px;height:46px;padding-right:0px;',
                            '.form-control.is-valid, .was-validated .form-control:valid': 'border-color: #28a745;padding-right: calc(1.5em + .75rem); background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHZpZXdCb3g9JzAgMCA4IDgnPjxwYXRoIGZpbGw9JyMyOGE3NDUnIGQ9J00yLjMgNi43M0wuNiA0LjUzYy0uNC0xLjA0LjQ2LTEuNCAxLjEtLjhsMS4xIDEuNCAzLjQtMy44Yy42LS42MyAxLjYtLjI3IDEuMi43bC00IDQuNmMtLjQzLjUtLjguNC0xLjEuMXonLz48L3N2Zz4=);background-repeat: no-repeat; background-position: 98% 50%; background-size: calc(.75em + .375rem) calc(.75em + .375rem);',
                            '.form-control.is-invalid, .was-validated .form-control:invalid': '  border-color: #dc3545;    padding-right: calc(1.5em + .75rem);background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIGZpbGw9JyNkYzM1NDUnIHZpZXdCb3g9Jy0yIC0yIDcgNyc+PHBhdGggc3Ryb2tlPScjZGMzNTQ1JyBkPSdNMCAwbDMgM20wLTNMMCAzJy8+PGNpcmNsZSByPScuNScvPjxjaXJjbGUgY3g9JzMnIHI9Jy41Jy8+PGNpcmNsZSBjeT0nMycgcj0nLjUnLz48Y2lyY2xlIGN4PSczJyBjeT0nMycgcj0nLjUnLz48L3N2Zz4=);background-repeat: no-repeat;background-position: 98% 50%;    background-size: calc(.75em + .375rem) calc(.75em + .375rem);',
                            '.form-control.is-valid.is-pristine': 'background-image: unset; border-color:#CFCFCF;'
                          }
            return template_style;
        },
        payEvent: function (ev) {
            ev.preventDefault();
            var form = this.el;
            var checked_radio = this.$('input[type="radio"]:checked');
            var provider = checked_radio.data('provider');
            var s2s_payment = checked_radio.data('s2s-payment');
            var self = this;
            var acquirer_id = this.getAcquirerIdFromRadio(checked_radio);
            var acquirer_form = this.$('#o_payment_form_acq_' + acquirer_id);
            if (checked_radio.length === 1) {
                checked_radio = checked_radio[0];
                var form_save_token = acquirer_form.find('input[name="o_payment_form_save_token"]').prop('checked');                
                if (s2s_payment && (provider === 'saferpay')){
                    var $tx_url = this.$el.find('input[name="prepare_tx_url"]');
                    // if there's a prepare tx url set
                    if ($tx_url.length === 1) {
                        // then we call the route to prepare the transaction
                        return ajax.jsonRpc($tx_url[0].value, 'call', {
                            'acquirer_id': parseInt(acquirer_id),
                            'save_token': form_save_token,
                            'access_token': self.options.accessToken,
                            'success_url': self.options.successUrl,
                            'error_url': self.options.errorUrl,
                            'callback_method': self.options.callbackMethod,
                            'order_id': self.options.orderId,
                        }).then(function (result) {
                            if (result) {
                                // if the server sent us the html form, we create a form element
                                var newForm = document.createElement('form');
                                newForm.setAttribute("method", "post"); // set it to post
                                newForm.setAttribute("provider", checked_radio.dataset.provider);
                                newForm.hidden = true; // hide it
                                newForm.innerHTML = result; // put the html sent by the server inside the form
                                var action_url = $(newForm).find('input[name="data_set"]').data('actionUrl');
                                newForm.setAttribute("action", action_url); // set the action url
                                $(document.getElementsByTagName('body')[0]).append(newForm); // append the form to the body
                                $(newForm).find('input[data-remove-me]').remove(); // remove all the input that should be removed
                                if(action_url) {
                                    newForm.submit(); // and finally submit the form
                                    return $.Deferred();
                                }
                            }
                            else {
                            	Dialog.alert(this, _t('We are not able to redirect you to the payment form.'), {
                                    title: _t('Warning'),
                                });
                                self.displayError(
                                    _t('Server Error'),
                                    _t("We are not able to redirect you to the payment form.")
                                );
                            }
                        }).fail(function (error, event) {
                        	Dialog.alert(this, _t('We are not able to redirect you to the payment form.\n') +
                                    error.data.message, {
                                title: _t('Warning'),
                            });
                            self.displayError(
                                _t('Server Error'),
                                _t("We are not able to redirect you to the payment form. ") +
                                   error.data.message
                            );
                        });
                    }
                    
                }                
            }
            self._super(ev);
        },        
        addPmEvent: function (ev) {
            /*ev.stopPropagation();*/
            ev.preventDefault();
            var checked_radio = this.$('input[type="radio"]:checked');
            var self = this;

            var acquirer_id = this.getAcquirerIdFromRadio(checked_radio);
            var save_from_saferpay = this.isFormPaymentRadio(checked_radio);
            var acquirer_form = this.$('#o_payment_add_token_acq_' + acquirer_id);
            var partner_id = this.options.partnerId;
            var interface_type = this.getAcquirerIdInterfaceType(checked_radio);
            var fields_token = null;
            if (acquirer_form){
                var inputs_form = $('input', acquirer_form);
                var form_data = this.getFormData(inputs_form);
                var ds = $('input[name="data_set"]', acquirer_form)[0];
                if (interface_type != 'saferpay_fields'){
                	if(save_from_saferpay){
                        rpc.query({
                            model: 'payment.acquirer',
                            method: 'new_alias_insert',
                            args: [acquirer_id, partner_id, fields_token],
                        })
                        .then(function (result) {
                            if ( result === false){
                                Dialog.alert(this, _t('We are not able to register your card at this moment.'), {
                                        title: _t('Warning'),
                                    });
                            }
                            else{
                                    window.location = result;
                            }
                        }, function () {
                            self.displayError(
                                _t('Server Error'),
                                _t("We are not able to add your payment method at the moment.")
                            );
                        });
                    }
                    else{
                        rpc.query({
                            model: 'payment.acquirer',
                            method: 'register_new_card',
                            args: [form_data],
                        })
                        .then(function (result) {
                            if ( result === true) {
                                if (form_data.return_url) {
                                    window.location = form_data.return_url;
                                }
                                else {
                                    Dialog.alert(this, _t('We are not able to register your card at this moment.'), {
                                        title: _t('Warning'),
                                    });
                                    /*window.location.reload();*/
                                }
                            }
                        }, function () {
                            self.displayError(
                                _t('Server Error'),
                                _t("We are not able to add your payment method at the moment.")
                            );
                        });
                    }
                }
                if (interface_type == 'saferpay_fields'){
                	var content = $(Qweb.render("saferpay_interface.saferpay_fields_interface_save_card"));
                	var parsed_provider_form = this.$('form[provider="saferpay"]');
                	var saferpay_fields_url = this.getSaferpayFieldsUrl(checked_radio);
                	var saferpay_fields_api_key = this.getSaferpayFieldsApiKey(checked_radio);
                    var saferpay_fields_api_url = this.getSaferpayFieldsApiUrl(checked_radio);
                    var saferpay_fields_css_url = this.getSaferpayFieldsCssUrl(checked_radio);
                    var saferpay_fields_form_type = this.getSaferpayFieldsFormType(checked_radio);
                    var payment_method_code = this.getPaymentMethodCode(checked_radio);
                    var saferpay_payment_method_code = payment_method_code.split(" ");
                	self.dialog = new Dialog(self, {
                        title: _t("Powered By Saferpay"),
                        subtitle: _t(""),
                        $content: content,
                        dialogClass: 'saferpay-payment-form-modal',
                        fullscreen: true,
                    });
                	self.dialog.opened().then(function () {
                        self.dialog.$modal.find('.modal-content').css({
                            'height': 600,
                        });
                        self.dialog.$modal.find('.modal-footer .btn-primary').hide();
                        self.dialog.$modal.find('#token').hide();
                        self.dialog.$modal.find('span').hide();
                        var referrer_path = $('input[name="return_url"]').val();
                        console.log("eeeeeee------>>",referrer_path, parsed_provider_form, saferpay_payment_method_code);
                        
                        var labels = {
                  			  cardnumber: 'Card number' ,
                  			  holdername: 'Name on card' ,
                  			  expiration: 'Valid thru' ,
                  			  cvc: 'Security number',
                  			  pay: 'Pay now',
                  			  invalid: 'Invalid input',
                  			  empty: 'Input cannot be empty',
                  			  unsupported: 'The card is not permitted',
                  			  expired: 'The card is expired'
                  			};
                        $.getScript(saferpay_fields_url, function(data, textStatus, jqxhr) {
                        }).done(function( script, textStatus ) {
                        	self.saferpayfieldshandler = SaferpayFields.init({
                                apiKey: saferpay_fields_api_key,
                                url: saferpay_fields_api_url,
                                paymentMethods: saferpay_payment_method_code,
                                placeholders: self.get_place_holder(saferpay_fields_form_type),
                                style : self.get_template_style(saferpay_fields_form_type),
                                cssUrl: saferpay_fields_css_url,
                                onSuccess: function() {
                                    var saferpay_fields_button = document.getElementById('submit');
                                    saferpay_fields_button.removeAttribute('disabled');
                                },
                        	});
                        	self.dialog.$('button.saferpay-submit').click(_.bind(function(e) {
                                SaferpayFields.submit({
                                        onSuccess: function(evt) {
                                          fields_token = evt.token;
                                          if(fields_token){
                                        	  rpc.query({
                                                        model: 'payment.acquirer',
                                                        method: 'new_alias_insert',
                                                        args: [acquirer_id, partner_id, fields_token],
                                                    })
                                                    .then(function (result) {
									                    if ( result === false){
									                        Dialog.alert(this, _t('We are not able to register your card at this moment.'), {
									                                title: _t('Warning'),
									                            });
									                    }
									                    else{
									                    	if (result === true){
									                    		self.dialog.close();
									                    		window.location.reload();
									                    	}
									                    	else{
//									                    		window.location = result;
									                            content = '<iframe width="100%" height="100%" frameborder="0" src="' + result + '"></iframe>'; 
                                                                self.dialog.$content.find(".saferpay_fields_interface_save_card").html(content);
									                    	}
									                    }
									                }, function () {
									                    self.displayError(
									                        _t('Server Error'),
									                        _t("We are not able to add your payment method at the moment.")
									                    );
									                });
                                                    
                                                    
                                                    /*.then(function (result) {
                                                    	
                                                        if (result) {
                                                            var redirect_url = result['redirect_url']
                                                            
                                                            if (redirect_url){
                                                                content = '<iframe width="100%" height="100%" frameborder="0" src="' + redirect_url + '"></iframe>'; 
                                                                self.dialog.$content.find(".saferpay_fields_interface_save_card").html(content);
                                                            } 
                                                            else{
                                                                self.dialog.close();
                                                                window.location = result['direct_url'];
                                                            }
                                                        }
                                                    }, function () {
                                                        self.displayError(
                                                            _t('Server Error'),
                                                            _t("We are not able to add your payment method at the moment.")
                                                        );
                                                    });*/
                                            }
                                        
                                        },
                                        onError: function(evt) {
                                          console.log('Error in submit: ' + evt.message);
                                        }
                                    }); 
                            }, this));
                        	self.dialog.$modal.find('button.close').on('click', function(e){
                                e.preventDefault();
                                e.stopPropagation();
                                e.stopImmediatePropagation();
                                if (referrer_path){
                                    window.location.href = referrer_path;
                                }else{
                                    location.reload();
                                }
                            });
                        }).fail(function( jqxhr, settings, exception ) {
                            self.dialog.$('ul.saferpay-payment-errors').text( "Triggered ajaxError handler." );
                        });
                	});
                	self.dialog.open(); 
                }
                
            }
            this._super(ev);
        },
        getMethodIdFromRadio: function (element) {
            return $(element).data('saferpay-method-id');
        },
        isSavedPaymentRadio: function (element) {
            if ($(element).data('save_card')){
                return 'True';
                }
        },        
        isNewPaymentRadio: function (element) {
            if (($(element).data('mode') === 'payment') && ($(element).data('provider') === 'saferpay')) {
                return $(element).data('s2s-payment') === 'False';
            }
            else{
                this._super();
            }
        },        
        updateNewPaymentDisplayStatus: function (){
            this._super();
            var checked_radio = this.$('input[type="radio"]:checked');
            var provider = checked_radio.data('provider');
            var mode = checked_radio.data('mode');
            var payment_flow = checked_radio.data('payment-flow')
            // we hide all the acquirers form
            this.$('[id*="o_payment_add_token_acq_"]').addClass('d-none');
            this.$('[id*="o_payment_form_acq_"]').addClass('d-none');
            
            if (checked_radio.length !== 1) {
                return;
            }
            checked_radio = checked_radio[0];
            var acquirer_id = this.getAcquirerIdFromRadio(checked_radio);

            // if we clicked on an add new payment radio, display its form
            if (payment_flow == 's2s') {
                if ((provider === 'saferpay') && (mode === 'payment')){
    
                    this.$('#o_payment_form_acq_' + acquirer_id).removeClass('d-none');
                    this.$('#o_payment_add_token_acq_' + acquirer_id).addClass('d-none');
                }
                else {
                    this.$('#o_payment_form_acq_' + acquirer_id).addClass('d-none');
                    this.$('#o_payment_add_token_acq_' + acquirer_id).removeClass('d-none');
                }
            }
            else if (this.isFormPaymentRadio(checked_radio)) {
                this.$('#o_payment_form_acq_' + acquirer_id).removeClass('d-none');
                this.$('#o_payment_add_token_acq_' + acquirer_id).addClass('d-none');
            }
            
        },
        // event handler when clicking on a button to Update a payment method
        updatePmEvent: function (ev) {
            // ev.stopPropagation();
            ev.preventDefault();
            var self = this;
            var pm_id = parseInt(ev.target.value);
            var content = $(Qweb.render('saferpay_update_alias_expiry'));
            self.dialog = new Dialog(self, {
                title: _t("Powered By Saferpay"),
                subtitle: _t(""),
                $content: content
              });            
            self.dialog.opened().then(function () {
                //self.dialog.$('input#cc_expiry').attr('placeholder', _t("MM/YYYY"));
				self.dialog.$('input#cc_expiry').datepicker({
					dateFormat: 'mm/yy',
				    changeMonth: true,
				    changeYear: true,
				    showButtonPanel: true,
				    showOn: "both",
				    yearRange: "-5:+20",
				    onClose: function(dateText, inst) { 
				    	self.dialog.$('input#cc_expiry').datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
		            }
				});
                self.dialog.$modal.find('.modal-footer .btn-primary').hide();
				self.dialog.$modal.find('.modal-content').css({
                        'height': '100%'
                    });
				self.dialog.$modal.find('.modal-body').css({
                        'height': '100%'
                    });
                self.dialog.$modal.find('.modal-footer .btn-primary').hide();  
                self.dialog.$('button.saferpay-update-alias').click(_.bind(function(e) {
                    var token = self.dialog.$('input#cc_expiry').val();
                     rpc.query({
                        model: 'payment.token',
                        method: 'update_alias_expiry',
                        args: [pm_id, token],
                    })
                    .then(function (result) {
                        self.dialog.close();
                        if ( result === false) {
                            Dialog.alert(this, _t('We are not able to update your card at this moment.'), {
                                title: _t('Warning'),
                            });
                        }
                    }, function () {
                        self.displayError(
                            _t('Server Error'),
                            _t("We are not able to update your card at this moment.")
                        );
                    });
                }, this));            

            }).guardedCatch(function (error) {
                error.event.preventDefault();
                self.dialog.$('ul.saferpay-payment-errors').text( "Triggered ajaxError handler." );
            });            
            self.dialog.open();
        },

        getPmTypeFromRadio: function (element) {
            return $(element).data('pm_type');
        },
        getInvoiceFromRadio: function (element) {
            return $(element).data('invoice_id');
        },
        getReturnUrlFromRadio: function (element) {
            return $(element).data('return_url');
        },
        saferpay_refund_card_data: function (ev) {
            ev.stopPropagation();
            ev.preventDefault();
            var checked_radio = this.$('input[type="radio"]:checked');
            var pm_type = this.getPmTypeFromRadio(checked_radio);
            var return_url = this.getReturnUrlFromRadio(checked_radio);
            if (pm_type == 'token') {
                var token_id = parseInt(checked_radio[0].value);
                var invoice_id = parseInt(this.getInvoiceFromRadio(checked_radio));

                rpc.query({
                    model: 'account.move',
                    method: 'action_saferpay_direct_refund',
                    args: [invoice_id, token_id],
                })
                .then(function (result) {
                    
                    window.location = return_url;
                    if ( result === true) {
                        if (form_data.return_url) {
                            window.location = form_data.return_url;
                        }
                        else {
                            window.location.reload();
                        }
                    }
                }, function () {
                    self.displayError(
                        _t('Server Error'),
                        _t("We are not able to refund your payment at the moment.")
                    );
                });
            } else {

                var acquirer_id = this.getAcquirerIdFromRadio(checked_radio);
                var acquirer_form = this.$('#o_payment_form_acq_' + acquirer_id)
                var inputs_form = $('input', acquirer_form);
                var form_data = this.getFormData(inputs_form);
                if(form_data){
                        rpc.query({
                            model: 'account.move',
                            method: 'action_saferpay_direct_card_refund',
                            args: [form_data],
                        })
                        .then(function (result) {
                            window.location = form_data.return_url;
                            if ( result === true) {
                                if (form_data.return_url) {
                                    window.location = form_data.return_url;
                                }
                                else {
                                    window.location.reload();
                                }
                            }
                        }, function () {
                            self.displayError(
                                _t('Server Error'),
                                _t("We are not able to refund your payment at the moment.")
                            );
                        });
                }
            }
        },      
    
    });
    
});
