# -*- coding: utf-8 -*-
#################################################################################
# Author      : PIT Solutions AG. (<https://www.pitsolutions.ch/>)
# Copyright(c): 2019 - Present PIT Solutions AG.
# License URL : https://www.webshopextension.com/en/licence-agreement/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.webshopextension.com/en/licence-agreement/>
#################################################################################

import logging

from odoo import api, models, fields, tools, _

TIMEOUT = 20

PROVIDER_TYPE = {
    ('saferpay', _("Saferpay")),
}

class SaferpayAcquirerConfig(models.Model):
    _name = "saferpay.acquirer.config"
    _description = "Saferpay acquirer configuration details"
            
            
    name = fields.Char(required=True, translate=True, copy=False)
    description = fields.Html(translate=True)    
    acquirer_ids = fields.One2many('payment.acquirer', 'saferpay_config_id', 'Payment Acquirers') 
    active = fields.Boolean(default=True)     
    # environment = fields.Selection([
    #                                 ('test', 'Test'),
    #                                 ('prod', 'Production')
    #                                 ],default='test', required=True) 

    state = fields.Selection([
        ('disabled', 'Disabled'),
        ('enabled', 'Enabled'),
        ('test', 'Test Mode')], required=True, default='test', copy=False,
        help="""In test mode, a fake payment is processed through a test
             payment interface. This mode is advised when setting up the
             acquirer. Watch out, test and production modes require
             different credentials.""")       
    saferpay_customer_id = fields.Integer(string='Customer ID', groups='base.group_user', copy=False)
    saferpay_terminal_id = fields.Integer(string='Terminal ID', groups='base.group_user', copy=False)
    saferpay_api_username = fields.Char(string='API User Name', groups='base.group_user', copy=False)
    saferpay_api_password = fields.Char(string='API Password', groups='base.group_user', copy=False)
    saferpay_flds_api_key = fields.Char(string='API Key', groups='base.group_user', copy=False)
    send_status_email = fields.Boolean(default=True)
    liability_shift = fields.Selection([
                                        ('hold', 'Hold'), 
                                        ('cancel', 'Cancel')], default="hold", string="Liability Shift Behavior")
    needs_advanced_security = fields.Boolean(string="Need Extra Security", help="Select if needed advanced security.")
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id, copy=False, ondelete='restrict')
    saferpay_config_name = fields.Char('Payment Page Configuration Name')
    prod_url = fields.Text(string="Production Url", required=True)
    test_url = fields.Text(string="Test Url", required=True)
    need_cvc_token_txn = fields.Boolean("Need CVC form for Alias payment")
    
    
    
    
    def action_view_payment_acquirers(self):
        self.ensure_one()    
        context = dict(self.env.context or {})
        action = self.env.ref('payment.action_payment_acquirer')
        result = action.read()[0]
        acquirer_ids = self.mapped('acquirer_ids')
        if not acquirer_ids or len(acquirer_ids) > 1:
            result['domain'] = "[('id','in',%s)]" % (acquirer_ids.ids)
        elif len(acquirer_ids) == 1:
            res = self.env.ref('payment.acquirer_form', False)
            result['views'] = [(res and res.id or False, 'form')]
            result['res_id'] = acquirer_ids.id
        result.update({'context': context})
        return result

    
    def action_view_saferpay_acquirer_logging(self):
        self.ensure_one()    
        context = dict(self.env.context or {})
        action = self.env.ref('payment_saferpay.action_saferpay_acquirer_log')
        result = action.read()[0]
        context.update({
            'search_default_saferpay_config_id': self.ids
        })
        result.update({
                        'context': context, 
                    })
        return result
