# -*- coding: utf-8 -*-
#################################################################################
# Author      : PIT Solutions AG. (<https://www.pitsolutions.ch/>)
# Copyright(c): 2019 - Present PIT Solutions AG.
# License URL : https://www.webshopextension.com/en/licence-agreement/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.webshopextension.com/en/licence-agreement/>
#################################################################################

import logging
import requests
from pprint import pformat
from time import time
from datetime import datetime
import json
import base64
import http.client

from odoo import api, models, fields, tools, _
from odoo.http import request
from odoo.addons.payment.models.payment_acquirer import ValidationError

_logger = logging.getLogger(__name__)

TIMEOUT = 100

SAFERPAY_API_VERSION = '1.18'

SAFERPAY_FIELDS_VERSION = '1.3'


class PaymentAcquirerSaferpay(models.Model):
    _inherit = 'payment.acquirer'

    @api.constrains('interface_type','license_type')
    def _check_license_switching(self):
        if (self.interface_type == 'widget' or self.payment_flow == 's2s') and self.license_type != 'business':
            raise ValidationError(_('Transaction Interface can be used by business licensed acquirer.'))
        if self.interface_type == 'saferpay_fields' and self.license_type != 'business':
            raise ValidationError(_('Saferpay fields can be used by business licensed acquirer.'))
    
    @api.constrains('provider','interface_type','save_token')
    def _check_save_card_interface_swiching(self):
        if self.provider == 'saferpay' and self.interface_type == 'payment_page' and self.save_token != 'none':
            raise ValidationError(_("Save card option can't with PaymentPage Interface."))
    
    @api.constrains('payment_icon_ids','provider','interface_type')
    def _check_payment_icon_ids_swiching(self):
        icons_not_saferpay = [icon.id for icon in self.payment_icon_ids if not icon.is_saferpay]
        if self.provider == 'saferpay':
            if len(icons_not_saferpay) > 0:
                raise ValidationError(_("Please configure saferpay specific icons."))
            icons_not_saferpay_fields = [icon.id for icon in self.payment_icon_ids if not icon.support_saferpay_fields]
            if self.interface_type and self.interface_type == 'saferpay_fields' and len(icons_not_saferpay_fields) > 0:
                raise ValidationError(_("Please configure saferpay fields specific icons."))
    
    @api.model
    def saferpay_send_request(self, acquirer_id, type, uri, params={}, json_data={}, data={}, headers={}, txn_id=0):
        _logger.info(_("Requesting Saferpay Services \nAcquirer Id: %s\nType: %s\nURL: %s\nTxn Id: %s"), \
                                pformat(acquirer_id), \
                                pformat(type), \
                                pformat(uri), \
                                pformat(txn_id))
        
        acquirer_id = int(acquirer_id)
        acquirer = self.browse(acquirer_id)
        saferpay_config = acquirer.sudo().saferpay_config_id.sudo()
        ask_time = fields.Datetime.now()        
        result = {'ask_time': ask_time}
        response = {}
        request_uri = response_error = detail = color = saferpay_txn_ref = request_ip = referrer_path = ''
        request_browser = request_country_code = request_state_code = request_country_id = request_state_id = ''
        try:
            timestamp = str(int(time()))
            sfp_login = str(saferpay_config.saferpay_api_username)
            sfp_password = str(saferpay_config.saferpay_api_password)
            sfp_credential = ":".join([sfp_login, sfp_password])
            sfp_encode = sfp_credential.encode("utf-8")
            sfp_encoded_credentials = base64.b64encode(sfp_encode).decode("utf-8")
            authorization = " ".join(['Basic', sfp_encoded_credentials])
            customer_id = str(saferpay_config.saferpay_customer_id)
            terminal_id = str(saferpay_config.saferpay_terminal_id)
                      
            headers.update({
                            "Content-type": "application/json; charset=utf-8", \
                            "Accept": "application/json",
                            "Authorization": authorization,
                        })           
            acquirer_api_base_url = "".join([acquirer.saferpay_base_url, '/api'])
            request_uri = "".join([acquirer_api_base_url, uri])
            if 'Payment' in json_data:
                if 'OrderId' in json_data.get('Payment'):
                    json_data['Payment']['OrderId'] = json_data.get('Payment').get('OrderId').replace('/', '-')
            if type.upper() in ('GET', 'DELETE'):
                res = requests.request(type.lower(), request_uri, params=params, headers=headers, timeout=TIMEOUT)
            elif type.upper() in ('POST', 'PATCH', 'PUT'):
                res = requests.request(type.lower(), request_uri, params=params, data=data, json=json_data, headers=headers, timeout=TIMEOUT)
            else:
                raise Exception(_('Method not supported [%s] not in [GET, POST, PUT, PATCH or DELETE]!') % (type))
            res.raise_for_status()
            status = res.status_code  
            try:
                response = res.json()        
            except Exception as e:
                _logger.info(_("Error! saferpay_send_request json reterival %s") % (e,))
                res_data = res.request.body
                res_data = res.content
                res_data = res.text
                response = res_data
            try:
                ask_time = datetime.strptime(res.headers.get('date'), "%a, %d %b %Y %H:%M:%S %Z")
                result.update({'ask_time': ask_time})
            except Exception as e:
                _logger.info("Date time cannot retrieved from response header : %s" % (e,))
            if status in [200]:
                _logger.info(_("Successfully executed Saferpay Services \nAcquirer Id: %s\nType: %s\nURL: %s"), \
                                pformat(acquirer_id), \
                                pformat(type), \
                                pformat(uri))
                detail = 'OK (No error)'
                if 'Status' in response:
                    detail = detail + '<br/>' + 'Status:' + response['Status']
                if 'Transaction' in response:
                    if 'Status' in response['Transaction']:
                        detail = detail + '<br/>' + 'Status:' + response['Transaction']['Status']
                        saferpay_txn_ref = response['Transaction']['Id']
                color = 'green'

        except requests.HTTPError as error:
            status = error.response.status_code                       
            try:
                req = json.loads(error.request.body)
            except Exception as e:
                _logger.info(_("HTTPError! saferpay_send_request json reterival :: req %s") % (e,))
                req = error.request.body                 
            try:
                res = error.response.json()          
                err_behavior = res.get('Behavior', '')    
                err_name = res.get('ErrorName', '')              
                err_message = res.get('ErrorMessage', '')       
                err_desc = res.get('ErrorDetail', '')
                err_detail = {
                        'err_behavior': err_behavior,
                        'err_name': err_name,
                        'err_message': err_message,
                        'err_desc': err_desc,
                      }                
                result.update({'error': err_message, 'err_detail': err_detail})
            except Exception as e:
                _logger.info(_("HTTPError! saferpay_send_request :: res %s") % (e,))
                result.update({'error': _('Unknown Saferpay Error %s') % (e,)})
                res = {}            
            response = res
            response_error = res
            error_detail = isinstance(response, dict) and response.get('ErrorDetail', '') or ''
            err_log = "\n".join([
                                '\n', \
                                _('Requested URL: %s') % pformat(uri), \
                                _('Behavior: %s') % (pformat(response.get('Behavior'))), \
                                _('Error Name: %s') % pformat(response.get('ErrorName')), \
                                _('Error Message: %s') % pformat(response.get('ErrorMessage'))
                                ])
            safe_log = "".join([
                                _('<p>Behavior: %s</p>') % (pformat(response.get('Behavior'))), \
                                _('<p>Error Name: %s</p>') % pformat(response.get('ErrorName')), \
                                _('<p>Error Message: %s</p>') % pformat(response.get('ErrorMessage'))
                                ])
            if error_detail:
                err_log = "\n".join([
                                    err_log,
                                    _('Error Detail: %s') % pformat(error_detail)
                                    ])
                safe_log = "".join([
                                    safe_log,
                                    _('<p>Error Detail: %s</p>') % pformat(error_detail)
                                    ])
            log_title = _('Saferpay Error')
            err_log_sub_title = _(' while requesting Saferpay Services ')
            if status in [400]:
                log_title = _('Validation Error')
                color = 'rosybrown'
            elif status in [401]:
                log_title = _('Authentication of the request failed')
                color = 'chocolate'
            elif status in [402]:
                log_title = _('Requested action failed')
                color = 'brown'
            elif status in [403]:
                log_title = _('Access denied')
                color = 'tomato'
            elif status in [406]:
                log_title = _('Not acceptable (wrong accept header)')
                color = 'firebrick'
            elif status in [415]:
                log_title = _('Unsupported media type (wrong content-type header)')
                color = 'orange'
            elif status in [500]:
                log_title = _('Internal error')
                color = 'red'
            else:
                log_title = _('Error! Unknown Error')
                color = 'red'
            err_log = "".join([log_title, err_log_sub_title, err_log])
            detail = "".join(['<p>', log_title, '</p>', safe_log])
            _logger.info(err_log)         
        except Exception as e:
            _logger.info("Error! Unknown Error while requesting Saferpay Services")
            status = 500
            color = 'red'
            result.update({'error': _('Unknown Error %s') % (e,)})
        result.update({'status': status, 'data': response})
        response_header = isinstance(response, dict) and response.get('ResponseHeader', '') or ''
        txn_ref = ''
        if txn_id:
            txn_ref = self.env['payment.transaction'].sudo().browse(txn_id).reference
        if request:
            try:
                request_ip = request.httprequest.remote_addr or ''
                request_user_agent = request.httprequest.environ.get('HTTP_USER_AGENT', '').lower() or ''
                request_domain = request.httprequest.host or ''
                referrer_path = request.httprequest.referrer or ''
                request_browser = request.httprequest.user_agent.browser or ''
                request_headers = request.httprequest.headers or ''
                request_meta = request.httprequest.headers.environ or {}
                if request.session.geoip:        
                    request_country_code = request.session.get('geoip', {}).get('country_code')                    
                    request_state_code = request.session.get('geoip', {}).get('region')
                    if request_country_code:                    
                        Country = self.env['res.country']
                        request_country_id = Country.search([('code', '=', country_code)], limit=1).id
                        if request_state_code:             
                            State = self.env['res.country.state']
                            request_state_id = State.search([('code', '=', request_state_code), ('country_id.code', '=', request_country_code)], limit=1).id
            except Exception as e:
                _logger.info(_("Error! Failed to collect meta data from the request %s") % (e,))
        self.env['saferpay.acquirer.log']._post_log({
                'name': status,
                'detail': detail,
                'type': color,
                'request_url': request_uri,
                'request_ip': request_ip,
                'request_browser': request_browser,
                'referrer_path': referrer_path,
                'response_header': response_header,
                'response_error': response_error,
                'acquirer_id': acquirer_id,
                'saferpay_config_id': saferpay_config.id,
                'txn_id': txn_id,
                'txn_ref': txn_ref,
                'saferpay_txn_ref' : saferpay_txn_ref,
                'request_country_code': request_country_code,
                'request_state_code': request_state_code,
                'request_country_id': request_country_id,
                'request_state_id': request_state_id,
                
            })
        return result
    
    def _get_feature_support(self):
        """Get advanced feature support by provider.

        Each provider should add its technical in the corresponding
        key for the following features:
            * fees: support payment fees computations
            * authorize: support authorizing payment (separates
                         authorization and capture)
            * tokenize: support saving payment data in a payment.tokenize
                        object
        """
        res = super(PaymentAcquirerSaferpay, self)._get_feature_support()
        res['tokenize'].append('saferpay')
        res['authorize'].append('saferpay')
        return res  
    
    def _compute_saferpay_base_url(self):
        for acquirer in self:
            saferpay_base_url = '' 
            if acquirer.provider == 'saferpay':
                saferpay_config = acquirer.sudo().saferpay_config_id        
                acquirer_env = acquirer.state
                saferpay_config_env = saferpay_config.state
                if acquirer_env == saferpay_config_env == 'enabled':
                    saferpay_base_url = saferpay_config.prod_url
                else:
                    saferpay_base_url = saferpay_config.test_url
            acquirer.saferpay_base_url = saferpay_base_url
    
    @api.depends('payment_icon_ids')
    def _compute_saferpay_code(self):
        for acquirer in self:
            saferpay_code = [icon.saferpay_code for icon in acquirer.payment_icon_ids if icon.saferpay_code]
            safer_code = " ".join(saferpay_code)
            acquirer.safer_code = safer_code.strip()
    
       
    @api.depends('payment_icon_ids.currency_ids')
    def _compute_saferpay_currency(self):
        for acquirer in self:
            acquirer.currency_ids = list(set([currency.id for icon in acquirer.payment_icon_ids for currency in icon.currency_ids]))
    
    @api.constrains('preauth','capture_manually')
    def _check_preauth_config(self):
        if self.preauth == True and self.capture_manually != True:
            raise ValidationError(_('Manual capture should be allowed for Pre-Authorization.'))
    
                
    provider = fields.Selection(selection_add=[('saferpay', 'Saferpay')], ondelete={'saferpay': 'set default'})
    saferpay_config_id = fields.Many2one('saferpay.acquirer.config', string='Saferpay Config', ondelete='restrict', 
                                domain="['|', ('company_id', '=', company_id), ('company_id', '=', False)]", required_if_provider='saferpay', groups='base.group_user')
    
    transaction_interface = fields.Char()
    safer_code = fields.Char("Code", compute='_compute_saferpay_code', required_if_provider='saferpay')
    currency_ids = fields.Many2many('res.currency', string="Allowed currencies", compute='_compute_saferpay_currency', store=True, readonly=True)
    need_order_limit = fields.Boolean('Need Order Limit?') 
    min_order_total = fields.Float(string="Minimum Order Total")
    max_order_total = fields.Float(string="Maximum Order Total")
    invoice_settlement_type = fields.Selection([
                        ('deferred_settlement', 'Deferred Settlement'),
                        ('settlement_after_order', 'Settlement after order')
                        ], string="Invoice Settlement",
                        help="Choose settlement after order in case you want an invoice to be created with your oder. The state of the invoice will set according to your capture status.")
    customer_address = fields.Selection([
                        ('no_addr', 'Dont send address'),
                        ('send_del_addr', 'Send delivery address'),
                        ('send_billing_addr', 'Send billing address'),
                        ('billing_shipping', 'Send both')
                        ], string="Send Customer Address",
                        help="Should the customer address be sent to Saferpay?", default="send_del_addr")
    special_customer_address = fields.Selection([
                        ('send_del_addr', 'Send delivery address'),
                        ('billing_shipping', 'Send billing and delivery address')
                        ], string="Send Customer Address ",
                        help="Should the customer address be sent to Saferpay?", default="send_del_addr")
    cust_confirm_mail = fields.Boolean(string="Customer confirmation email",
                        help="Should Saferpay send a confirmation email to the customer. (Available only if the customer is redirected to Saferpay.)")
    interface_type = fields.Selection([
                        ('payment_page','Payment Page'),
                        ('widget','Transaction Interface'),
                        ('saferpay_fields','Saferpay Fields')
                        ],string="Payment Interface", default="payment_page",
                        help="Select the payment interface to use in order to process this payment method.(Transaction interface and Saferpay fields)")
    alias_manager = fields.Selection([
                        ('active','Active'),
                        ('inactive','Inactive')
                        ], help="The alias manager allows the customer to select from a credit card previously stored. The sensitive data is stored by Saferpay.")
    register_payment_type = fields.Selection([
                                ('CARD', 'CARD'),
                                ('BANK_ACCOUNT', 'BANK_ACCOUNT'),
                                ('POSTFINANCE', 'POSTFINANCE'),
                                ('TWINT', 'TWINT')],
                                help="Type of payment means to register")

    multipart_capture = fields.Boolean('Allow Multipart Capture', default=False)
    is_support_refund = fields.Boolean('Support Refunds?', default=False)
    support_refund_type = fields.Selection([('referenced', 'Referenced Refund'),
                                            ('direct', 'Refund Direct'),
                                            ('both', 'Both')
                                            ], string="Supported Refund Type")
    saferpay_customer_id = fields.Integer(string='Customer ID', groups='base.group_user', related='saferpay_config_id.saferpay_customer_id', readonly=True)
    saferpay_terminal_id = fields.Integer(string='Terminal ID', groups='base.group_user', related='saferpay_config_id.saferpay_terminal_id', readonly=True)
    saferpay_api_username = fields.Char(string='API User Name', groups='base.group_user', related='saferpay_config_id.saferpay_api_username', readonly=True)
    saferpay_api_password = fields.Char(string='API Password', groups='base.group_user', related='saferpay_config_id.saferpay_api_password', readonly=True)
    saferpay_flds_api_key = fields.Char(string='API Key', groups='base.group_user', related='saferpay_config_id.saferpay_flds_api_key', readonly=True)
    send_status_email = fields.Boolean(related='saferpay_config_id.send_status_email', readonly=True)
    liability_shift = fields.Selection(string="Liability Shift Behavior", related='saferpay_config_id.liability_shift', readonly=True)
    needs_advanced_security = fields.Boolean(string="Need Extra Security", related='saferpay_config_id.needs_advanced_security', readonly=True)
    save_from_saferpay = fields.Boolean("Save card through Saferpay")
    saferpay_base_url = fields.Text(compute='_compute_saferpay_base_url')
    license_type = fields.Selection([('eCommerce','eCommerce License'),
                                    ('business','Business License')
                                    ], default="eCommerce")
    is_wallet = fields.Boolean('Support Masterpass?', default=False)
    supports_notify_url = fields.Boolean('Supports Notify URL?', default=False)
    preauth = fields.Boolean('Pre-Authorization')
    saferpay_fields_form_type = fields.Selection([
                                ('sample1', 'Sample1'),
                                ('sample2', 'Sample2'),
                                ('sample3', 'Sample3')
                                ], string="Saferpay Field Template Type", default="sample1")
    enable_styling = fields.Boolean("Enable Styling")
    saferpay_theme = fields.Selection([
                                ('DEFAULT', 'DEFAULT'),
                                ('SIX', 'SIX'),
                                ('NONE', 'NONE')
                                ], string="Saferpay Theme", default="DEFAULT")
    saferpay_css = fields.Many2one('ir.attachment', 
                                   domain="[\
                                            ('public', '=', True), \
                                            ('res_model', '=', 'payment.acquirer'), \
                                            ('mimetype', 'ilike', 'text/css') \
                                        ]", 
                                   string='CSS')
    is_applepay = fields.Boolean('Support ApplePay?', default=False)
    issuer_id = fields.Many2one("ideal.issuerid.config", string="IssuerId", domain="[('state', '=', state)]")
    payment_icon_ids = fields.Many2many(required_if_provider='saferpay')

    @api.onchange('special_customer_address')
    def onchange_customer_address(self):
        if self.special_customer_address:
            self.customer_address = self.special_customer_address
    
    @api.onchange('payment_flow', 'provider')
    def onchange_payment_flow(self):
        result = {}
        if self.payment_flow == 's2s' and self.provider == 'saferpay':
            self.interface_type = 'widget'
        if self.provider and self.provider == 'saferpay':
            domain = {'payment_icon_ids': [('is_saferpay', '=', True)]}
            result = {'domain': domain}
        return result

    @api.onchange('interface_type')
    def onchange_interface_type(self):
        if self.interface_type and self.interface_type == 'saferpay_fields' and \
            self.provider == 'saferpay' and self.saferpay_config_id and \
            self.saferpay_config_id.saferpay_flds_api_key == False:
            raise ValidationError(_('Saferpay Fields need API Key for working, So please set API Key in Acquirer Configuration,'))
        if self.interface_type and self.interface_type == 'payment_page' and self.provider == 'saferpay':
            self.save_token = 'none'
            
    
    
    def toggle_environment_value(self):
        test = self.filtered(lambda acquirer: acquirer.state == 'test' and \
                                              acquirer.provider == 'saferpay' and \
                                              acquirer.saferpay_config_id.state == 'test')
        self.issuer_id = False
        if test:
            raise ValidationError(_('Acquirer config must be in production.'))
        return super(PaymentAcquirerSaferpay, self).toggle_environment_value()

    
    def toggle_license_switch(self):
        self.ensure_one()
        if self.license_type == 'eCommerce':
            self.write({'license_type' : 'business', 'is_support_refund' : False})
        else:
            self.write({'license_type' : 'eCommerce'})
        return True
        
    
    @api.model
    def get_eligible_acquirers(self, acquirers, payment_datas={}):
        currency_id = payment_datas.get('currency_id')
        amount = payment_datas.get('amount')
        origin = payment_datas.get('origin')
        saferpay_acquirers = [acq for acq in acquirers if acq.provider == 'saferpay']
        payment_acquirers = acquirers
        for saferpay_acquirer in saferpay_acquirers:
            if saferpay_acquirer.need_order_limit and amount and \
                not (saferpay_acquirer.min_order_total <= amount <= saferpay_acquirer.max_order_total):
                try:
                    payment_acquirers.remove(saferpay_acquirer)
                except Exception as e:
                    try:
                        payment_acquirers -= saferpay_acquirer
                    except Exception as e:
                        pass
                continue 
            currency_list = saferpay_acquirer.currency_ids.ids
            if currency_id and currency_id not in currency_list and (not saferpay_acquirer.is_wallet and not saferpay_acquirer.is_applepay) :
                try:
                    payment_acquirers.remove(saferpay_acquirer)
                except Exception as e:
                    try:
                        payment_acquirers -= saferpay_acquirer
                    except Exception as e:
                        pass
                continue 
            if not currency_id and (saferpay_acquirer.save_token not in ('ask','always')):
                try:
                    payment_acquirers.remove(saferpay_acquirer)
                except Exception as e:
                    try:
                        payment_acquirers -= saferpay_acquirer
                    except Exception as e:
                        pass
        return payment_acquirers
    
    @api.model
    def get_eligible_pms(self, pms, payment_datas={}):
        currency_id = payment_datas.get('currency_id')
        amount = payment_datas.get('amount')
        origin = payment_datas.get('origin')
        saferpay_tokens = pms and pms.filtered(lambda r: r.acquirer_id.provider == 'saferpay') or self.env['payment.token']
        tokens = pms
        for saferpay_token in saferpay_tokens:
            if saferpay_token.acquirer_id.need_order_limit and amount and \
                not (saferpay_token.acquirer_id.min_order_total <= amount <= saferpay_token.acquirer_id.max_order_total):
                tokens -= saferpay_token
            currency_list = saferpay_token.acquirer_id.currency_ids.ids
            if currency_id and currency_id not in currency_list:
                tokens -= saferpay_token
            if saferpay_token.acquirer_id.save_token not in ['ask','always']:
                tokens -= saferpay_token              
        return tokens

    
    def action_view_saferpay_acquirer_logging(self):
        self.ensure_one()    
        context = dict(self.env.context or {})
        action = self.env.ref('payment_saferpay.action_saferpay_acquirer_log')
        result = action.read()[0]
        context.update({
            'search_default_acquirer_id': self.ids
        })
        result.update({
                        'context': context, 
                    })
        return result
    