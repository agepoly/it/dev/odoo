# -*- coding: utf-8 -*-
#################################################################################
# Author      : PIT Solutions AG. (<https://www.pitsolutions.ch/>)
# Copyright(c): 2019 - Present PIT Solutions AG.
# License URL : https://www.webshopextension.com/en/licence-agreement/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.webshopextension.com/en/licence-agreement/>
#################################################################################

import logging
from datetime import datetime
import json
import http.client
from werkzeug import urls

from odoo import api, models, fields, tools, _
from ..controllers.main import SaferpayController
import random

_logger = logging.getLogger(__name__)


class SaferpayRegisterAlias(models.Model):
    _inherit = 'payment.acquirer'
    
    
    def create_payment_token(self, acquirer_id, partner,  data):
        '''
            Function to create token while Alias Direct Insert
        '''
        token = False
        vals = {
                'acquirer_id' : acquirer_id,
                'partner_id': partner.id,
                'verified': True
                }
        if data:
            if data.get('Token') and acquirer_id:
                vals.update({
                    'name' : self.browse(int(acquirer_id)).safer_code,
                    'acquirer_ref': data.get('Token'),
                    'active' : False,
                    'saferpay_token' : data.get('Token'),
                    'request_id' : data.get('request_id'),
                    })
            else:
                vals.update({
                    'name' : data['PaymentMeans'].get("Brand") and data['PaymentMeans']["Brand"].get("PaymentMethod") or self.browse(int(acquirer_id)).safer_code + ' - ' + data['PaymentMeans']['DisplayText'],
                    'acquirer_ref' : data['Alias']['Id'],
                    'request_id': data['ResponseHeader']['RequestId'],
                    })
            token = self.env['payment.token'].create(vals)
        return token
    
    
    def get_nw_card_payment_means(self, form_data):
        payment_means = {}
        if form_data.get('acquirer_id'):
            acquirer_obj = self.browse(int(form_data['acquirer_id']))
            if acquirer_obj and acquirer_obj.register_payment_type == 'CARD':
                if form_data.get('cc_expiry'):
                    date_format = datetime.strptime(form_data.get('cc_expiry'),'%m / %y').strftime('%Y / %m')
                    exp_year = date_format.split(' / ')[0]
                    exp_month = date_format.split(' / ')[1]
                    payment_means.update({
                        "Card": {
                            "Number": form_data.get('cc_number'),
                            "ExpYear": int(exp_year),
                            "ExpMonth": int(exp_month),
                            "HolderName": form_data.get('cc_holder_name'),
                            "VerificationCode": form_data.get('cvc')
                            }
                    })
            elif acquirer_obj and acquirer_obj.register_payment_type == 'BANK_ACCOUNT':
                payment_means.update({
                    "BankAccount": {
                        "IBAN": form_data.get('cc_number'),
                        "HolderName": form_data.get('cc_holder_name'),
                        "BIC": form_data.get('cvc'),
                        "BankName": form_data.get('cc_brand'),
                        }
                    })
        return payment_means
    
    @api.model
    def new_alias_insert(self, acquirer_id, partner_id, fields_token):
        '''
            Function to register card on saferpay without knowledge about the card details
        '''
        if acquirer_id and partner_id:
            saferpay_acquirer_id = int(acquirer_id)
            saferpay_acquirer = self.browse(saferpay_acquirer_id)
            uri = "/Payment/v1/Alias/Insert"
            type = "POST"
            partner_obj = self.env['res.partner'].browse(int(partner_id))
            saferpay_customer_id = saferpay_acquirer.sudo().saferpay_customer_id
            request_id = str(partner_obj.id) + str(saferpay_acquirer_id) + str(random.randrange(9999))
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            fail_url = urls.url_join(base_url, SaferpayController._alias_failed_url) 
            sucess_url = urls.url_join(base_url, SaferpayController._alias_sucess_url)
            code = saferpay_acquirer.sudo().safer_code.replace(',', ' ').strip()
            payment_method = code.split(" ")
            
            if saferpay_acquirer.sudo().register_payment_type:
                json_data = {
                                "RequestHeader": self.get_request_header(saferpay_acquirer_id, request_id),
                                "RegisterAlias": {
                                    "IdGenerator": "RANDOM_UNIQUE"
                                    },
                               "Type": saferpay_acquirer.sudo().register_payment_type,
                               "ReturnUrls": {
                                   "Success": sucess_url,
                                   "Fail": fail_url 
                                   }
                        }
                if fields_token:
                    json_data.update({
                                    "PaymentMeans": {
                                            "SaferpayFields": {
                                                "Token": fields_token
                                                }
                                            }
                                    })
                else:
                    json_data.update({
                                    "PaymentMethods" : payment_method,
                                    })
                    
                if saferpay_acquirer.sudo().safer_code in ["VISA", "MASTERCARD"]:
                    json_data.update({
                                    "Check": {
                                            "Type": "ONLINE_STRONG",
                                            "TerminalId": saferpay_acquirer.sudo().saferpay_config_id.saferpay_terminal_id
                                            }
                                    })
                    
#                 json_data = self.saferpay_build_styling(saferpay_acquirer_id, json_data)
                response = saferpay_acquirer.saferpay_send_request(saferpay_acquirer_id, \
                                                           type=type, uri=uri, \
                                                           json_data=json_data)  
                status = response.get('status', 0)                
                data = response.get('data', [])
                error = response.get('error', _('Error cant be retrived'))
                if status == 200 and data:
                    card_data = {
                                'Token' :  data.get('Token'),
                                'request_id': request_id,
                                }
                    token = self.create_payment_token(saferpay_acquirer_id, partner_obj, card_data)
                    if data.get('Redirect'):
                        return data['Redirect'].get('RedirectUrl')
                    else:
                        saferpay_req_token = data.get('Token')
                        self.env['payment.token']._saferpay_get_token_status(saferpay_req_token, request_id, saferpay_customer_id, saferpay_acquirer_id, token, fields_card=True)
                        return True
                else:
                    return False
    
    @api.model
    def register_new_card(self, form_data):
        if form_data:
            saferpay_acquirer_id = int(form_data.get('acquirer_id'))
            saferpay_acquirer = self.browse(saferpay_acquirer_id)
            saferpay_customer_id = saferpay_acquirer.sudo().saferpay_customer_id
            partner_obj = self.env['res.partner'].browse(int(form_data['partner_id']))
            request_id = str(form_data.get('cvc')) + str(form_data['acquirer_id']) + str(form_data['partner_id'])
            uri = "/Payment/v1/Alias/InsertDirect"
            type = "POST"
            json_data = {
                        "RequestHeader": self.get_request_header(saferpay_acquirer_id, request_id),
                        "PaymentMeans": self.get_nw_card_payment_means(form_data),
                        "RegisterAlias": {
                            "IdGenerator": "RANDOM_UNIQUE"
                            }
                }
            if saferpay_acquirer.sudo().safer_code in ["VISA", "MASTERCARD"]:
                json_data.update({
                                "Check": {
                                        "Type": "ONLINE",
                                        "TerminalId": saferpay_acquirer.sudo().saferpay_config_id.saferpay_terminal_id
                                        }
                                })
        
            response = saferpay_acquirer.saferpay_send_request(saferpay_acquirer_id, \
                                                       type=type, uri=uri, \
                                                       json_data=json_data)  
            status = response.get('status', 0)                
            data = response.get('data', [])
            error = response.get('error', _('Error cant be retrived'))
            if status == 200 and data:
                if data.get('Alias'):
                    if data['Alias'].get('Id'):
                        payment_token_obj = self.env['payment.token'].search([('partner_id', '=', partner_obj.id), ('acquirer_ref', '=', data['Alias']['Id'])])
                        if not payment_token_obj:
                            payment_token_obj = self.create_payment_token(saferpay_acquirer_id, partner_obj, data)
                return True
            else:
                return False
