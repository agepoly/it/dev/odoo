# -*- coding: utf-8 -*-
#################################################################################
# Author      : PIT Solutions AG. (<https://www.pitsolutions.ch/>)
# Copyright(c): 2019 - Present PIT Solutions AG.
# License URL : https://www.webshopextension.com/en/licence-agreement/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.webshopextension.com/en/licence-agreement/>
#################################################################################

import logging
import string
import re
import json
import http.client
from werkzeug import urls
from datetime import datetime

from odoo import api, models, fields, tools, _
from odoo.exceptions import ValidationError
from odoo.http import request
from odoo.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT
from ..controllers.main import SaferpayController
from .payment_acquirer import SAFERPAY_API_VERSION, SAFERPAY_FIELDS_VERSION
from reportlab.lib.colors import peachpuff

_logger = logging.getLogger(__name__)

class PaymentAcquirerSaferpay(models.Model):
    _inherit = 'payment.acquirer'
    
    
    def saferpay_get_form_action_url(self):
        if self.interface_type == 'payment_page':
            return "/payment/saferpay/redirect"
        else:
            return ''
    
    
    def saferpay_form_generate_values(self, tx_values):
        self.ensure_one()
        trans_vals = {}
        form_data = {}
        fields_token = False
        save_card_token = False
        acquirer_id = self.id
        saferpay_config = self.sudo().saferpay_config_id.sudo()
        context = self._context or {}
        reference = tx_values.get('reference')
        name = reference
        if reference:
            punctuation = re.escape(string.punctuation)
            name = re.sub(r'['+punctuation+']', '-', reference)
        Transaction = self.env['payment.transaction']               
        transaction = Transaction.search([('reference', '=', reference)])
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        url_params = '?txnId=%s'% (transaction.id)
        success_url = urls.url_join(base_url, SaferpayController._success_url)
        failed_url = urls.url_join(base_url, SaferpayController._failed_url) 
        notify_url = urls.url_join(base_url, SaferpayController._notify_url)
        saferpay_redirect_url = urls.url_join(base_url, SaferpayController._saferpay_redirect_url)        
        referrer_path = request and request.httprequest.referrer or ''
        tx_values.update({'saferpay_txn_url': '', 
                          'saferpay_txn_interface_url': '', 
                          'saferpay_fields_url': '', 
                          'successUrl': "".join([success_url,url_params]),
                          'failedUrl': "".join([failed_url,url_params]),
                          'notify_url': "".join([notify_url,url_params]),
                          'referrer_path': referrer_path})
        saferpay_tx_values = {}
        billing_partner_state = tx_values.get('billing_partner_state').name or ''
        billing_partner_div_code = tx_values.get('billing_partner_state').code or ''
        billing_partner_country_code = tx_values.get('billing_partner_country').code or ''
        shipping_partner_state = tx_values.get('partner_state').name or ''
        shipping_partner_div_code = tx_values.get('partner_state').code or ''
        shipping_partner_country_code = tx_values.get('partner_country').code or ''

        if tx_values.get('partner_first_name', '') and tx_values.get('partner_last_name', ''):
            partner_first_name = tx_values.get('partner_first_name', '') 
            partner_last_name = tx_values.get('partner_last_name', '')
        else:
            partner_first_name = tx_values.get('partner_name').split()[0] 
            partner_last_name = tx_values.get('partner_name').split()[1:] or '.'

        if tx_values.get('billing_partner_first_name', '') and tx_values.get('billing_partner_first_name', ''):
            billing_partner_first_name = tx_values.get('billing_partner_first_name', '') 
            billing_partner_last_name = tx_values.get('billing_partner_last_name', '')
        else:
            billing_partner_first_name = tx_values.get('billing_partner_name').split()[0] 
            billing_partner_last_name = tx_values.get('partner_last_name').split()[1:] or '.'

        billing_address = {
            "city": tx_values.get('billing_partner_city', ''),
            "emailAddress": tx_values.get('billing_partner_email', ''),
            "givenName": tx_values.get('billing_partner_name', ''),
            "first_name": billing_partner_first_name,
            "phoneNumber": tx_values.get('billing_partner_phone', ''),
            "organisationName": tx_values.get('billing_partner_commercial_company_name', ''),
            "postCode": tx_values.get('billing_partner_zip', ''),
            "state": billing_partner_state,
            "street": tx_values.get('billing_partner_address', ''),
            "country_code": billing_partner_country_code,
            "sub_div_code": billing_partner_div_code,
        }
        if billing_partner_last_name != '.':
            billing_address.update({
                    "last_name": billing_partner_last_name,
                    })

        shipping_address = {
            "givenName": tx_values.get('partner_name', ''),
            "first_name": partner_first_name,
            "phoneNumber": tx_values.get('partner_phone', ''),
            "street": tx_values.get('partner_address', ''),
            "city": tx_values.get('partner_city', ''),
            "state": shipping_partner_state,
            "country_code": shipping_partner_country_code,
            "emailAddress": tx_values.get('partner_email', ''),
            "postCode": tx_values.get('partner_zip', ''),
            "sub_div_code" : shipping_partner_div_code,
        }

        if shipping_address != '.':
            shipping_address.update({
                    "last_name": shipping_address,
                    })

        tx_values['currency_name'] = tx_values['currency'].name
        tx_details = {
                        'currency_name': tx_values['currency_name'],
                        'name': reference,
                        'partner_id': tx_values.get('partner_id', ''),
                    }
        saferpay_tx_values['tx_details'] = tx_details
        saferpay_tx_values['partner_id'] = tx_values.get('partner_id', '')
        saferpay_tx_values['billing_address'] = billing_address
        saferpay_tx_values['shipping_address'] = shipping_address
        saferpay_tx_values['amount'] = tx_values.get('amount')
        
        saferpay_req_token = ''
        if (self.payment_flow == 'form' and self.interface_type == 'widget') or \
            (self.payment_flow == 's2s') or \
            (transaction.payment_token_id and transaction.pay_card):
                txn_vals = {
                            'acquirer_id': acquirer_id,
                            'saferpay_tx_values': saferpay_tx_values, 
                            'transaction': transaction,
                            }  
                if transaction.payment_token_id and transaction.pay_card:
                    save_card_token = transaction.payment_token_id.acquirer_ref
                response = self.saferpay_build_transaction_interface_url(txn_vals, form_data, fields_token, save_card_token)
                saferpay_txn_interface_url = response.get('redirect_url', '')
                data = response.get('data', {})
                saferpay_req_token = data.get('Token', '')
                tx_values.update({
                            'saferpay_txn_interface_url': saferpay_txn_interface_url,
                            'saferpay_req_token': saferpay_req_token
                          })
        elif self.payment_flow == 'form':
            if self.interface_type == 'payment_page':     
                response = self.saferpay_build_payment_page_url(acquirer_id, saferpay_tx_values, transaction)
                data = response.get('data', {})
                saferpay_req_token = data.get('Token', '')
                saferpay_txn_url = data.get('RedirectUrl', '')
                redirect = data.get('Redirect', {})
                response_header = data.get('ResponseHeader', {})
                saferpay_request_id = response_header.get('RequestId', '')
                if not saferpay_txn_url and redirect:
                    saferpay_txn_url = redirect.get('RedirectUrl', '')
                tx_values['saferpay_request_id'] = saferpay_request_id                  
                tx_values['saferpay_req_token'] = saferpay_req_token            
                tx_values['saferpay_txn_url'] = saferpay_txn_url
            elif self.interface_type == 'saferpay_fields':
                response = self.saferpay_build_saferpay_fields_url(acquirer_id)
                saferpay_fields_url = response.get('saferpay_fields_url', '')
                saferpay_fields_api_key = response.get('saferpay_fields_api_key', '')
                saferpay_fields_api_url = response.get('saferpay_fields_api_url', '')
                saferpay_fields_form_type = response.get('saferpay_fields_form_type', '')
                saferpay_fields_css_url = response.get('saferpay_fields_css_url', '')
                saferpay_payment_method_code = response.get('saferpay_payment_method_code', '')
                tx_values.update({
                        'saferpay_fields_url': saferpay_fields_url,
                        'saferpay_fields_api_key': saferpay_fields_api_key,
                        'saferpay_fields_api_url': saferpay_fields_api_url,
                        'saferpay_fields_form_type': saferpay_fields_form_type,
                        'saferpay_fields_css_url': saferpay_fields_css_url,
                        'saferpay_payment_method_code': saferpay_payment_method_code,
                })
        tx_values['saferpay_customer_id'] = saferpay_config.saferpay_customer_id
        trans_vals.update({
                            'saferpay_req_token': saferpay_req_token,
                        })
        transaction.write(trans_vals)
        return tx_values
        
    
    def get_payer_details(self, values):
        self.ensure_one()
        payer_details = {}
        saferpay_code = ''
        lang_data = ['de','de-ch','en','fr','da','cs','es','et','hr','it','hu','lv','lt','nl','nn','pl','pt','ru','ro','sk','sl','fi','sv','tr','el','ja','zh']
        current_lang = request.lang.code
        if current_lang.lower() in lang_data:
            saferpay_code = current_lang.lower()
        elif '_' in current_lang:
            code1 = current_lang.lower().replace("_","-")
            code2 = current_lang.lower().split('_')[1]
            if code1 in lang_data:
                saferpay_code = code1
            elif code2 in lang_data:
                saferpay_code = code2
            else:
                saferpay_code = 'en'
        else:
            saferpay_code = 'en'
        
        payer_details.update({
                    'LanguageCode': saferpay_code,
                    'Id': values['partner_id']
                    })

        if self.customer_address in ['send_billing_addr','billing_shipping']:
            billing_address = {
                                'City': values['billing_address']['city'],
                                'Email': values['billing_address']['emailAddress'],
                                'FirstName': values['billing_address']['first_name'],
                                'LastName': values['billing_address'].get('last_name') or '.',
                                'Phone': values['billing_address']['phoneNumber'],
                                'CountryCode': values['billing_address']['country_code'],
                                'Street': values['billing_address']['street'],
                                'zip': values['billing_address']['postCode'],
                                'CountrySubdivisionCode': values['billing_address']['sub_div_code'],
                                }
            payer_details.update({
                        'BillingAddress': billing_address,
                        })
          
        if self.customer_address in ['send_del_addr','billing_shipping']:
            delivery_address = {
                                'City': values['shipping_address']['city'],
                                'Email': values['billing_address']['emailAddress'],
                                'FirstName': values['shipping_address']['first_name'],
                                'LastName': values['billing_address'].get('last_name') or '.',
                                'Phone': values['shipping_address']['phoneNumber'],
                                'CountryCode': values['shipping_address']['country_code'],
                                'Street': values['shipping_address']['street'],
                                'zip': values['shipping_address']['postCode'],
                                'CountrySubdivisionCode': values['shipping_address']['sub_div_code'],
                                }
            payer_details.update({
                        'DeliveryAddress': delivery_address,
                        })
        return payer_details
    
#     
#     def get_order_details(self):
#         order_details = {}
#         return order_details
        
    @api.model
    def get_request_header(self, acquirer_id, merchantReference):
        acquirer_id = int(acquirer_id)
        acquirer = self.browse(acquirer_id)
        saferpay_config = acquirer.sudo().saferpay_config_id
        customer_id = str(saferpay_config.sudo().saferpay_customer_id)
        chars = re.escape(string.punctuation)
        merchantReference = re.sub(r'['+chars+']', '-',merchantReference)
        rqt_hdr = {}
        if customer_id:
            rqt_hdr.update({
                            'SpecVersion': SAFERPAY_API_VERSION,
                            'CustomerId': customer_id,
                            'RequestId': merchantReference,
                            'RetryIndicator': 0,
                            'ClientInfo' : {
                                            'ShopInfo': 'ODOO_12.0: PIT Solutions AG_12.0.1.0',
                                            }
                            })
        return rqt_hdr
    
    @api.model
    def get_amount(self, amount):
        amt = "0"
        if amount:
            amt = str(round(amount * 100))
        return amt
    
    @api.model
    def get_payment(self, tx_details, amount):       
        payment_dict = {}
        if tx_details:
            currency_name = tx_details.get('currency_name')
            merchantReference = tx_details.get('name')
            amount = self.get_amount(amount)
            payment_dict = {
                            "Amount": {
                                      "Value": amount,
                                      "CurrencyCode": currency_name,
                                    },
                            "OrderId": merchantReference,
                            "Description": "Payment for " + merchantReference
                            }
            if self.preauth == True:
                payment_dict.update({'Options': 
                                        {'PreAuth': True}
                                    })
        return payment_dict
    
    @api.model
    def saferpay_build_payment_page_url(self, saferpay_acquirer_id, values, transaction): 
        json_data = {}
        lang = request.lang and request.lang.code.split('_')[0] or 'en'   
        saferpay_acquirer_id = int(saferpay_acquirer_id)
        saferpay_acquirer = self.browse(saferpay_acquirer_id)
        code = saferpay_acquirer.sudo().safer_code.replace(',', ' ').strip()
        payment_method = code.split(" ")
        state_message = ''
        uri = "/Payment/v1/PaymentPage/Initialize"
        txn_id = transaction.id
        if saferpay_acquirer.sudo().is_wallet != True and saferpay_acquirer.sudo().is_applepay != True:
            json_data.update({"PaymentMethods" : payment_method})
        if saferpay_acquirer.sudo().safer_code == 'IDEAL' and saferpay_acquirer.sudo().issuer_id != False:
            json_data.update({"PaymentMethodsOptions" : {"Ideal": {"IssuerId": saferpay_acquirer.sudo().issuer_id.sudo().code}}})
        type = "POST"
        
        url_params = '?txnId=%s'% (txn_id)
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        success_url = urls.url_join(base_url, SaferpayController._success_url)
        failed_url = urls.url_join(base_url, SaferpayController._failed_url)    
        notify_url = urls.url_join(base_url, SaferpayController._notify_url)
        
        tx_details = values.get('tx_details')          
        currency_name = tx_details.get('currency_name')
        merchantReference = tx_details.get('name')
        amount = values.get('amount')

        notify_dict = {
                    "NotifyUrl": notify_url + url_params
                }
        
        merchant_email = transaction.acquirer_id.company_id.email
        
        if saferpay_acquirer.sudo().cust_confirm_mail == True:
            notify_dict.update({
                            'MerchantEmails': [merchant_email],
                            'PayerEmail': values['billing_address']['emailAddress'],
                            })
        
        json_data.update({
            "language": lang,
            "RequestHeader": self.get_request_header(saferpay_acquirer_id, merchantReference),
            "TerminalId": saferpay_acquirer.sudo().saferpay_config_id.saferpay_terminal_id,
            "Payment": self.get_payment(tx_details, amount),
            "Payer" : self.get_payer_details(values),
            "Notification" : notify_dict,   
            "ReturnUrls": {
                        "Success": "".join([success_url,url_params]),
                        "Fail": "".join([failed_url,url_params])
                        },            
            })
        if saferpay_acquirer.sudo().save_token in  ('ask','always') and transaction.type == 'form_save':
            json_data.update({
                            "RegisterAlias": {
                                            "IdGenerator": "RANDOM_UNIQUE"
                                            },
                            })
        '''elif saferpay_acquirer.sudo().saferpay_config_id.needs_advanced_security:
            json_data.update({
                            "Authentication": {
                                            "ThreeDsChallenge": "FORCE"
                                            } 
                            })
        else:
            json_data.update({
                            "Authentication": {
                                            "Exemption": "TRANSACTION_RISK_ANALYSIS"
                                            }
                            })'''

        if saferpay_acquirer.sudo().is_wallet == True:
            json_data.update({"Wallets": ["MASTERPASS"]})
            
        if saferpay_acquirer.sudo().is_applepay == True:
            json_data.update({"Wallets": ["APPLEPAY"]})

        if saferpay_acquirer.sudo().saferpay_config_id.saferpay_config_name:
            json_data.update({"ConfigSet": saferpay_acquirer.sudo().saferpay_config_id.saferpay_config_name})
                        
#         json_data = self.saferpay_build_styling(saferpay_acquirer_id, json_data)

        response = saferpay_acquirer.saferpay_send_request(saferpay_acquirer_id, \
                                                       type=type, uri=uri, \
                                                       json_data=json_data, txn_id=txn_id)            
        status = response.get('status', 0)                
        data = response.get('data', [])
        error = response.get('error', _('Error cant be retrived'))
        res = {}
        if status == 200 and data:
            res = {'request_id': data['ResponseHeader']['RequestId'], 'data': data}
            if transaction:
                transaction.sudo().write({
                                    'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                                    })
        else:
            res = {'request_id': False, 'error': error, 'data': data}
            state_message = "\n".join([transaction.state_message or '', data.get('ErrorName', '')])
            state_message = "\n".join([state_message or '', data.get('ErrorMessage', '')])
            error_msg = str(data.get('ErrorMessage', ''))
            error_detail =  str(data.get('ErrorDetail', ''))
            if transaction:
                transaction.sudo().write({
                                    'state_message':  state_message,
                                    'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                                    })
            raise ValidationError(_("\n\n Error Message:- %s \n Error Detail:- %s"% (error_msg, error_detail)))
        return res
    
    @api.model
    def saferpay_token_payment(self, save_token_id):
        txn_vals = {}
        fields_token = False
        save_card = self.env['payment.token'].browse(save_token_id)
        save_card_token = save_card.acquirer_ref
        form_data = {
                    'acquirer': save_card.acquirer_id.id,
                     }
        respose = self.saferpay_build_transaction_interface_url(txn_vals, form_data, fields_token, save_card_token)
        return respose
        
    @api.model
    def saferpay_build_transaction_interface_url(self, txn_vals, form_data, fields_token, save_card_token):
        res = {}
        json_data = {}
        tx_details = {}
        PaymentMeans = {}
        uri = "/Payment/v1/Transaction/Initialize"
        type = "POST"
        transaction = txn_vals.get('transaction')
        if not transaction:
            transaction = self.env['payment.transaction'].search([('reference', '=', form_data.get("merchantReference"))])
        txn_id = transaction and transaction.id or 0
        lang = request.lang and request.lang.code.split('_')[0] or 'en'
        saferpay_acquirer_id = form_data.get('acquirer') and int(form_data.get('acquirer')) or txn_vals.get('acquirer_id')
        saferpay_acquirer = self.browse(saferpay_acquirer_id)
        request_ref = form_data.get("merchantReference") or txn_vals.get('saferpay_tx_values') and \
                        txn_vals['saferpay_tx_values'].get('tx_details') and txn_vals['saferpay_tx_values']['tx_details'].get('name')
        amount = form_data.get("amountIncludingTax") or txn_vals.get('saferpay_tx_values') and txn_vals['saferpay_tx_values'].get('amount')
        code = saferpay_acquirer.sudo().safer_code.replace(',', ' ').strip()
        payment_method = code.split(" ")
        
        tx_details.update({
                        'currency_name' : form_data.get("currency") or txn_vals.get('saferpay_tx_values') and \
                                            txn_vals['saferpay_tx_values'].get('tx_details') and txn_vals['saferpay_tx_values']['tx_details'].get('currency_name'),
                        'name' : form_data.get("merchantReference") or txn_vals.get('saferpay_tx_values') and \
                                    txn_vals['saferpay_tx_values'].get('tx_details') and txn_vals['saferpay_tx_values']['tx_details'].get('name'),
                        })
        
        url_params = '?txnId=%s'% (transaction.sudo().id)
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        success_interface_url = urls.url_join(base_url, SaferpayController._success_interface_url)
        failed_interface_url = urls.url_join(base_url, SaferpayController._failed_interface_url)
        notify_url = urls.url_join(base_url, SaferpayController._notify_url)
        
        if fields_token:
            json_data.update({
                            "PaymentMeans" : {
                                "SaferpayFields": {
                                                "Token": fields_token
                                                }
                                },
                            "Payer": {"LanguageCode" : lang},
                            })
        else:
            values = txn_vals['saferpay_tx_values']
            if save_card_token:
                json_data.update({
                                "PaymentMeans" : {
                                    "Alias" : {
                                        "Id" : save_card_token
                                        }
                                    },
                                })
                if saferpay_acquirer.sudo().saferpay_config_id.sudo().need_cvc_token_txn:
                    json_data.update({
                                "CardForm" : {
                                    "VerificationCode" : "MANDATORY"
                                    },
                                })
                
            else:
                json_data.update({
                                "PaymentMethods" : payment_method,
                                })
            json_data.update({
                "Payer": self.get_payer_details(values),
                })
        json_data.update({
            "RequestHeader": self.get_request_header(saferpay_acquirer_id, request_ref),
            "TerminalId": saferpay_acquirer.sudo().saferpay_config_id.saferpay_terminal_id,
            "Payment": self.get_payment(tx_details, float(amount)),
            "ReturnUrls": {
                        "Success": "".join([success_interface_url,url_params]),
                        "Fail": "".join([failed_interface_url,url_params]),
                        },            
            })

        if saferpay_acquirer.sudo().saferpay_config_id.saferpay_config_name:
            json_data.update({"ConfigSet": saferpay_acquirer.sudo().saferpay_config_id.saferpay_config_name})
        
        '''if saferpay_acquirer.sudo().save_token in  ('ask','always') or saferpay_acquirer.sudo().saferpay_config_id.needs_advanced_security:
            json_data.update({
                            "Authentication": {
                                            "ThreeDsChallenge": "FORCE"
                                            }
                            })
        else:
            json_data.update({
                            "Authentication": {
                                            "Exemption": "TRANSACTION_RISK_ANALYSIS"
                                            }
                            })'''
                        
#         json_data = self.saferpay_build_styling(saferpay_acquirer_id, json_data)
        response = saferpay_acquirer.saferpay_send_request(saferpay_acquirer_id, \
                                                       type=type, uri=uri, \
                                                       json_data=json_data, txn_id=txn_id)
        status = response.get('status', 0)                
        data = response.get('data', [])
        error = response.get('error', _('Error cant be retrived'))
        if status == 200 and data:
            token = data.get('Token')
            if transaction:
                transaction.sudo().write({
                                        'saferpay_req_token': token,
                                        'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                                        })
            
            if data['RedirectRequired'] == True and data.get('Redirect'):
                res = {'redirect_url': data['Redirect'].get('RedirectUrl'), 'data': data}
            elif data['RedirectRequired'] == False and token:
                res = {'direct_url': "".join([success_interface_url,url_params])}
            else:
                res = {'direct_url': "".join([failed_interface_url,url_params])}
        else:
            res = {'direct_url':"".join([failed_interface_url,url_params])}
            error_msg = str(data.get('ErrorMessage', ''))
            error_detail =  str(data.get('ErrorDetail', ''))
            if transaction:
                state_message = "\n".join([transaction.state_message or '', data.get('ErrorName', '')])
                state_message = "\n".join([state_message or '', data.get('ErrorMessage', '')])
                transaction.sudo().write({
                                        'state_message':  state_message,
                                        'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                                        })
#             if fields_token:
#                 res = {'request_id': False, 'error': error, 'data': data}
            if not fields_token:
                raise ValidationError(_("\n\n Error Message:- %s \n Error Detail:- %s"% (error_msg, error_detail)))
        return res
    
    @api.model
    def saferpay_build_saferpay_fields_url(self, acquirer_id):        
        acquirer_id = int(acquirer_id)
        acquirer = self.browse(acquirer_id)
        saferpay_config = acquirer.sudo().saferpay_config_id.sudo()        
        acquirer_env = acquirer.sudo().state
        saferpay_config_env = saferpay_config.state
        acquirer_fields_base_url = "".join([acquirer.saferpay_base_url, '/Fields'])
        saferpay_customer_id = str(saferpay_config.saferpay_customer_id)
        saferpay_fields_api_key = saferpay_config.saferpay_flds_api_key
        saferpay_fields_url = "".join([acquirer_fields_base_url, '/lib/', SAFERPAY_FIELDS_VERSION, '/saferpay-fields.js'])
        saferpay_fields_api_url = "".join([acquirer_fields_base_url, '/', saferpay_customer_id])    
        saferpay_fields_form_type = acquirer.sudo().saferpay_fields_form_type   
        saferpay_fields_css_url = acquirer.sudo().saferpay_build_cssurl(acquirer_id)
        saferpay_payment_method_code = acquirer.sudo().safer_code.replace(',', ' ').strip().lower()
        res = {
                'saferpay_fields_url': saferpay_fields_url,
                'saferpay_fields_api_key': saferpay_fields_api_key,
                'saferpay_fields_api_url': saferpay_fields_api_url,
                'saferpay_fields_form_type': saferpay_fields_form_type,
                'saferpay_fields_css_url': saferpay_fields_css_url,
                'saferpay_payment_method_code': saferpay_payment_method_code,
            }
        return res
    
    @api.model
    def saferpay_build_styling(self, saferpay_acquirer_id, json_data):        
        saferpay_acquirer_id = int(saferpay_acquirer_id)
        saferpay_acquirer = self.browse(saferpay_acquirer_id)
        saferpay_styling = {}
        if saferpay_acquirer.sudo().enable_styling and saferpay_acquirer.sudo().saferpay_theme:
            saferpay_styling.update({
                                    'Theme': saferpay_acquirer.sudo().saferpay_theme,  
                                })
        saferpay_css_url = self.saferpay_build_cssurl(saferpay_acquirer_id)
        if saferpay_css_url:
            saferpay_styling.update({
                                    'ContentSecurityEnabled': True,
                                    'CssUrl': saferpay_css_url 
                                })
        if saferpay_styling:
            json_data.update({'Styling': saferpay_styling})
        
        return json_data
    
    @api.model
    def saferpay_build_cssurl(self, saferpay_acquirer_id): 
        saferpay_css_url = ''
        saferpay_acquirer_id = int(saferpay_acquirer_id)
        saferpay_acquirer = self.browse(saferpay_acquirer_id)
        if saferpay_acquirer.sudo().enable_styling and saferpay_acquirer.sudo().saferpay_css:
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            attach_url = "/".join(["web/content", str(saferpay_acquirer.saferpay_css.id)])
            saferpay_css_url = urls.url_join(base_url, attach_url)
        return saferpay_css_url
        
        
        
        
