# -*- coding: utf-8 -*-
#################################################################################
# Author      : PIT Solutions AG. (<https://www.pitsolutions.ch/>)
# Copyright(c): 2019 - Present PIT Solutions AG.
# License URL : https://www.webshopextension.com/en/licence-agreement/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.webshopextension.com/en/licence-agreement/>
#################################################################################


from odoo import api, models, fields, tools, _


class IdealIssueridConfig(models.Model):
    _name = "ideal.issuerid.config"
    _description = "Saferpay Ideal Issuer Details"
            
            
    name = fields.Char(required=True, translate=True, copy=False)
    code = fields.Char(required=True, translate=True, copy=False)
    state = fields.Selection([
        ('disabled', 'Disabled'),
        ('enabled', 'Enabled'),
        ('test', 'Test Mode')], required=True, default='test', copy=False,
        help="""In test mode, a fake payment is processed through a test
             payment interface. This mode is advised when setting up the
             acquirer. Watch out, test and production modes require
             different credentials.""")
