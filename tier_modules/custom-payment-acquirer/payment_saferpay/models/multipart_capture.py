# -*- coding: utf-8 -*-
#################################################################################
# Author      : PIT Solutions AG. (<https://www.pitsolutions.ch/>)
# Copyright(c): 2019 - Present PIT Solutions AG.
# License URL : https://www.webshopextension.com/en/licence-agreement/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.webshopextension.com/en/licence-agreement/>
#################################################################################

import logging
from odoo import api, models, fields, tools, _
from odoo.addons.payment.models.payment_acquirer import ValidationError

_logger = logging.getLogger(__name__)

class MultipartCapture(models.Model):
    _name = 'saferpay.multipart.capture'
    _description = "Multi-part Capture"


    @api.depends('transaction_id')
    def _compute_sequence_name(self):
        for record in self:
            if record.transaction_id:
                multipart_captures = self.env['saferpay.multipart.capture'].search([('transaction_id','=', record.transaction_id.id)])
                if multipart_captures:
                    record.name = record.transaction_id.reference + str(len(multipart_captures) + 1)
                else:
                    record.name = record.transaction_id.reference + str(1)
            else:
                 record.name = '/'


    name = fields.Char(string='Reference', copy=False, compute="_compute_sequence_name", store=True)
    transaction_id = fields.Many2one('payment.transaction', string="Transaction")
    amount = fields.Monetary(string='Amount', currency_field='currency_id', required=True)
    currency_id = fields.Many2one('res.currency', 'Currency', related='transaction_id.currency_id', readonly=True, store=True)    
    saferpay_state = fields.Char(readonly=True, track_visibility='onchange', default="DRAFT")
    state = fields.Selection([('draft','Draft'),('done','Done'),('cancel','Cancel')], string="Status", default="draft")
    saferpay_captureid = fields.Char('Capture Reference', readonly=True,)

    _sql_constraints = [
        ('neg_zero_amount', 'CHECK (amount>0)', 'The amount should be greater than zero!'),
    ]

    @api.constrains('amount', 'transaction_id', 'currency_id')
    def _check_amount(self):
        for record in self:
            captured_amount = 0
            multipart_captures = self.search([
                                            ('transaction_id','=', record.transaction_id.id),
                                            ('id','!=', record.id),
                                            ('state', '!=', 'cancel')
                                        ])
            for amt in multipart_captures:
                captured_amount += amt.amount 
            allowed_amount = record.transaction_id.amount - captured_amount
            allowed_amount = record.currency_id.round(allowed_amount)
            if record.amount > allowed_amount:
                raise ValidationError(_('You cannot capture more than the authorized amount.'))
    
    def button_done(self):
        for record in self:
            transaction = record.transaction_id
            txn_id = transaction.id
            trans_amount = transaction.amount
            trans_captured_amount = transaction.captured_amount
            uri = "/Payment/v1/Transaction/MultipartCapture"
            type = "POST"
            saferpay_acquirer = transaction.acquirer_id
            saferpay_acquirer_id = transaction.acquirer_id.id
            saferpay_request_id = transaction.reference
            saferpay_tx_id = transaction.acquirer_reference
            
            if record.amount == record.currency_id.round(trans_amount - trans_captured_amount):
                capture_type = 'FINAL'
            else:
                capture_type = 'PARTIAL'
            json_data = {
                "RequestHeader": saferpay_acquirer.get_request_header(saferpay_acquirer_id, saferpay_request_id),
                "TransactionReference": {
                            "TransactionId": saferpay_tx_id
                            },
                "Amount": {
                            "Value": transaction.acquirer_id.get_amount(record.amount),
                            "CurrencyCode": record.currency_id.name,
                            },
                "Type": capture_type,
                "OrderPartId": record.name
            }                
            response = saferpay_acquirer.saferpay_send_request(saferpay_acquirer_id, \
                                                   type=type, uri=uri, \
                                                   json_data=json_data, txn_id=txn_id)  
            status = response.get('status', 0)                
            data = response.get('data', [])
            error = response.get('error', _('Error cant be retrived'))
            if status == 200 and data:
                if data.get('Status') and data['Status'] == 'CAPTURED':
                    record.write({'saferpay_state' : 'CAPTURED',
                                 'saferpay_captureid' : data['CaptureId'],
                                 'state': 'done'
                                })
                    transaction._compute_multipart_capture_amount()
                    if trans_amount == transaction.captured_amount:
                        transaction.saferpay_captured_txn()
            else:
                error = _('Saferpay: feedback error')
                error = error + '\n' + data.get('ErrorName') + '\n' + data.get('ErrorMessage')
                _logger.info(error)
                record.write({
                                'saferpay_state' : 'CANCELLED',
                                'state': 'cancel'
                            })
                return {
                    'name': _('Saferpay Error ...!!!'),
                    'type': 'ir.actions.act_window',
                    'res_model': 'saferpay.warning.wizard',
                    'view_id': self.env.ref('payment_saferpay.view_saferpay_warning_wizard').id,
                    'view_mode': 'form',
                    'view_type': 'form',
                    'context': {'default_message': error},
                    'target': 'new'
                }
                