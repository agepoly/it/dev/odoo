# -*- coding: utf-8 -*-
#################################################################################
# Author      : PIT Solutions AG. (<https://www.pitsolutions.ch/>)
# Copyright(c): 2019 - Present PIT Solutions AG.
# License URL : https://www.webshopextension.com/en/licence-agreement/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.webshopextension.com/en/licence-agreement/>
#################################################################################

import logging
import http.client

from odoo import api, models, fields, tools, _
from odoo.http import request
from odoo.exceptions import UserError, ValidationError

_logger = logging.getLogger(__name__)

class PaymentTokenSaferpay(models.Model):
    _inherit = 'payment.token'

    request_id = fields.Char()
    saferpay_token = fields.Char("Token from Saferpay")
    auth_result = fields.Char("Authentication Result", readonly=True)
    is_authenticated = fields.Boolean("Authenticated", readonly=True, default=True)
    
    @api.model
    def unlink_registeralias(self, token):
        '''
            Function to delete Register Alias from Saferpay
        '''
        saferpay_acquirer = token.acquirer_id
        saferpay_acquirer_id = token.acquirer_id.id
        saferpay_customer_id = token.acquirer_id.sudo().saferpay_customer_id
        
        uri = "/Payment/v1/Alias/Delete"
        type = "POST"
        if token.payment_ids:
            req_id = str(token.payment_ids.ids[0])
        else:
            req_id = str(token.request_id)
        json_data = {
                    "RequestHeader": saferpay_acquirer.get_request_header(saferpay_acquirer_id, req_id),
                    "AliasId": token.acquirer_ref
                }                
        response = saferpay_acquirer.saferpay_send_request(saferpay_acquirer_id, \
                                                       type=type, uri=uri, \
                                                       json_data=json_data)  
        status = response.get('status', 0)                
        data = response.get('data', [])
        error = response.get('error', _('Error cant be retrived'))  
        if status == 200 and data:
            return True
        else:
            raise ValidationError(_("Token can't be deleted from Saferpay."))        
    
    
    def unlink(self):
        for token in self:
            if token.acquirer_id.provider == 'saferpay':
                token_unlink_status = token.unlink_registeralias(token)
            return super(PaymentTokenSaferpay, self).unlink()
    
    @api.model
    def _saferpay_get_token_status(self, saferpay_req_token, saferpay_request_id, saferpay_customer_id, saferpay_acquirer_id, old_token, fields_card):
        if saferpay_req_token and saferpay_request_id and saferpay_customer_id and saferpay_acquirer_id:
            saferpay_acquirer = self.env['payment.acquirer'].browse(saferpay_acquirer_id)
            uri = "/Payment/v1/Alias/AssertInsert"
            type = "POST"
            json_data = {
                        "RequestHeader": saferpay_acquirer.get_request_header(saferpay_acquirer_id, saferpay_request_id),
                        "Token": saferpay_req_token
                }
            response = saferpay_acquirer.saferpay_send_request(saferpay_acquirer_id, \
                                                       type=type, uri=uri, \
                                                       json_data=json_data)
            status = response.get('status', 0)                
            data = response.get('data', [])
            error = response.get('error', _('Error cant be retrived'))
            if status == 200 and data:
                self.search([('acquirer_ref', '=', data['Alias']['Id'])])
                token_exist = self.env['payment.token'].search([('partner_id', '=', old_token.partner_id.id),('acquirer_ref', '=', data['Alias']['Id'])])

                if token_exist:
                    old_token.unlink()
                else:
                    old_token.write({
                                'name' : data['PaymentMeans']['Brand']['PaymentMethod'] + ' - '+ data['PaymentMeans']['DisplayText'],
                                'acquirer_ref' : data['Alias']['Id'],
                                'active': True
                                })
                if data.get("CheckResult") and data["CheckResult"].get("Result"):
                    old_token.is_authenticated = True
                    old_token.auth_result = data["CheckResult"]["Result"]
                    if data["CheckResult"]["Result"] == "NOT_PERFORMED":
                        old_token.is_authenticated = False
    
    @api.model
    def alias_feedback(self):
        tokens = self.search([('active', '=', False), ('saferpay_token', '!=', False)])
        for token in tokens:
            saferpay_req_token = token.saferpay_token
            saferpay_request_id = token.request_id
            saferpay_customer_id = token.acquirer_id.saferpay_customer_id
            acquirer_id = token.acquirer_id.id
            response = self._saferpay_get_token_status(saferpay_req_token, saferpay_request_id, saferpay_customer_id, acquirer_id, token, fields_card=False)
    
    @api.model
    def update_alias_expiry(self, token, form_data):
        '''
            Function to update Alias Expiry date
        '''
        Token = self.sudo().browse(token)
        saferpay_acquirer = Token.acquirer_id
        saferpay_acquirer_id = Token.acquirer_id.id
        saferpay_request_id = Token.request_id or Token.id
        data = form_data.split('/')
        month = data[0]
        date = data[1]
        uri = "/Payment/v1/Alias/Update"
        type = "POST"
        json_data = {
                    "RequestHeader": saferpay_acquirer.get_request_header(saferpay_acquirer_id, saferpay_request_id),
                    "UpdateAlias": {
                        "Id": Token.acquirer_ref,
                        "LifeTime": 1600,
                    },
                    "UpdatePaymentMeans": {
                        "Card": {
                          "ExpMonth": month,
                          "ExpYear": date
                        }
                    }
            }
        response = saferpay_acquirer.saferpay_send_request(saferpay_acquirer_id, \
                                                   type=type, uri=uri, \
                                                   json_data=json_data)
        status = response.get('status', 0)                
        data = response.get('data', [])
        error = response.get('error', _('Error cant be retrived'))
        if status == 200 and data:
            Token.write({
                        'acquirer_ref': data['Alias']['Id'],
                        })
            return True
        else:
            return False


class ResPartner(models.Model):
    _inherit = 'res.partner' 
    
    alias = fields.Char("Alias ID SaferPay")  
     
