# -*- coding: utf-8 -*-
#################################################################################
# Author      : PIT Solutions AG. (<https://www.pitsolutions.ch/>)
# Copyright(c): 2019 - Present PIT Solutions AG.
# License URL : https://www.webshopextension.com/en/licence-agreement/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.webshopextension.com/en/licence-agreement/>
#################################################################################

from odoo import api, fields, models, tools, _


class Currency(models.Model):
    _inherit = "res.currency"
    
    saferpay_payment_icons = fields.Many2many('payment.icon', 'currency_safer_icon_rel', 'currency_id', 'icon_id' , string="Allowed saferpay payment icons")
