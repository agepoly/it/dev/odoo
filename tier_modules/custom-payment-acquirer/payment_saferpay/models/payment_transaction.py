# -*- coding: utf-8 -*-
#################################################################################
# Author      : PIT Solutions AG. (<https://www.pitsolutions.ch/>)
# Copyright(c): 2019 - Present PIT Solutions AG.
# License URL : https://www.webshopextension.com/en/licence-agreement/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.webshopextension.com/en/licence-agreement/>
#################################################################################

import logging
from psycopg2 import OperationalError, Error
import ast
from pprint import pformat
from datetime import datetime
import http.client
from werkzeug import urls

from odoo import api, registry, models, fields, tools, _
from odoo.http import request
from odoo.exceptions import UserError, ValidationError
from ..controllers.main import SaferpayController
from odoo.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT

_logger = logging.getLogger(__name__)

class PaymentTransaction(models.Model):
    _name = 'payment.transaction'
    _inherit = ['payment.transaction', 'mail.thread'] 
       
    _saferpay_pending_states = ['CREATE', 'CONFIRMED', \
                              'PROCESSING', 'COMPLETED']
       
    _saferpay_cancel_messages = ['GENERAL_DECLINED', 'TRANSACTION_ABORTED', \
                              'TRANSACTION_DECLINED', 'ACTION_NOT_SUPPORTED', 'ALIAS_INVALID', \
                              'AMOUNT_INVALID', 'BLOCKED_BY_RISK_MANAGEMENT', \
                              'AUTHENTICATION_FAILED', 'CARD_CHECK_FAILED', \
                              'CARD_CVC_INVALID', 'CARD_CVC_REQUIRED', \
                              'CARD_EXPIRED', 'COMMUNICATION_TIMEOUT', \
                              'CONDITION_NOT_SATISFIED', 'CURRENCY_INVALID', \
                              'GENERAL_DECLINED', 'NO_CREDITS_AVAILABLE', \
                              'PAYMENTMEANS_INVALID', 'PAYMENTMEANS_NOT_SUPPORTED', \
                              '3DS_AUTHENTICATION_FAILED', 'TRANSACTION_ABORTED', \
                              'TRANSACTION_DECLINED', 'VALIDATION_FAILED', 'ABORT'
                              ]
    
    _saferpay_error_messages = ['TRANSACTION_NOT_FOUND', 'TRANSACTION_IN_WRONG_STATE', \
                              'TOKEN_EXPIRED', 'TOKEN_INVALID']
    
    _saferpay_threeds_payments_code = ['AMEX', 'DINERS', 'DISCOVER','MAESTRO',\
                                       'MASTERCARD', 'VISA', 'VPAY']
    
    saferpay_state = fields.Char(readonly=True, track_visibility='onchange', default="DRAFT")
    saferpay_token = fields.Char(readonly=True, track_visibility='onchange')
    liability_shift = fields.Text("Liability Shift Info")
    pay_card = fields.Boolean(default=False)
    form_validate = fields.Boolean(default=False)
    saferpay_captureid = fields.Char("CaptureID")
    saferpay_txn_ref = fields.Char(string="TransactionID")
    liability = fields.Boolean(string="Liability Shift")
    three_ds = fields.Boolean(string="ThreeDs")
    liability_entity_shift = fields.Boolean(string="ThreeDs Liability Shift")
    authenticated = fields.Boolean(string="ThreeDs Authenticated")
    saferpay_req_token = fields.Char(string="Token")
    error_on_saferpay = fields.Char(String="Error to Capture this Transaction")
    multipart_capture_ids = fields.One2many('saferpay.multipart.capture','transaction_id')
    multipart_capture_ids_nbr = fields.Integer(compute='_compute_multipart_capture_ids_nbr', string='# of Multipart Captures', store=True)
    multipart_capture_ids_done_nbr = fields.Integer(compute='_compute_multipart_capture_ids_nbr')
    captured_amount = fields.Monetary(compute="_compute_multipart_capture_amount", store=True, readonly=True, currency_field='currency_id')
    multipart_capture = fields.Boolean('Supports Multicapture?', related="acquirer_id.multipart_capture")
    invoke_notify_url = fields.Boolean('Invoke Notify URL', default=False)
    auth_result = fields.Char("Authentication Result")
    auth_message = fields.Char("Authentication Message")

    
    def unlink(self):
        for transaction in self:
            if transaction.acquirer_id.provider == 'saferpay' and transaction.date:
                raise UserError(_('You can not delete a saferpay tranasction which is already processed!'))
        return super(PaymentTransaction, self).unlink()

    @api.depends('multipart_capture_ids','multipart_capture_ids_nbr')
    def _compute_multipart_capture_amount(self):
        for trans in self:
            captured_amount = 0.0
            for capture in self.env['saferpay.multipart.capture'].search([('transaction_id','=', trans.id),('state','=','done')]):
                captured_amount += capture.amount
            trans.captured_amount = captured_amount


    @api.depends('multipart_capture_ids')
    def _compute_multipart_capture_ids_nbr(self):
        for trans in self:
            trans.multipart_capture_ids_nbr = len(trans.multipart_capture_ids)
            trans.multipart_capture_ids_done_nbr = len(trans.multipart_capture_ids.filtered(lambda x: x.state == 'done'))

    
    def saferpay_multipart_capture(self):
        '''
            Function for multipart capture from Saferpay
        '''
        self.ensure_one()
        draft_captures = self.env['saferpay.multipart.capture'].search([('transaction_id','=', self.id),('state','=','draft')])
        if draft_captures:
            raise ValidationError(_('You have Multipart Captures that are not completed. Please update the status'))
        else:
            return {
                    'name': _('Multipart Capture!!!'),
                    'type': 'ir.actions.act_window',
                    'res_model': 'saferpay.multipart.capture',
                    'view_id': self.env.ref('payment_saferpay.view_saferpay_multipart_capture_form').id,
                    'view_mode': 'form',
                    'view_type': 'form',
                    'context': {'default_transaction_id': self.id, 'default_amount':self.amount-self.captured_amount},
                    'target': 'new'
                      }

    
    def action_view_multipart_captures(self):
        action = {
            'name': _('Multipart Captures'),
            'type': 'ir.actions.act_window',
            'res_model': 'saferpay.multipart.capture',
            'target': 'current',
        }
        multipart_capture_ids = self.multipart_capture_ids.ids
        if len(multipart_capture_ids) == 1:
            multipart_capture = multipart_capture_ids[0]
            action['res_id'] = multipart_capture
            action['view_mode'] = 'form'
            action['views'] = [(self.env.ref('payment_saferpay.view_saferpay_multipart_capture_form').id, 'form')]
        else:
            action['view_mode'] = 'tree,form'
            action['domain'] = [('id', 'in', multipart_capture_ids)]
        return action
    
    
    def get_saferpay_state_and_txn_token(self, data):
        saferpay_state = ''
        vals = {}
        if data:
            if data.get('Transaction'):
                saferpay_state = data['Transaction']['Status']
                vals.update({'saferpay_state': saferpay_state})
            if data.get('RegistrationResult') and data['RegistrationResult'].get('Alias') and data['RegistrationResult']['Alias'].get('Id'):
                token_obj = self.env['payment.token'].search([('partner_id', '=', self.partner_id.id), ('acquirer_ref', '=', data['RegistrationResult']['Alias']['Id'])])
                if not token_obj:
                    self.saferpay_alias_authentication_result(data)
                if token_obj:
                    vals.update({
                                'payment_token_id' : token_obj.id,
                                'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                                })
            self.write(vals)
        return saferpay_state
    
    
    def update_token_data(self, card_no):
        self.payment_token_id.write({
                                    'name': card_no,
                                    'request_id': self.reference,
                                    })
    
    
    def process_txn(self, saferpay_state, card_no):
        resp = False
        response = {}
        vals = {}
        if self.payment_token_id:
            self.update_token_data(card_no)
        if saferpay_state == 'AUTHORIZED' and (not self.liability_shift and self.acquirer_id.capture_manually):
            self._set_transaction_authorized()
            self.write({
                        'saferpay_state': 'AUTHORIZED',
                        'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                        })
            resp = True
        elif saferpay_state == 'AUTHORIZED' and (not self.liability_shift):
            response = self.acquirer_id.saferpay_capture_transation(self, self.acquirer_id.id)
        elif saferpay_state == 'AUTHORIZED' and self.liability_shift:
            liability = ast.literal_eval(self.liability_shift)
            response = self.saferpay_txn_liability_shift_checking(liability)
        elif saferpay_state == 'CAPTURED':
            self.saferpay_captured_txn()
            resp = True
        elif saferpay_state == 'PENDING':
            self._set_transaction_pending()
            self.write({
                        'saferpay_state': 'PENDING',
                        'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                        })
            resp = True
        
        if response == False:
            response = {}
        status = response.get('status', 0)
        data = response.get('data', {})
        if response:
            if status == 200 and data:
                if (data.get('Status') and data['Status'] == 'CAPTURED') or (data.get('Transaction') and data['Transaction']['Status'] == 'CAPTURED'):
                    self.saferpay_captured_txn()
                    vals.update({'saferpay_captureid': data.get('CaptureId')})
                else:
                    if self.state != 'authorized':
                        self._set_transaction_authorized()
                    vals.update({
                                'saferpay_state': 'AUTHORIZED',
                                'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                                })
                self.write(vals)
                resp = True
            else:
                error = _('Saferpay: feedback error')
                error = error + '\n' + data.get('ErrorName', '') + '\n' + data.get('ErrorMessage', '')
                _logger.info(error)
                vals.update({'state_message': error})
                if self.state in ['draft', 'authorized']:
                    self._set_transaction_cancel()
                    vals.update({
                                'saferpay_state' : 'CANCELED',
                                'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                                })
                if self.acquirer_id.send_status_email:
                    try:        
                        template_id = self.env.ref('payment_saferpay.saferpay_email_template_payment_transaction_cancel')
                        if template_id:
                            template_id.send_mail(self.id, force_send=True)
                    except Exception as e:
                        _logger.info("Error! email cannot be send %s" % (e,))
                self.write(vals)
                resp = False
        return resp
        
    
    def update_txn_status(self):
        saferpay_state = ''
        card_no = ''
        vals = {}
        if (self.saferpay_state == 'DRAFT' and self.state == 'draft' and self.saferpay_req_token) or \
            (self.state == 'draft' and self.saferpay_req_token):
            saferpay_req_token = self.saferpay_req_token
            saferpay_request_id = self.reference
            saferpay_customer_id = self.acquirer_id.saferpay_customer_id
            acquirer_id = self.acquirer_id.id
            response = self._saferpay_get_txn_status(saferpay_req_token, saferpay_request_id, saferpay_customer_id, acquirer_id)
            status = response.get('status', 0)
            data = response.get('data', {})
            if status == 200 and data:
                card_no = data['PaymentMeans']['Brand']['PaymentMethod'] + ' - ' + data['PaymentMeans']['DisplayText']
                saferpay_state = self.get_saferpay_state_and_txn_token(data)
                if saferpay_state:
                    self.process_txn(saferpay_state, card_no)
            else:
                error = _('Saferpay: feedback error')
                _logger.info(error)
                state_message = self.state_message or '' + "\n" + data['ErrorMessage']
                vals.update({
                            'state_message': state_message,
                            'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                            })
                self.write(vals)
        elif self.saferpay_state in ['PENDING', 'AUTHORIZED'] and self.acquirer_reference:
            self.saferpay_s2s_capture_transaction()
        elif self.saferpay_state == 'DRAFT' and not self.saferpay_req_token:
            self._set_transaction_cancel()
    
    
    def saferpay_s2s_capture_transaction(self):
        '''
            Function for manual capture from Saferpay
        '''
        for txn in self:
            vals = {}
            if txn.state in ('pending', 'authorized'):
                uri = "/Payment/v1/Transaction/Capture"
                type = "POST"
                saferpay_acquirer = self.acquirer_id
                saferpay_acquirer_id = self.acquirer_id.id
                saferpay_request_id = self.reference
                saferpay_tx_id = self.acquirer_reference
                url_params = '?txnId=%s'% (txn.id)
                base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
                notify_url = urls.url_join(base_url, SaferpayController._notify_url)
                
                json_data = {
                    "RequestHeader": saferpay_acquirer.get_request_header(saferpay_acquirer_id, saferpay_request_id),
                    "TransactionReference": {
                        "TransactionId": saferpay_tx_id
                      }
                } 
                if txn.acquirer_id.safer_code == 'PAYDIREKT':
                    json_data.update({
                                    'PendingNotification': {
                                                            "NotifyUrl": notify_url
                                                            }
                                    })                      
                response = saferpay_acquirer.saferpay_send_request(saferpay_acquirer_id, \
                                                       type=type, uri=uri, \
                                                       json_data=json_data, txn_id=txn.id)  
                status = response.get('status', 0)                
                data = response.get('data', [])
                error = response.get('error', _('Error cant be retrived'))   
                if status == 200 and data:
                    if data.get('Status') and data['Status'] == 'CAPTURED':
                        vals.update({
                                     'saferpay_state' : 'CAPTURED',
                                     'saferpay_captureid' : data['CaptureId'],
                                     'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                                    })
                        self._set_transaction_done()
                else:
                    if data['ErrorName'] == 'TRANSACTION_ALREADY_CAPTURED' and self.state != 'done':
                        vals.update({
                                    'saferpay_state' : 'CAPTURED',
                                    'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                                    })
                        self._set_transaction_done()
                    else:
                        state_message = self.state_message or '' + "\n" + data['ErrorMessage']
                        vals.update({
                                    'state_message' : state_message,
                                     'error_on_saferpay' : True,
                                     'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                                    })
                self.write(vals)

    
    def saferpay_s2s_do_transaction(self, **kwargs):
        self.ensure_one()
        self.write({'pay_card': True})
        return True
    
    
    def force_check_safepay_txn_status(self):
        '''
            Function to check the txn status of Safepay when backend shows error message
        '''
        vals = {}
        self.ensure_one()
        if self.state in ('pending', 'authorized') and self.acquirer_reference:
            uri = "/Payment/v1/Transaction/Inquire"
            type = "POST"
            saferpay_acquirer = self.acquirer_id
            saferpay_acquirer_id = self.acquirer_id.id
            saferpay_request_id = self.reference
            
            json_data = {
                "RequestHeader": saferpay_acquirer.get_request_header(saferpay_acquirer_id, saferpay_request_id),
                "TransactionReference": {
                    "TransactionId": self.acquirer_reference
                  }
            }                
            response = saferpay_acquirer.saferpay_send_request(saferpay_acquirer_id, \
                                                   type=type, uri=uri, \
                                                   json_data=json_data, txn_id=self.id)  

            status = response.get('status', 0)                
            data = response.get('data', [])
            saferpay_state = ''
            state_message = ''
            card_no = ''
            if status == 200 and data:
                card_no = data['PaymentMeans']['Brand']['PaymentMethod'] + ' - ' + data['PaymentMeans']['DisplayText']
                saferpay_state = self.get_saferpay_state_and_txn_token(data)
                if saferpay_state and saferpay_state == 'PENDING':
                    if self.state != 'pending':
                        vals.update({
                                    'saferpay_state': 'PENDING',
                                    'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                                    })
                        self._set_transaction_pending()
                    state_message = self.state_message or '' + "\n" + "Transaction PENDING"
                
                elif saferpay_state and saferpay_state == 'AUTHORIZED':
                    if self.state != 'authorized':
                        vals.update({
                                    'saferpay_state': 'AUTHORIZED',
                                    'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                                    })
                        self._set_transaction_authorized()
                    state_message = self.state_message or '' + "\n" + "Transaction AUTHORIZED"
                        
                elif saferpay_state and saferpay_state == 'CAPTURED':
                    if self.state != 'done':
                        vals.update({
                                    'saferpay_captureid': data['Transaction']['Id'],
                                    'saferpay_state': saferpay_state,
                                    'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                                    })
                        self._set_transaction_done()
                    state_message = self.state_message or '' + "\n" + "Transaction CAPTURED"
                    
                elif saferpay_state and saferpay_state == 'CANCELED':
                    if self.state != 'cancel':
                        if self.state not in ['draft', 'authorized']:
                            vals.update({'state' : 'draft'})
                        vals.update({
                                    'saferpay_state' : saferpay_state,
                                    'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                                    })
                        self._set_transaction_cancel()
                    state_message = self.state_message or '' + "\n" + "Transaction CANCELED"
                    
            else:
                state_message = self.state_message or '' + "\n" + data['ErrorMessage']
                
            vals.update({
                        'state_message': state_message,
                        'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                        })
            self.write(vals)
            
    
    
    def saferpay_cancel_txn(self):
        self.ensure_one()
        saferpay_acquirer_id = int(self.acquirer_id.id)
        saferpay_acquirer = self.env['payment.acquirer'].browse(self.acquirer_id)
        saferpay_request_id = self.reference
        
        uri = "/Payment/v1/Transaction/Cancel"
        type = "POST"
        json_data = {
                      "RequestHeader": saferpay_acquirer.get_request_header(saferpay_acquirer_id, saferpay_request_id),
                      "TransactionReference": {
                        "TransactionId": self.acquirer_reference
                      }
                    }
        response = saferpay_acquirer.saferpay_send_request(saferpay_acquirer_id, \
                                                       type=type, uri=uri, \
                                                       json_data=json_data, txn_id=self.id)
        return response
    
    
    def saferpay_s2s_void_transaction(self):
        response = self.saferpay_cancel_txn()
        status = response.get('status', 0)                
        data = response.get('data', [])
        error = response.get('error', _('Error cant be retrived'))
        if status == 200 and data:
            self._set_transaction_cancel()
            return True
        else:
            return False
    
    
    def _set_transaction_date(self):
        if any(trans.state != 'draft' for trans in self):
            raise ValidationError(_('Only draft transaction can be processed.'))
        self.write({'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)})
        
    
    def _saferpay_get_txn_status(self, token, request_id, saferpay_customer_id, saferpay_acquirer_id):
        res = {}
        vals = {}
        json_data = {}
        payment_method_code = ''
        if token and request_id and saferpay_customer_id and saferpay_acquirer_id:
            saferpay_acquirer = self.env['payment.acquirer'].browse(saferpay_acquirer_id)
            saferpay_txn = self.env['payment.transaction'].search([('reference', '=', request_id)])
            if saferpay_acquirer.interface_type == 'payment_page' and self.pay_card == False:
                uri = "/Payment/v1/PaymentPage/Assert"
                type = "POST"
            elif (saferpay_acquirer.interface_type in ['saferpay_fields', 'widget']) or saferpay_acquirer.payment_flow == 's2s' or self.pay_card:
                uri = "/Payment/v1/Transaction/Authorize"
                type = "POST"
                
                if saferpay_acquirer.save_token in  ('ask','always') and saferpay_txn.type == 'form_save':
                    json_data.update({"RegisterAlias": {
                                        "IdGenerator": "RANDOM_UNIQUE"
                                    }})
            
            json_data.update({
                        "RequestHeader": saferpay_acquirer.get_request_header(saferpay_acquirer_id, request_id), 
                       "Token": token
                })
            
            response = saferpay_acquirer.saferpay_send_request(saferpay_acquirer_id, \
                                                       type=type, uri=uri, \
                                                       json_data=json_data, txn_id=saferpay_txn.id)  
            status = response.get('status', 0)                
            data = response.get('data', [])
            error = response.get('error', _('Error cant be retrived'))
            if status == 200 and data:
                res = {'request_id': data['ResponseHeader']['RequestId'], 'data': data, 'status' : status}
                vals.update({
                            'acquirer_reference': data['Transaction']['Id'],
                            'saferpay_txn_ref': data['Transaction']['Id'],
                            'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                            })
                
                if data.get('PaymentMeans') and data['PaymentMeans'].get('Brand') and data['PaymentMeans']['Brand'].get('PaymentMethod'):
                    payment_method_code = data['PaymentMeans']['Brand']['PaymentMethod']
                    
                if data['Liability'] and ((saferpay_acquirer.safer_code in self._saferpay_threeds_payments_code) or \
                                          (payment_method_code in self._saferpay_threeds_payments_code)):
                    vals.update({'liability_shift': data['Liability']}) 
                if saferpay_txn.acquirer_id.save_token in ('ask','always'):
                    if (data.get('RegistrationResult') and data['RegistrationResult'].get('AuthenticationResult') and \
                        data['RegistrationResult']['AuthenticationResult'].get('Result') == 'OK'):
                            vals.update({
                                        'type': 'form_save',
                                        'auth_result' : data['RegistrationResult']['AuthenticationResult'].get('Result'),
                                        'auth_message' : data['RegistrationResult']['AuthenticationResult'].get('Message')
                                        })
                    if "RegistrationResult" not in data.keys():
                        vals.update({
                                    'type': 'form_save'
                                    })
                saferpay_txn.write(vals)
            elif data.get('Behavior') in self._saferpay_cancel_messages:
                vals.update({
                            'saferpay_state' : 'ABORT',
                            'state_message': "\n".join([self.state_message or '', data.get('ErrorMessage', '')]),
                            'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                            })
                saferpay_txn._set_transaction_cancel()
            elif data.get('Behavior') in self._saferpay_error_messages:
                error = _('Saferpay: feedback error')
                error = error + '\n' + data.get('ErrorName') + '\n' + data.get('ErrorMessage')
                saferpay_txn.saferpay_state = 'ERROR'
                saferpay_txn._set_transaction_error(error)
            else:
                vals.update({
                            'saferpay_state' : 'ERROR',
                            'state_message': "\n".join([self.state_message or '', data.get('ErrorMessage', '')]),
                            'date': fields.Datetime.now(),
                            })
                error = _('Saferpay: feedback error')
                error = "\n".join([error or '', data.get('ErrorMessage', '')])
                _logger.info(error)
            saferpay_txn.write(vals)
        return res

    
    def saferpay_alias_authentication_result(self, data):
        if data:
            if data.get('RegistrationResult') and self.partner_id and \
               (self.type == 'form_save' or self.acquirer_id.save_token in ('ask','always'))\
               and not self.payment_token_id:
                pm = self.env['payment.token'].create({
                    'partner_id': self.partner_id.id,
                    'acquirer_id': self.acquirer_id.id,
                    'acquirer_ref': data['RegistrationResult']['Alias'].get('Id'),
                    'name': '%s - %s' % (self.partner_id.name, self.acquirer_id.safer_code)
                })
                self.payment_token_id=pm.id
            if self.payment_token_id:
                self.payment_token_id.verified = True
    
    
    def check_saferpay_acquirer_liability_shift(self):
        '''
            Function to check the liability configuration of Acquirer
        '''
        saferpay_acquirer = self.acquirer_id
        liability_shift = saferpay_acquirer.saferpay_config_id.liability_shift
        vals = {}
        if liability_shift == 'hold':
            if self.state != 'authorized':
                self._set_transaction_authorized()
            vals.update({
                        'saferpay_state': 'AUTHORIZED',
                        'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                        })
        
        elif liability_shift == 'cancel':
            cancel_res = self.saferpay_cancel_txn()
            status = cancel_res.get('status', 0)                
            data = cancel_res.get('data', [])
            error = cancel_res.get('error', _('Error cant be retrived'))
            if status == 200 and data:
                self._set_transaction_cancel()
                vals.update({
                            'saferpay_state': 'CANCELED',
                            'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                            })
        self.write(vals)
    
    
    def saferpay_txn_liability_shift_checking(self, liability):
        '''
            Function to check the liability of transaction (Payment Gateway) result
        '''
        response = {}
        saferpay_acquirer = self.acquirer_id
        vals = {
                'liability': False,
                'three_ds' : False,
                'liability_entity_shift' : False,
                'authenticated' : False,
            }
        liability_shift = liability.get('LiabilityShift', False)
        threeds = liability.get('ThreeDs', {})
        threeds_liability_shift = threeds.get('LiabilityShift', False)
        threeds_authenticated = threeds.get('Authenticated', False)
        needs_advanced_security = self.acquirer_id.needs_advanced_security 
        if (liability_shift and \
                (needs_advanced_security and threeds_authenticated and threeds_liability_shift) or \
                    (not needs_advanced_security and threeds_liability_shift and threeds_authenticated) or \
                        (not needs_advanced_security and threeds_liability_shift and not threeds_authenticated)):
            if self.acquirer_id.capture_manually:
                if self.state != 'authorized':  
                    self._set_transaction_authorized()
                vals.update({
                            'saferpay_state': 'AUTHORIZED',
                            'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                            })
            else:
                response = saferpay_acquirer.saferpay_capture_transation(self, self.acquirer_id.id)
        
        if (not liability_shift and not threeds_authenticated and not needs_advanced_security) or \
            (threeds and  (not liability_shift and \
                (not needs_advanced_security and not threeds_liability_shift and not threeds_authenticated) or \
                    (not threeds_liability_shift and threeds_authenticated))):
            self.check_saferpay_acquirer_liability_shift()
        
        if (not threeds_authenticated and needs_advanced_security) or \
            (threeds and (not liability_shift and \
                (needs_advanced_security and not threeds_liability_shift and not threeds_authenticated) or \
                    (needs_advanced_security and threeds_liability_shift and not threeds_authenticated))):
            cancel_res = self.saferpay_cancel_txn()
            status = cancel_res.get('status', 0)                
            data = cancel_res.get('data', [])
            error = cancel_res.get('error', _('Error cant be retrived'))
            if status == 200 and data:
                self._set_transaction_cancel()
                vals.update({
                            'saferpay_state': 'CANCELED',
                            'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                            })
                        
        if liability_shift:
            vals.update({'liability': True})                        
        if needs_advanced_security and threeds_authenticated:
            vals.update({'authenticated': True})        
        if threeds_liability_shift:
            vals.update({'liability_entity_shift': True})    
        if threeds:
            vals.update({'three_ds': True})
        self.write(vals)
        return response
    

        
    def saferpay_captured_txn(self):
        vals = {}
        self._set_transaction_done()
        vals.update({
                    'saferpay_state': 'CAPTURED',
                    'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                    })
        self.write(vals)
        if self.acquirer_id.send_status_email:
            try:        
                template_id = self.env.ref('payment_saferpay.saferpay_email_template_payment_transaction_confirm')
                if template_id:
                    template_id.send_mail(self.id, force_send=True)
            except Exception as e:
                _logger.info("Error! email cannot be send %s" % (e,))
        return True 
    
    @api.model
    def _saferpay_form_get_tx_from_data(self, data):
        _logger.info('Saferpay: _saferpay_form_get_tx_from_data data %s', pformat(data))        
        saferpay_request_id = data.get('saferpay_request_id', 0)
        tx_ids_list = self.search([('reference', '=', saferpay_request_id)]).ids
        tx = self.browse(tx_ids_list)
        if not tx or len(tx) > 1:
            error_msg = _('Saferpay: not received data for reference')
            if not tx:
                error_msg += _('; no order found')
            else:
                error_msg += _('; multiple order found')
            _logger.info(error_msg)
            raise ValidationError(error_msg)
        return tx[0]

    
    def _saferpay_form_validate(self, data):
        _logger.info('Saferpay: _saferpay_form_validate data %s', pformat(data)) 
        self.ensure_one()   
        vals = {
                'form_validate': True
            } 
        res = False
        try:
            with self.env.cr.savepoint():
                self._cr.execute("SELECT 1 FROM payment_transaction WHERE id = %s FOR UPDATE NOWAIT", [self.id], log_exceptions=False)
                acquirer = self.acquirer_id        
                saferpay_config = acquirer.sudo().saferpay_config_id
                saferpay_customer_id = saferpay_config.saferpay_customer_id            
                saferpay_payment_method = acquirer.safer_code
                saferpay_req_token = self.saferpay_req_token
                saferpay_request_id = self.reference
                acquirer_id = acquirer.id 
                response = self._saferpay_get_txn_status(saferpay_req_token, saferpay_request_id, saferpay_customer_id, acquirer_id)        
                status = response.get('status', 0)                
                data = response.get('data', {})
                error = response.get('error', '')
                err_detail = response.get('err_detail', {})
                saferpay_state = ''
                card_no = ''
                if status == 200 and data:
                    card_no = data['PaymentMeans']['Brand']['PaymentMethod'] + ' - ' + data['PaymentMeans']['DisplayText']                
                    saferpay_state = self.get_saferpay_state_and_txn_token(data)
                    if saferpay_state:
                        res = self.process_txn(saferpay_state, card_no)
                elif data.get('Behavior') in self._saferpay_cancel_messages:
                    vals.update({
                                'state_message': "\n".join([self.state_message or '', data.get('ErrorMessage', '')]),
                                'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                                })
                    self._set_transaction_cancel()
                elif data.get('Behavior') in self._saferpay_error_messages:
                    error = _('Saferpay: feedback error')
                    error = error + '\n' + data.get('ErrorName') + '\n' + data.get('ErrorMessage')
                    self._set_transaction_error(error)
                else:
                    vals.update({
                                'state_message': "\n".join([self.state_message or '', data.get('ErrorMessage', '')]),
                                'date': fields.Datetime.now(),
                                })
                    error = _('Saferpay: feedback error')
                    error = "\n".join([error or '', data.get('ErrorMessage', '')])
                    _logger.info(error)
                self.write(vals)
        except OperationalError as e:
            _logger.info("A pgOperationalError occured while processing _saferpay_form_validate: %s", e.pgerror) 
            if e.pgcode == '55P03':
                pass
            else:
                raise
        except Error as e:
            _logger.info("A pgError occured while _processing _saferpay_form_validate: %s", e.pgerror)    
            raise 
        except Exception as e:
            _logger.info("An Error occured while processing _saferpay_form_validate: %s" % (e,))
            raise   
        return res
