# -*- coding: utf-8 -*-
#################################################################################
# Author      : PIT Solutions AG. (<https://www.pitsolutions.ch/>)
# Copyright(c): 2019 - Present PIT Solutions AG.
# License URL : https://www.webshopextension.com/en/licence-agreement/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.webshopextension.com/en/licence-agreement/>
#################################################################################

import logging
import json
import http.client
from werkzeug import urls

from odoo import api, models, fields, tools, _
from odoo.http import request
from ..controllers.main import SaferpayController

_logger = logging.getLogger(__name__)

class PaymentAcquirerSaferpay(models.Model):
    _inherit = 'payment.acquirer'       
    
    
    def saferpay_capture_transation(self, txn, saferpay_acquirer_id):        
        saferpay_acquirer_id = int(saferpay_acquirer_id)
        saferpay_acquirer = self.browse(saferpay_acquirer_id)
        uri = "/Payment/v1/Transaction/Capture"
        type = "POST"
        saferpay_request_id = txn.reference
        saferpay_tx_id = txn.acquirer_reference
        url_params = '?txnId=%s'% (txn.id)
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        notify_url = urls.url_join(base_url, SaferpayController._notify_url)
        
        json_data = {
                    "RequestHeader": self.get_request_header(saferpay_acquirer_id, saferpay_request_id),
                    "TransactionReference": {
                        "TransactionId": saferpay_tx_id
                      }
                }   
        if txn.acquirer_id.safer_code == 'PAYDIREKT':
            json_data.update({
                            'PendingNotification': {
                                                    "NotifyUrl": notify_url
                                                    }
                            })          
        response = saferpay_acquirer.saferpay_send_request(saferpay_acquirer_id, \
                                                       type=type, uri=uri, \
                                                       json_data=json_data, txn_id=txn.id)  
        status = response.get('status', 0)                
        data = response.get('data', [])
        error = response.get('error', _('Error cant be retrived'))           
        return response
    
    
    def cron_update_saferpay_state(self):
        saferpay_transactions = self.env['payment.transaction'].search([
                                            ('acquirer_id.provider','=', 'saferpay'),
                                            '|',
                                            ('saferpay_state','in', ['DRAFT', 'PENDING']),
                                            '&',
                                            ('saferpay_state', '=', 'AUTHORIZED'),
                                            ('acquirer_id.capture_manually', '!=', True)
                                            ])
        for tx in saferpay_transactions:
            tx.update_txn_status()

        saferpay_refunds = self.env['payment.transaction'].search([
                                            ('is_saferpay_refund','=', True),
                                            ('acquirer_id.provider','=', 'saferpay'),
                                            ('saferpay_state', '=', 'PENDING')
                                            ])
        for tx in saferpay_refunds:
            tx.saferpay_refund_form_validate(data={})

            json_data = {
                        "RequestHeader": self.get_request_header(saferpay_acquirer.id, saferpay_request_id),
                        "CaptureReference": {
                            "CaptureId": capture.saferpay_captureid
                          }
                    }                
            response = saferpay_acquirer.saferpay_send_request(saferpay_acquirer.id, \
                                                           type=type, uri=uri, \
                                                           json_data=json_data, txn_id=self.id)  
            status = response.get('status', 0)                
            data = response.get('data', [])
            error = response.get('error', _('Error cant be retrived'))
            if status == 200 and data:
                if data.get('Status') and data['Status'] == 'CAPTURED':
                    capture.write({'saferpay_state' : 'CAPTURED',
                                 'saferpay_captureid' : data['CaptureId'],
                                 'state': 'done'
                                })
                    capture.transaction_id._compute_multipart_capture_amount()
                    if capture.transaction_id.amount == capture.transaction_id.captured_amount:
                        self.transaction_id.saferpay_captured_txn()
            else:
                error = _('Saferpay: feedback error')
                error = error + '\n' + data.get('ErrorName') + '\n' + data.get('ErrorMessage')
                _logger.info(error)
                capture.write({
                                'state': 'cancel'
                                })
                return False
