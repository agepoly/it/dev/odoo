# -*- coding: utf-8 -*-
#################################################################################
# Author      : PIT Solutions AG. (<https://www.pitsolutions.ch/>)
# Copyright(c): 2019 - Present PIT Solutions AG.
# License URL : https://www.webshopextension.com/en/licence-agreement/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.webshopextension.com/en/licence-agreement/>
#################################################################################

from odoo import api, models, fields, tools, _
from datetime import datetime
import logging
import json
from odoo.addons.payment.models.payment_acquirer import ValidationError
from odoo.exceptions import UserError
from odoo.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT


_logger = logging.getLogger(__name__)

class PaymentTransaction(models.Model):
    _inherit = 'payment.transaction' 

    is_saferpay_refund = fields.Boolean('Saferpay Refund')


    
    def saferpay_refund_inquire(self):
        for record in self:
            uri = "/Payment/v1/Transaction/Inquire"
            type = "POST"

            saferpay_acquirer = record.acquirer_id
            saferpay_acquirer_id = record.acquirer_id.id
            saferpay_customer_id = saferpay_acquirer.saferpay_customer_id
            saferpay_request_id = record.reference
            saferpay_tx_id = record.acquirer_reference
            
            json_data = {
                        "RequestHeader": saferpay_acquirer.get_request_header(saferpay_acquirer_id, saferpay_request_id),
                        "TransactionReference": {
                            "TransactionId": saferpay_tx_id
                          }
                    }                
            response = saferpay_acquirer.saferpay_send_request(saferpay_acquirer.id, \
                                                           type=type, uri=uri, \
                                                           json_data=json_data, txn_id=record.id)
            
            if response['status'] == 200:
                data = response['data']
                transaction = data['Transaction']
                saferpay_state = transaction['Status']
                record.write({
                            'saferpay_state': saferpay_state,
                            'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                            })
                if saferpay_state == "CAPTURED":
                    record._set_transaction_done()
                    record._post_process_after_done()
                    status = 'confirm'
                    record._send_status_mail(status)
                elif saferpay_state == "CANCELED":
                    record._set_transaction_cancel()
                    status = 'cancel'
                    record._send_status_mail(status)
                elif saferpay_state == "PENDING":
                    if record.state != 'pending':
                        record._set_transaction_pending()
                    return True
                elif saferpay_state == "AUTHORIZED":
                    record._set_transaction_authorized()
                    return True


    
    def saferpay_refund_done(self):
        self.ensure_one()
        return {
                'name': _('Transaction Done!!!'),
                'type': 'ir.actions.act_window',
                'res_model': 'saferpay.capture.wizard',
                'view_id': self.env.ref('payment_saferpay.view_saferpay_capture_wizard').id,
                'view_mode': 'form',
                'view_type': 'form',
                'context': {'default_transaction_id': self.id},
                'target': 'new'
                  }


    
    def _prepare_account_payment_vals(self):
        self.ensure_one()
        res = super(PaymentTransaction, self)._prepare_account_payment_vals()
        if self.is_saferpay_refund == True:
            res['payment_type'] = 'outbound'
        return res


    
    def _saferpay_get_txn_refund_status(self, request_id, saferpay_customer_id, saferpay_acquirer_id):
        self.ensure_one()
        if request_id and saferpay_customer_id and saferpay_acquirer_id:
            saferpay_acquirer = self.env['payment.acquirer'].browse(saferpay_acquirer_id)
            
            json_data = {}
            uri = "/Payment/v1/Transaction/AssertRefund"
            type = "POST"

            json_data = {
                       "RequestHeader": saferpay_acquirer.get_request_header(saferpay_acquirer_id, request_id),
                       "TransactionReference": {
                               "TransactionId": self.acquirer_reference
                                }, 

                }
        
            response = saferpay_acquirer.saferpay_send_request(saferpay_acquirer_id, \
                                                       type=type, uri=uri, \
                                                       json_data=json_data, txn_id=self.id)  
        return response

    
    def saferpay_capture_refund_transation(self, saferpay_acquirer_id):        
        self.ensure_one()
        saferpay_acquirer_id = int(saferpay_acquirer_id)
        saferpay_acquirer = self.env['payment.acquirer'].browse(saferpay_acquirer_id)
        uri = "/Payment/v1/Transaction/Capture"
        type = "POST"

        saferpay_customer_id = saferpay_acquirer.saferpay_customer_id
        saferpay_request_id = self.reference
        saferpay_tx_id = self.acquirer_reference
        
        json_data = {
                    "RequestHeader": saferpay_acquirer.get_request_header(saferpay_acquirer_id, saferpay_request_id),
                    "TransactionReference": {
                        "TransactionId": saferpay_tx_id
                      }
                }                
        response = saferpay_acquirer.saferpay_send_request(saferpay_acquirer_id, \
                                                       type=type, uri=uri, \
                                                       json_data=json_data, txn_id=self.id)  
        return response



    
    def saferpay_refund_form_validate(self, data):
        self.ensure_one()
        saferpay_acquirer = self.acquirer_id
        saferpay_customer_id = saferpay_acquirer.saferpay_customer_id
        saferpay_request_id = self.reference
        
        if self.saferpay_state == "PENDING":
            response = self._saferpay_get_txn_refund_status(saferpay_request_id, saferpay_customer_id, saferpay_acquirer)
            status = response.get('status', 0)                
            data = response.get('data', {})
            error = response.get('error', _('Error cant be retrived'))
            saferpay_state = ''
            if status == 200 and data:
                saferpay_trans = data
                saferpay_state = saferpay_trans.get('Status', '')
                self.write({
                            'saferpay_state': saferpay_state,
                            'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                            })
            
                if saferpay_state == 'CAPTURED':
                    self._set_transaction_done()
                    self._post_process_after_done()
                    status = 'confirm'
                    self._send_status_mail(status)
                    return True
                    
                elif saferpay_state == 'PENDING':
                    if self.state != 'pending':
                        self._set_transaction_pending()
                    return True

                elif saferpay_state == 'CANCELED':
                    self._set_transaction_cancel()
                    status = 'cancel'
                    self._send_status_mail(status)
                    return False
    
    
    def _send_status_mail(self, status):
        self.ensure_one()
        if status and status == 'cancel':
            if self.acquirer_id.send_status_email:
                try:        
                    template_id = self.env.ref('payment_saferpay.saferpay_email_template_payment_transaction_cancel')
                    if template_id:
                        template_id.send_mail(self.id, force_send=True)
                except Exception as e:
                    _logger.info("Error! email cannot be send %s" % (e,))
        if status and status == 'confirm':
            if self.acquirer_id.send_status_email:
                try:        
                    template_id = self.env.ref('payment_saferpay.saferpay_email_template_payment_transaction_confirm')
                    if template_id:
                        template_id.send_mail(self.id, force_send=True)
                except Exception as e:
                    _logger.info("Error! email cannot be send %s" % (e,))

    
    def _saferpay_get_refund_cancel(self, request_id, saferpay_customer_id, saferpay_acquirer_id):
        self.ensure_one()
        if request_id and saferpay_customer_id and saferpay_acquirer_id:
            saferpay_acquirer = self.env['payment.acquirer'].browse(saferpay_acquirer_id)
           
            json_data = {}
            uri = "/Payment/v1/Transaction/Cancel"
            type = "POST"

            json_data = {
                       "RequestHeader": saferpay_acquirer.get_request_header(saferpay_acquirer_id, request_id),
                       "TransactionReference": {
                               "TransactionId": self.acquirer_reference
                                }, 

                }
        
            response = saferpay_acquirer.saferpay_send_request(saferpay_acquirer_id, \
                                                       type=type, uri=uri, \
                                                       json_data=json_data, txn_id=self.id)  
            status = response.get('status', 0)                
            data = response.get('data', [])
            error = response.get('error', _('Error cant be retrived'))
            error = response.get('error', '')
            if status == 200 and data:
                res = {'request_id': data['ResponseHeader']['RequestId'], 'data': data, 'status' : status}
                txn_obj = self.env['payment.transaction'].search([('reference', '=', res.get('request_id'))])
                txn_obj.acquirer_reference = data.get('TransactionId', False)   
            else:
                res = {'request_id': False, 'error': error, 'data': data, 'status' : status} 
            return res


    def _create_payment(self, add_payment_vals={}):
        ''' Create an account.payment record for the current payment.transaction.
        If the transaction is linked to some invoices, the reconciliation will be done automatically.
        :param add_payment_vals:    Optional additional values to be passed to the account.payment.create method.
        :return:                    An account.payment record.
        '''
        self.ensure_one()
        payment_type = 'inbound' if self.amount > 0 else 'outbound'
        if self.is_saferpay_refund:
            payment_type = 'outbound'
        payment_vals = {
            'amount': self.amount,
            # 'payment_type': 'inbound' if self.amount > 0 else 'outbound',
            'payment_type': payment_type,
            'currency_id': self.currency_id.id,
            'partner_id': self.partner_id.id,
            'partner_type': 'customer',
            'journal_id': self.acquirer_id.journal_id.id,
            'company_id': self.acquirer_id.company_id.id,
            'payment_method_id': self.env.ref('payment.account_payment_method_electronic_in').id,
            'payment_token_id': self.payment_token_id and self.payment_token_id.id or None,
            'payment_transaction_id': self.id,
            'ref': self.reference,
            **add_payment_vals,
        }
        saferpay_refund = self.env.context.get('saferpay_refund', False)
        if saferpay_refund:
            payment_vals['saferpay_refund'] = saferpay_refund
        is_saferpay_refund = self.env.context.get('is_saferpay_refund', False)
        if is_saferpay_refund:
            payment_vals['is_saferpay_refund'] = is_saferpay_refund
        refund_type = self.env.context.get('refund_type', False)
        if refund_type:
            payment_vals['refund_type'] = refund_type
        is_token = self.env.context.get('is_token', False)
        if is_token:
            payment_vals['is_token'] = is_token
        token_id = self.env.context.get('token_id', False)
        if token_id:
            payment_vals['token_id'] = token_id.id
        refund_manage_type = self.env.context.get('refund_manage_type', False)
        if refund_manage_type:
            payment_vals['refund_manage_type'] = refund_manage_type

        payment = self.env['account.payment'].create(payment_vals)
        payment.action_post()

        # Track the payment to make a one2one.
        self.payment_id = payment

        if self.invoice_ids:
            self.invoice_ids.filtered(lambda move: move.state == 'draft')._post()

            (payment.line_ids + self.invoice_ids.line_ids)\
                .filtered(lambda line: line.account_id == payment.destination_account_id and not line.reconciled)\
                .reconcile()

        return payment



class AccountMove(models.Model):
    _inherit = 'account.move'

    _saferpay_pending_states = ['CREATE', 'CONFIRMED', \
                              'PROCESSING', 'COMPLETED']

    
    @api.depends('reversed_entry_id')
    def _compute_is_saferpay_refund(self):
        for record in self:
            if record.reversed_entry_id and any(saferpay_acquirer in record.reversed_entry_id.mapped('transaction_ids.acquirer_id').ids for saferpay_acquirer in self.env['payment.acquirer'].search([('provider', '=', 'saferpay')]).ids):
                record.is_saferpay_refund = True


    
    @api.depends('transaction_ids')
    def _compute_show_proceed_refund(self):
        for trans in self:
            if len(trans.transaction_ids.filtered(lambda t: t.saferpay_state == 'AUTHORIZED')) > 0:
                trans.proceed_refund = True
            else:
                trans.proceed_refund = False


    is_saferpay_refund = fields.Boolean(compute='_compute_is_saferpay_refund', store=True, string="Is saferpay Refund", default=False)
    refund_manage_type = fields.Selection([('customer', 'Refund managed by Customer'),
                                    ('merchant', 'Refund managed by Merchant')
                                    ], string = "Refund Managed By")
    proceed_refund = fields.Boolean(compute='_compute_show_proceed_refund', store=True, string="Show Proceed Refund", default=False)
    saferpay_active = fields.Boolean(default=False)


    
    def get_refund_amount(self, amount):
        amt = "0"
        if amount:
            amt = str(round(amount * 100))
        return amt


    
    def action_saferpay_refund(self, amount, currency):

        self.ensure_one()
        saferpay_acquirer = self.reversed_entry_id.transaction_ids.filtered(lambda r: r.provider == 'saferpay').mapped('acquirer_id')
        saferpay_acquirer = saferpay_acquirer and saferpay_acquirer[0] or False
        if not saferpay_acquirer:
            raise UserError(_('This Refund is not related to Saferpay.'))
        
        last_transaction = self.reversed_entry_id.transaction_ids.get_last_transaction()
        
            
        if currency != self.reversed_entry_id.currency_id:
            raise ValidationError(_('Currency should be same as that of Invoice.'))

        partner = self[0].partner_id
        vals = {}
        vals.update({
            'amount': amount,
            'currency_id': currency.id,
            'partner_id': partner.id,
            'acquirer_id': saferpay_acquirer.id,
            'invoice_ids': [(6, 0, self.ids)],
            'type': 'form',
            'is_saferpay_refund': True,
        })
        refund_transaction = self.env['payment.transaction'].sudo().create(vals)
        reference = refund_transaction.reference
       
        ################################
        json_data = {}
        uri = "/Payment/v1/Transaction/Refund"
        type = "POST"
        saferpay_acquirer_id = int(saferpay_acquirer.id)

        json_data = {
                    "RequestHeader": saferpay_acquirer.get_request_header(saferpay_acquirer_id, reference),
                    "Refund": {
                            "Amount": {
                                "Value": self.get_refund_amount(amount),
                                "CurrencyCode": currency.name
                                }
                            },

                   "CaptureReference": {
                            "TransactionId": last_transaction.acquirer_reference
                            }
                    }
        

        response = saferpay_acquirer.saferpay_send_request(saferpay_acquirer_id, \
                                                   type=type, uri=uri, \
                                                   json_data=json_data, txn_id=refund_transaction.id)

        status = response.get('status', 0)             
        data = response.get('data', [])
        error = response.get('error', '')
        saferpay_state = ''
        if status == 200 and data:
            saferpay_trans = data['Transaction']
            saferpay_state = saferpay_trans.get('Status', '')
            refund_transaction.write({
                                    'saferpay_state': saferpay_state,
                                    'acquirer_reference': saferpay_trans.get('Id', False),
                                    'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                                    })
        
        
            if saferpay_state == 'AUTHORIZED':
                refund_transaction._set_transaction_authorized()
                response = refund_transaction.saferpay_capture_refund_transation(saferpay_acquirer.id)
                data = response.get('data', [])
                saferpay_state = data['Status']
                if saferpay_state == "CAPTURED":
                    refund_transaction.write({'saferpay_state': saferpay_state})
                    refund_transaction._set_transaction_done()
                    refund_transaction._post_process_after_done()
                    status = 'confirm'
                    refund_transaction._send_status_mail(status)
                    return True
                elif saferpay_state == "PENDING":
                    refund_transaction._set_transaction_pending()
                    return True

            elif saferpay_state == 'PENDING':
                refund_transaction._set_transaction_pending()
                return True

            elif saferpay_state == 'CAPTURED':
                refund_transaction._set_transaction_done()
                refund_transaction._post_process_after_done()
                status = 'confirm'
                refund_transaction._send_status_mail(status)
                return True

            elif saferpay_state == 'CANCELED':
                refund_transaction._set_transaction_cancel()
                status = 'cancel'
                refund_transaction._send_status_mail(status)
                return True
        
        else:
            error = _('Saferpay: feedback error')
            error = error + '\n' + data.get('ErrorName') + '\n' + data.get('ErrorMessage')
            _logger.info(error)
            if data.get('Behavior') in refund_transaction._saferpay_cancel_messages:
                vals.update({
                            'state_message': "\n".join([refund_transaction.state_message or '', data.get('ErrorMessage', '')]),
                            'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                            })
                refund_transaction._set_transaction_cancel()
                status = 'cancel'
            elif data.get('Behavior') in refund_transaction._saferpay_error_messages:
                refund_transaction._set_transaction_error(error)
                status = 'error'
            else:  
                vals.update({
                            'state_message': "\n".join([refund_transaction.state_message or '', data.get('ErrorMessage', '')]),
                            'date': fields.Datetime.now(),
                            })
                status = refund_transaction.state
            refund_transaction.write(vals)
            refund_transaction._send_status_mail(status)
            return {
                'name': _('Saferpay Error ...!!!'),
                'type': 'ir.actions.act_window',
                'res_model': 'saferpay.warning.wizard',
                'view_id': self.env.ref('payment_saferpay.view_saferpay_warning_wizard').id,
                'view_mode': 'form',
                'view_type': 'form',
                'context': {'default_message': error},
                'target': 'new'
                  }

    
    def action_saferpay_direct_refund(self, token, amount=None, currency=None):
        self.ensure_one()

        token = self.env['payment.token'].browse(token)
        currency = self.env['res.currency'].browse(currency)
        if not currency:
            currency = self.currency_id
        if not amount:
            amount = self.amount_total

        if currency != self.reversed_entry_id.currency_id:
            raise ValidationError(_('Currency should be same as that of Invoice.'))


        saferpay_acquirer = token.acquirer_id
        saferpay_acquirer_id = int(saferpay_acquirer.id)

        partner = self[0].partner_id
        vals = {}
        vals.update({
            'amount': amount,
            'currency_id': currency.id,
            'partner_id': partner.id,
            'acquirer_id': saferpay_acquirer.id,
            'invoice_ids': [(6, 0, self.ids)],
            'type': 'form',
            'is_saferpay_refund': True,
            'payment_token_id': token.id,
        })
        refund_transaction = self.env['payment.transaction'].sudo().create(vals)
        reference = refund_transaction.reference

        ################################
        json_data = {}
        uri = "/Payment/v1/Transaction/RefundDirect"
        type = "POST"

        reference = self.reversed_entry_id.name

        json_data = {
                    "RequestHeader": saferpay_acquirer.get_request_header(saferpay_acquirer_id, reference),
                    "TerminalId": saferpay_acquirer.saferpay_terminal_id,
                    "Refund": {
                            "Amount": {
                                "Value": self.get_refund_amount(amount),
                                "CurrencyCode": currency.name
                                }
                        },
                    
                    "PaymentMeans": {
                        "Alias": {
                            "Id": token.acquirer_ref
                            }
                        }
                    }
        
        response = saferpay_acquirer.saferpay_send_request(saferpay_acquirer_id, \
                                                   type=type, uri=uri, \
                                                   json_data=json_data, txn_id=refund_transaction.id)

        status = response.get('status', 0)             
        data = response.get('data', [])
        error = response.get('error', '')
        saferpay_state = ''
        if status == 200 and data:
            saferpay_trans = data['Transaction']
            saferpay_state = saferpay_trans.get('Status', '')
            refund_transaction.write({
                                    'saferpay_state': saferpay_state,
                                    'acquirer_reference': saferpay_trans.get('Id', False),
                                    'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                                    })
        
            if saferpay_state == 'AUTHORIZED':
                refund_transaction._set_transaction_authorized()
                response = refund_transaction.saferpay_capture_refund_transation(saferpay_acquirer.id)
                data = response.get('data', [])
                saferpay_state = data['Status']
                if saferpay_state == "CAPTURED":
                    refund_transaction.write({'saferpay_state': saferpay_state})
                    refund_transaction._set_transaction_done()
                    refund_transaction._post_process_after_done()
                    status = 'confirm'
                    refund_transaction._send_status_mail(status)
                    return True

                elif saferpay_state == "PENDING":
                    refund_transaction._set_transaction_pending()
                    return True

            elif saferpay_state == 'PENDING':
                refund_transaction._set_transaction_pending()
                return True

            elif saferpay_state == 'CAPTURED':
                refund_transaction._set_transaction_done()
                refund_transaction._post_process_after_done()
                status = 'confirm'
                refund_transaction._send_status_mail(status)
                return True

            elif saferpay_state == 'CANCELED':
                refund_transaction._set_transaction_cancel()
                status = 'cancel'
                refund_transaction._send_status_mail(status)
                return True

        else:
            error = _('Saferpay: feedback error')
            error = error + '\n' + data.get('ErrorName') + '\n' + data.get('ErrorMessage')
            _logger.info(error)
            if data.get('Behavior') in refund_transaction._saferpay_cancel_messages:
                vals.update({
                            'state_message': "\n".join([refund_transaction.state_message or '', error]),
                            'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                            })
                refund_transaction._set_transaction_cancel()
                status = 'cancel'
            elif data.get('Behavior') in refund_transaction._saferpay_error_messages:
                refund_transaction._set_transaction_error(error)
                status = 'error'
            else:  
                vals.update({
                            'state_message': "\n".join([refund_transaction.state_message or '', error]),
                            'date': fields.Datetime.now(),
                            })
                status = refund_transaction.state
            refund_transaction.write(vals)
            refund_transaction._send_status_mail(status)
            return {
                'name': _('Saferpay Error ...!!!'),
                'type': 'ir.actions.act_window',
                'res_model': 'saferpay.warning.wizard',
                'view_id': self.env.ref('payment_saferpay.view_saferpay_warning_wizard').id,
                'view_mode': 'form',
                'view_type': 'form',
                'context': {'default_message': error},
                'target': 'new'
                  }
            
    @api.model
    def action_saferpay_direct_card_refund(self, form_data):
        '''
            Function to direct refund using card details
        '''

        invoice_id = int(form_data.get('invoice'))
        invoice = self.browse(invoice_id)

        if len(invoice.transaction_ids.filtered(lambda t: t.saferpay_state == 'AUTHORIZED')) > 0:
            raise ValidationError(_('You are already confirmed this refund.'))


        if invoice.currency_id != invoice.reversed_entry_id.currency_id:
            raise ValidationError(_('Currency should be same as that of Invoice.'))

        return_url = form_data.get('return_url')
        cc_number = form_data.get('cc_number')
        cc_holder_name = form_data.get('cc_holder_name')
        cc_brand = form_data.get('cc_brand')
        cvc = form_data.get('cvc')
        saferpay_acquirer_id = int(form_data.get('acquirer_id'))
        saferpay_acquirer = self.env['payment.acquirer'].browse(saferpay_acquirer_id)
        cc_expiry_year =  ''
        cc_expiry_month =  ''
        if form_data.get('cc_expiry'):
            date_format = datetime.strptime(form_data.get('cc_expiry'),'%m / %y').strftime('%Y / %m')
            cc_expiry_year = date_format.split(' / ')[0]
            cc_expiry_month = date_format.split(' / ')[1]

        partner = invoice[0].partner_id
        vals = {}
        vals.update({
            'amount': sum(invoice.mapped('amount_total')),
            'currency_id': invoice.currency_id.id,
            'partner_id': partner.id,
            'acquirer_id': saferpay_acquirer.id,
            'invoice_ids': [(6, 0, invoice.ids)],
            'type': 'form',
            'is_saferpay_refund': True,
        })
        refund_transaction = self.env['payment.transaction'].sudo().create(vals)
        reference = refund_transaction.reference
        ################################

        json_data = {}
        uri = "/Payment/v1/Transaction/RefundDirect"
        type = "POST"

        reference = invoice.reversed_entry_id.name

        json_data = {
                    "RequestHeader": saferpay_acquirer.get_request_header(saferpay_acquirer_id, reference),
                    "TerminalId": saferpay_acquirer.saferpay_terminal_id,
                    "Refund": {
                            "Amount": {
                                "Value": self.get_refund_amount(invoice.amount_total),
                                "CurrencyCode": invoice.currency_id.name
                                }
                        }
                    }
        if cc_number and cc_holder_name and cc_expiry_year and cc_expiry_month:

            json_data.update({"PaymentMeans": {
                                "Card": {
                                    "Number": cc_number,
                                    "ExpYear": cc_expiry_year,
                                    "ExpMonth": cc_expiry_month,
                                    "HolderName": cc_holder_name,
                                    }
                                }
                        })
        if cc_number and cc_holder_name and cvc and cc_brand:

            json_data.update({"PaymentMeans": {
                                "BankAccount": {
                                    "IBAN": cc_number,
                                    "HolderName": cc_holder_name,
                                    "BIC": cvc,
                                    "BankName": cc_brand,
                                            }
                                        }
                            })
    
        response = saferpay_acquirer.saferpay_send_request(saferpay_acquirer_id, \
                                                   type=type, uri=uri, \
                                                   json_data=json_data, txn_id=refund_transaction.id)
        status = response.get('status', 0)             
        data = response.get('data', [])
        error = response.get('error', '')
        saferpay_state = ''
        if status == 200 and data:
            saferpay_trans = data['Transaction']
            saferpay_state = saferpay_trans.get('Status', '')
            refund_transaction.write({
                                    'saferpay_state': saferpay_state,
                                    'acquirer_reference': saferpay_trans.get('Id', False),
                                    'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                                    })
        
            if saferpay_state == 'AUTHORIZED':
                refund_transaction._set_transaction_authorized()
                return True

            elif saferpay_state == 'PENDING':
                refund_transaction._set_transaction_pending()
                return True

            elif saferpay_state == 'CAPTURED':
                refund_transaction._set_transaction_done()
                refund_transaction._post_process_after_done()
                status = 'confirm'
                refund_transaction._send_status_mail(status)
                return True

            elif saferpay_state == 'CANCELED':
                refund_transaction._set_transaction_cancel()
                status = 'cancel'
                refund_transaction._send_status_mail(status)
                return True
        
        else:
            error = _('Saferpay: feedback error')
            error = error + '\n' + data.get('ErrorName') + '\n' + data.get('ErrorMessage')
            _logger.info(error)
            if data.get('Behavior') in refund_transaction._saferpay_cancel_messages:
                vals.update({
                            'state_message': "\n".join([refund_transaction.state_message or '', error]),
                            'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                            })
                refund_transaction._set_transaction_cancel()
                status = 'cancel'
            elif data.get('Behavior') in refund_transaction._saferpay_error_messages:
                refund_transaction._set_transaction_error(error)
                status = 'error'
            else:  
                vals.update({
                            'state_message': "\n".join([refund_transaction.state_message or '', error]),
                            'date': fields.Datetime.now(),
                            })
                status = refund_transaction.state
            refund_transaction.write(vals)
            refund_transaction._send_status_mail(status)
            return {
                'name': _('Saferpay Error ...!!!'),
                'type': 'ir.actions.act_window',
                'res_model': 'saferpay.warning.wizard',
                'view_id': self.env.ref('payment_saferpay.view_saferpay_warning_wizard').id,
                'view_mode': 'form',
                'view_type': 'form',
                'context': {'default_message': error},
                'target': 'new'
                  }
        invoice.sudo()._compute_show_proceed_refund()
        if return_url:
            return return_url

    
    def assign_outstanding_credit(self, credit_aml_id):
        self.ensure_one()
        if self.is_saferpay_refund and self.env.context.get('is_saferpay_refund'):
            return True
        else:
            return super(AccountInvoice, self).assign_outstanding_credit(credit_aml_id)


class AccountPayment(models.Model):
    _inherit = 'account.payment'

    
    @api.depends('reconciled_invoice_ids')
    def _compute_is_saferpay_refund(self):
        # self.ensure_one()
        for record in self:
            invoice = self.env['account.move'].browse(self.env.context.get('active_id'))
            if invoice.reversed_entry_id and any(saferpay_acquirer in invoice.reversed_entry_id.mapped('transaction_ids.acquirer_id').ids for saferpay_acquirer in self.env['payment.acquirer'].search([('provider', '=', 'saferpay')]).ids):
                record.is_saferpay_refund = True

    saferpay_refund = fields.Boolean(string="Saferpay Refund", default=False)
    is_saferpay_refund = fields.Boolean(compute='_compute_is_saferpay_refund', store=True, default=False)
    refund_type = fields.Selection([('referenced', 'Referenced Refund'),
                                    ('direct', 'Direct Refund')
                                    ], compute="_compute_refund_type", store=True)
    is_token = fields.Boolean(string="Contains token for this transaction?", compute="_compute_token", default=False, store=True)
    token_id = fields.Many2one('payment.token', string="Token")
    refund_manage_type = fields.Selection([('customer', 'Manage by Customer'),
                                    ('merchant', 'Manage by Merchant')
                                    ], string = "Refund Managed By")

    @api.onchange('is_token','refund_type')
    def onchange_refund_manage_type(self):
        if self.is_token != True and self.refund_type == 'direct':
            self.refund_manage_type = 'customer'

    @api.onchange('refund_type', 'refund_manage_type')
    def onchange_token_domain(self):
        domain = {}
        tokens = []
        if self.refund_type and self.refund_manage_type == 'merchant':
            support_refund_type = [self.refund_type] + ['both']
            invoice = self.env['account.move'].browse(self.env.context.get('active_id'))
            tokens = self.env['payment.token'].search([('partner_id','=', invoice.partner_id.id),
                                                        ('acquirer_id.is_support_refund','=', True),
                                                        ('acquirer_id.support_refund_type','in', support_refund_type),
                                                        ('acquirer_id.license_type','=', 'business'),
                                                        ('acquirer_id.currency_ids','in', self.currency_id.ids)
                                                        ])
            domain = {'token_id': [('id', 'in', tokens.ids)]}
        return {'domain': domain}

    
    @api.depends('saferpay_refund','refund_type')
    def _compute_token(self):
        for record in self:
            if record.refund_type:
                support_refund_type = [record.refund_type] + ['both']
                invoice = self.env['account.move'].browse(record.env.context.get('active_id'))
                tokens = self.env['payment.token'].search([('partner_id','=', invoice.partner_id.id),
                                                            ('acquirer_id.is_support_refund','=', True),
                                                            ('acquirer_id.support_refund_type','in', support_refund_type),
                                                            ('acquirer_id.license_type','=', 'business'),
                                                            ('acquirer_id.currency_ids','in', record.currency_id.ids)
                                                            ])
                if tokens:
                    record.is_token = True
                else:
                    record.is_token = False


    
    @api.depends('saferpay_refund')
    def _compute_refund_type(self):
        for record in self:
            invoice = self.env['account.move'].browse(record.env.context.get('active_id'))
            tokens = self.env['payment.token'].search([('partner_id','=', invoice.partner_id.id), ('acquirer_id.is_support_refund','=', True)])
            transactions = self.env['payment.transaction'].search([
                                                    ('acquirer_id.is_support_refund','=', True),
                                                    ('acquirer_id.support_refund_type','in', ['referenced','both']),
                                                    ('acquirer_id.license_type','=', 'business'),
                                                    ('acquirer_id.currency_ids','in', record.currency_id.ids),
                                                    ('invoice_ids','in', invoice.reversed_entry_id.ids),
                                                    ('acquirer_reference','!=', False),
                                                    ('state','=','done')
                                                    ])
            
            if transactions:
                record.refund_type = 'referenced'
            else:
                record.refund_type = 'direct'

    def action_validate_invoice_payment(self):
        invoice = self.env['account.move'].browse(self.env.context.get('active_id'))
        transactions = invoice.transaction_ids.filtered(lambda r: r.saferpay_state in ['PENDING','AUTHORIZED'] and r.acquirer_id.provider == 'saferpay')
        if transactions:
            raise UserError(_('You have Payment Transactions that are not completed. Goto Payment Transactions and update the status'))
        else:
            if self.saferpay_refund == True:
                invoice.refund_manage_type = self.refund_manage_type
                amount = self.amount
                currency = self.currency_id
                if self.amount > invoice.residual:
                    raise UserError(_('You are not allowed to refund more than the remaining amount'))
                
                refund_amount = 0.0
                refund_invoices = self.env['account.move'].search([
                                                    ('id','!=', invoice.id),
                                                    ('saferpay_active','=', True),
                                                    ('reversed_entry_id','=', invoice.reversed_entry_id.id),
                                                    ('state','!=', 'cancel')])
                for inv in refund_invoices:
                    refund_amount += inv.amount_total
                refund_amount = refund_amount + (invoice.amount_total - invoice.residual)
                if round(invoice.reversed_entry_id.amount_total - refund_amount, 2) < self.amount:
                    raise UserError(_('You are not allowed to refund more than the amount in invoice'))
                else:
                    invoice.saferpay_active = True

                if self.refund_type == 'referenced':
                    return invoice.action_saferpay_refund(amount,currency)
                else:
                    if self.refund_manage_type == 'merchant':
                        return invoice.action_saferpay_direct_refund(self.token_id.id,amount,currency.id)
                    else:
                        acquirers = self.env['payment.acquirer'].search([('license_type','=','business'),
                                                            ('is_support_refund','=', True),
                                                            ('support_refund_type','in', ['direct','both'])
                                                            ])
                        if len(acquirers) >= 1:
                            return True
                        else:
                            raise ValidationError(_("The payment cannot be processed through saferpay because there is no acquirer supported for this action!"))
            else:
                res = super(AccountPayment, self).action_validate_invoice_payment()
                return res
