# -*- coding: utf-8 -*-
#################################################################################
# Author      : PIT Solutions AG. (<https://www.pitsolutions.ch/>)
# Copyright(c): 2019 - Present PIT Solutions AG.
# License URL : https://www.webshopextension.com/en/licence-agreement/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.webshopextension.com/en/licence-agreement/>
#################################################################################

import datetime
from psycopg2 import OperationalError
import logging
from odoo import api, fields, models, registry, _

from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError

_logger = logging.getLogger(__name__)

EXCEPTION_LOG_TYPE = {
    ('red', _("Internal error")),
    ('olive', _("Warning")),
    ('gray', _("Info")),
    ('green', _("Success")),
    ('rosybrown', _("Validation error")),
    ('chocolate', _("Authentication of the request failed")),
    ('brown', _("Requested action failed")),
    ('tomato', _("Access denied")),
    ('firebrick', _("Not acceptable")),
    ('orange', _("Unsupported media type")),
}


class SaferpayAcquirerLog(models.Model):
    _name = "saferpay.acquirer.log"
    _description = "Saferpay acquirer log details"
    _order = "id desc"

    name = fields.Char(string="Description", required=True, readonly=True)
    detail = fields.Html(readonly=True)
    origin = fields.Char(default='saferpay', readonly=True)    
    saferpay_config_id = fields.Many2one('saferpay.acquirer.config', string='Acquirer Config', 
                                        ondelete='set null', groups='base.group_user', readonly=True) 
    acquirer_id = fields.Many2one('payment.acquirer', string='Acquirer', 
                                        ondelete='set null', groups='base.group_user', readonly=True)  
    txn_id = fields.Integer(string='Transaction ID', groups='base.group_user', readonly=True)    
    txn_ref = fields.Char(string='Transaction Ref', groups='base.group_user', readonly=True)  
    type = fields.Selection(EXCEPTION_LOG_TYPE, default='gray', readonly=True, required=True)
    request_url = fields.Char(readonly=True)
    saferpay_txn_ref = fields.Char(string='Saferpay Transaction Ref', groups='base.group_user', readonly=True) 
    response_header = fields.Text(readonly=True)
    response_error = fields.Text(readonly=True)
    request_ip = fields.Text(readonly=True)
    request_browser = fields.Text(readonly=True)
    referrer_path = fields.Text(readonly=True)
    request_country_code = fields.Text(readonly=True, string='Requested Country Code')
    request_state_code = fields.Text(readonly=True, string='Requested Region Code')
    request_country_id = fields.Many2one('res.country', string='Requested Country', 
                                        ondelete='set null', groups='base.group_user', readonly=True)
    request_state_id = fields.Many2one('res.country.state', string='Requested State', 
                                        ondelete='set null', groups='base.group_user', readonly=True)

    @api.model
    def clean_old_logging(self, days=90):
        """
        Function called by a cron to clean old loggings.
        @return: True
        """
        last_days = datetime.datetime.now() +\
            datetime.timedelta(days=-days)
        domain = [
            ('create_date', '<', last_days.strftime(
                DEFAULT_SERVER_DATETIME_FORMAT))
        ]
        logs = self.search(domain)
        logs.unlink()
        message = " %d logs are deleted" % (len(logs))
        return self._post_log({'name': message})

    @api.model
    def _post_log(self, vals):
        saferpay_log_id = 0
        saferpay_log = self.create(vals)
        saferpay_log_id = saferpay_log.id
        # cr = registry(self._cr.dbname).cursor()
        # self = self.with_env(self.env(cr=cr))
        # try:
        #     try:            
        #         saferpay_log = self.create(vals)
        #         saferpay_log_id = saferpay_log.id
        #         self._cr.commit()
        #     except OperationalError:
        #         self._cr.rollback()
        # finally:
        #     try:
        #         self._cr.close()
        #     except Exception as e:
        #         _logger.info("An error occured while processing _post_log on the model saferpay_acquirer_log: %s" % (e,))
        return saferpay_log_id
