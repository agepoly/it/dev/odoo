# -*- coding: utf-8 -*-
#################################################################################
# Author      : PIT Solutions AG. (<https://www.pitsolutions.ch/>)
# Copyright(c): 2019 - Present PIT Solutions AG.
# License URL : https://www.webshopextension.com/en/licence-agreement/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.webshopextension.com/en/licence-agreement/>
#################################################################################

from odoo import api, models, fields, tools, _

SAFERPAY_CODE = [
        ('MYONE', _('MYONE')),
        ('PAYDIREKT', _('PAYDIREKT')),
        ('IDEAL', _('IDEAL')),
        ('TWINT', _('TWINT')),
        ('DINERS', _('DINERS')),
        ('SAFERPAYTEST', _('SAFERPAYTEST')),
        ('EPRZELEWY', _('EPRZELEWY')),
        ('EPS', _('EPS')),
        ('DISCOVER', _('DISCOVER')),
        ('GIROPAY', _('GIROPAY')),
        ('DIRECTDEBIT', _('DIRECTDEBIT')),
        ('SOFORT', _('SOFORT')),
        ('UNIONPAY', _('UNIONPAY')),
        ('BONUS', _('BONUS')),
        ('VPAY', _('VPAY')),
        ('VISA', _('VISA')),
        ('PAYPAL', _('PAYPAL')),
        ('ALIPAY', _('ALIPAY')),
        ('JCB', _('JCB')),
        ('MAESTRO', _('MAESTRO')),
        ('BANCONTACT', _('BANCONTACT')),
        ('MASTERCARD', _('MASTERCARD')),
        ('AMEX', _('AMEX')),
        ('DIRECTDEBIT', _('DIRECTDEBIT')),
        ('INVOICE', _('INVOICE')),
        ('POSTCARD', _('POSTCARD')),
        ('POSTFINANCE', _('POSTFINANCE')),
        ('MASTERPASS', _('MASTERPASS')),
        ('APPLEPAY', _('APPLEPAY')),
    ]

class PaymentIcon(models.Model):
    _inherit = 'payment.icon'
    
    @api.model
    def default_get(self, default_fields):
        """ Compute default is_saferpay  field for Saferpay acquirer,
        using the context.
        """
        res = super(PaymentIcon, self).default_get(default_fields)

        provider = self._context.get('provider')
        if provider == 'saferpay':
            res['is_saferpay'] = True
        return res
    
    is_saferpay = fields.Boolean("Saferpay", readonly=True)
    saferpay_code = fields.Selection(SAFERPAY_CODE)
    currency_ids = fields.Many2many('res.currency', 'currency_icon_rel', 'icon_id', 'currency_id', string="Allowed currencies")
    support_saferpay_fields = fields.Boolean("Supports Saferpay Fields")

    def change_support_saferpay_fields(self):
        if self.support_saferpay_fields:
            self.support_saferpay_fields = False
        else:
            self.support_saferpay_fields = True

