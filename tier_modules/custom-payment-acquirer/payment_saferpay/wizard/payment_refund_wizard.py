# -*- coding: utf-8 -*-
#################################################################################
# Author      : PIT Solutions AG. (<https://www.pitsolutions.ch/>)
# Copyright(c): 2019 - Present PIT Solutions AG.
# License URL : https://www.webshopextension.com/en/licence-agreement/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.webshopextension.com/en/licence-agreement/>
#################################################################################

from odoo import api, models, fields, tools, _
from odoo.exceptions import UserError

class AccountMove(models.Model):
    _inherit = 'account.move'

    def action_register_payment(self):
        ''' Open the account.payment.register wizard to pay the selected journal entries.
        :return: An action opening the account.payment.register wizard.
        '''
        transactions = self.env['payment.transaction'].search([
            ('acquirer_id.is_support_refund','=', True),
            ('acquirer_id.support_refund_type','in', ['referenced','both']),
            ('acquirer_id.license_type','=', 'business'),
            ('acquirer_id.currency_ids','in', self.currency_id.ids),
            ('invoice_ids','in', self.reversed_entry_id.ids),
            ('acquirer_reference','!=', False),
            ('state','=','done')
            ])
            
        if transactions:
            refund_type = 'referenced'
        else:
            refund_type = 'direct'
        support_refund_type = [refund_type] + ['both']


        tokens = self.env['payment.token'].search([('partner_id','=', self.partner_id.id),
            ('acquirer_id.is_support_refund','=', True),
            ('acquirer_id.support_refund_type','in', support_refund_type),
            ('acquirer_id.license_type','=', 'business'),
            ('acquirer_id.currency_ids','in', self.currency_id.ids)
            ])
        is_token = False
        if tokens:
            is_token = True

        is_saferpay_refund = False
        if self.reversed_entry_id and any(saferpay_acquirer in self.reversed_entry_id.mapped('transaction_ids.acquirer_id').ids for saferpay_acquirer in self.env['payment.acquirer'].search([('provider', '=', 'saferpay')]).ids):
             is_saferpay_refund = True  
        return {
            'name': _('Register Payment'),
            'res_model': 'account.payment.register',
            'view_mode': 'form',
            'context': {
                'active_model': 'account.move',
                'active_ids': self.ids,
                'default_invoice_id': self.id,
                'default_amount_residual': self.amount_residual,
                'default_is_saferpay_refund': is_saferpay_refund,
                'default_is_token': is_token,
            },
            'target': 'new',
            'type': 'ir.actions.act_window',
        }



class AccountPaymentRegister(models.TransientModel):
    _inherit = 'account.payment.register'

    @api.depends('invoice_id')
    def _compute_is_saferpay_refund(self):
        # self.ensure_one()
        for record in self:
            invoice = self.env['account.move'].browse(self.env.context.get('active_id'))
            if not invoice:
                invoice = record.invoice_id
            if invoice.reversed_entry_id and any(saferpay_acquirer in invoice.reversed_entry_id.mapped('transaction_ids.acquirer_id').ids for saferpay_acquirer in self.env['payment.acquirer'].search([('provider', '=', 'saferpay')]).ids):
                record.is_saferpay_refund = True

    saferpay_refund = fields.Boolean(string="Saferpay Refund", default=False)
    is_saferpay_refund = fields.Boolean(compute='_compute_is_saferpay_refund', store=True, default=False)
    refund_type = fields.Selection([('referenced', 'Referenced Refund'),
                                    ('direct', 'Direct Refund')
                                    ], compute="_compute_refund_type", store=True)
    is_token = fields.Boolean(string="Contains token for this transaction?", compute="_compute_token", default=False, store=True)
    token_id = fields.Many2one('payment.token', string="Token")
    refund_manage_type = fields.Selection([
                                    # ('customer', 'Manage by Customer'),
                                    ('merchant', 'Manage by Merchant')
                                    ], string = "Refund Managed By")
    invoice_id = fields.Many2one('account.move', string="Invoice")
    amount_residual = fields.Float(string="Residual Amount")

    @api.onchange('is_token','refund_type')
    def onchange_refund_manage_type(self):
        if self.is_token != True and self.refund_type == 'direct':
            self.refund_manage_type = 'customer'

    @api.onchange('refund_type', 'refund_manage_type')
    def onchange_token_domain(self):
        domain = {}
        tokens = []
        if self.refund_type and self.refund_manage_type == 'merchant':
            support_refund_type = [self.refund_type] + ['both']
            invoice = self.env['account.move'].browse(self.env.context.get('active_id'))
            tokens = self.env['payment.token'].search([('partner_id','=', invoice.partner_id.id),
                                                        ('acquirer_id.is_support_refund','=', True),
                                                        ('acquirer_id.support_refund_type','in', support_refund_type),
                                                        ('acquirer_id.license_type','=', 'business'),
                                                        ('acquirer_id.currency_ids','in', self.currency_id.ids)
                                                        ])
            domain = {'token_id': [('id', 'in', tokens.ids)]}
        return {'domain': domain}

    
    @api.depends('saferpay_refund','refund_type')
    def _compute_token(self):
        for record in self:
            if record.refund_type:
                support_refund_type = [record.refund_type] + ['both']
                invoice = self.env['account.move'].browse(record.env.context.get('active_id'))
                if not invoice:
                    invoice = record.invoice_id
                tokens = self.env['payment.token'].search([('partner_id','=', invoice.partner_id.id),
                                                            ('acquirer_id.is_support_refund','=', True),
                                                            ('acquirer_id.support_refund_type','in', support_refund_type),
                                                            ('acquirer_id.license_type','=', 'business'),
                                                            ('acquirer_id.currency_ids','in', record.currency_id.ids)
                                                            ])
                if tokens:
                    record.is_token = True
                else:
                    record.is_token = False


    
    @api.depends('saferpay_refund')
    def _compute_refund_type(self):
        for record in self:
            invoice = self.env['account.move'].browse(self.env.context.get('active_id'))
            if not invoice:
                invoice = self.invoice_id
            # tokens = self.env['payment.token'].search([('partner_id','=', invoice.partner_id.id), ('acquirer_id.is_support_refund','=', True)])
            transactions = self.env['payment.transaction'].search([
                                                    ('acquirer_id.is_support_refund','=', True),
                                                    ('acquirer_id.support_refund_type','in', ['referenced','both']),
                                                    ('acquirer_id.license_type','=', 'business'),
                                                    ('acquirer_id.currency_ids','in', record.currency_id.ids),
                                                    ('invoice_ids','in', invoice.reversed_entry_id.ids),
                                                    ('acquirer_reference','!=', False),
                                                    ('state','=','done')
                                                    ])
            
            if transactions:
                record.refund_type = 'referenced'
            else:
                record.refund_type = 'direct'



    def _create_payments(self):
        if self.saferpay_refund == True:
            invoice = self.env['account.move'].browse(self.env.context.get('active_id'))
            if not invoice:
                invoice = self.invoice_id
            amount_residual = invoice.amount_residual
            if not amount_residual:
                amount_residual = self.amount_residual
            transactions = invoice.transaction_ids.filtered(lambda r: r.saferpay_state in ['PENDING','AUTHORIZED'] and r.acquirer_id.provider == 'saferpay')
            if transactions:
                raise UserError(_('You have Payment Transactions that are not completed. Goto Payment Transactions and update the status'))
            else:
                invoice.refund_manage_type = self.refund_manage_type
                amount = self.amount
                currency = self.currency_id
                # if self.amount > invoice.amount_residual:
                if self.amount > amount_residual:
                    raise UserError(_('You are not allowed to refund more than the remaining amount'))
                
                refund_amount = 0.0
                refund_invoices = self.env['account.move'].search([
                                                    ('id','!=', invoice.id),
                                                    ('saferpay_active','=', True),
                                                    ('reversed_entry_id','=', invoice.reversed_entry_id.id),
                                                    ('state','!=', 'cancel')])
                for inv in refund_invoices:
                    refund_amount += inv.amount_total
                # refund_amount = refund_amount + (invoice.amount_total - invoice.amount_residual)
                refund_amount = refund_amount + (invoice.amount_total - amount_residual)

                if round(invoice.reversed_entry_id.amount_total - refund_amount, 2) < self.amount:
                    raise UserError(_('You are not allowed to refund more than the amount in invoice'))
                else:
                    invoice.saferpay_active = True
                invoice.saferpay_active = True
                
                payment_context = dict(self.env.context, saferpay_refund=self.saferpay_refund, is_saferpay_refund=self.is_saferpay_refund, refund_type=self.refund_type, is_token=self.is_token, token_id=self.token_id, refund_manage_type=self.refund_manage_type)


                if self.refund_type == 'referenced':
                    return invoice.with_context(payment_context).action_saferpay_refund(amount,currency)
                else:
                    if self.refund_manage_type == 'merchant':
                        return invoice.with_context(payment_context).action_saferpay_direct_refund(self.token_id.id,amount,currency.id)
                    else:
                        acquirers = self.env['payment.acquirer'].search([('license_type','=','business'),
                                                            ('is_support_refund','=', True),
                                                            ('support_refund_type','in', ['direct','both'])
                                                            ])
                        if len(acquirers) >= 1:
                            return True
                        else:
                            raise ValidationError(_("The payment cannot be processed through saferpay because there is no acquirer supported for this action!"))
        else:
            return super(AccountPaymentRegister, self)._create_payments()


class SaferpayWaringWizard(models.TransientModel):
    _name = 'saferpay.warning.wizard'
    _description = "Saferpay Warning"

    message = fields.Text(readonly=True)

class SaferpayCaptureWizard(models.TransientModel):
    _name = 'saferpay.capture.wizard'
    _description = "Saferpay Capture"

    refund_done = fields.Selection([('with_capture', 'Done With Capture API'),
                                            ('without_capture', 'Done Without Capture API')
                                            ], string="Need Manual Capture?")
    transaction_id = fields.Many2one('payment.transaction', string="Transaction")

    
    
    def button_done(self):
        if self.refund_done == 'with_capture' and self.transaction_id.saferpay_state == 'AUTHORIZED':
            response = self.transaction_id.saferpay_capture_refund_transation(self.transaction_id.acquirer_id.id)
            data = response.get('data', [])
            if data.get('ErrorMessage'):
                ErrorMessage = data.get('ErrorMessage', [])
                if ErrorMessage:
                    raise ValidationError(_(ErrorMessage))
            saferpay_state = data['Status']
            if saferpay_state == "CAPTURED":
                self.transaction_id.write({'saferpay_state': saferpay_state})
                self.transaction_id._set_transaction_done()
                self.transaction_id._post_process_after_done()
                
            elif saferpay_state == "PENDING":
                refund_transaction._set_transaction_pending()
        else:
            self.transaction_id._set_transaction_done()
            self.transaction_id._post_process_after_done()
        return