============
INSTALLATION
============


This module modify tables used at startup and may make odoo fail to start. 
You may need to run this manually to upgrade it :

/usr/bin/python3 /usr/bin/odoo --no-http -u agepoly_users --without-demo=all --db_host 172.17.0.1 --db_port 5432 --db_user odoo --db_password odoo --database odoo --no-database-list