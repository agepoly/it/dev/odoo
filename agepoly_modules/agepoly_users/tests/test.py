# Copyright 2022 'AGEPoly'
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl)

from odoo import api, registry
from odoo.tests import common
from odoo.tests.common import TransactionCase



class TestResUsers(TransactionCase):
    def setUp(self):
        super().setUp()
        self.user = self.env["res.users"].browse(1)
        self._model = self.env["res.users"]

    def test_login(self):
        self.user.write({'groups_id': self.user._generate_sso_groups_values(["test_sso_1", "test_sso2"])})
        group = self.env["res.groups"].search(["name", "=", "test_sso_1"])
        self.assertTrue(group, "sso groups must be created if needed")
        self.assertTrue(group.id in self.user.groups_id.ids, "user must be added to sso groups")

        self.user.write({'groups_id': self.user._generate_sso_groups_values(["test_sso2"])})
        self.assertTrue(group.id not in self.user.groups_id.ids, "user must be removed from sso groups")

