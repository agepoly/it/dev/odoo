# -*- coding: utf-8 -*-

from odoo import api, models, fields, _
from odoo.exceptions import UserError
import logging

_logger = logging.getLogger()

SSO_NAME = "truffe__%s"
SSO_INTERNAL_GROUP = SSO_NAME % "any__%"

class Partner(models.Model):
    _inherit = 'res.partner'

    ref = fields.Char(string="Sciper (or Truffe Username)") 
    _sql_constraints = [
        ('ref', 'unique(ref)', 'partner with sciper/username already exists!')
    ]

class Groups(models.Model):
    _inherit = 'res.groups'

    is_sso_group = fields.Boolean()

class User(models.Model):
    _inherit = 'res.users'

    oauth_uid = fields.Char(related="partner_id.ref", readonly=False)

    def _generate_sso_groups_values(self, groups):
        groups = groups.split(",")
        groups = [SSO_NAME % gr for gr in groups]
        target_groups_names = self.env["res.groups"].search([['name', 'in', groups]]).mapped('name')
        groups_to_create = []
        for gr in groups:
            if gr not in target_groups_names:
                # Group not existing, creating it
                groups_to_create.append({'name': gr, 'is_sso_group': True})

        if groups_to_create:
            self.env["res.groups"].create(groups_to_create)

        if self and self[0]:
            existing_tr_groups_ids = self.env["res.groups"].search([('is_sso_group', '=', True), ('users.id', "=", self[0].id)])
        else:
            existing_tr_groups_ids = self.env["res.groups"]

        target_groups = self.env["res.groups"].search([['name', 'in', groups]])

        groups_to_remove = existing_tr_groups_ids - target_groups 
        groups_to_add = target_groups - existing_tr_groups_ids

        return [(3, g.id, 0) for g in groups_to_remove] + [(4, g.id, 0) for g in groups_to_add]

    @api.model
    def _auth_oauth_signin(self, provider, validation, params):    
        login = super(User, self)._auth_oauth_signin(provider, validation, params)
        user = self.search([("login", "=", login)])
        values = {
            'groups_id': user._generate_sso_groups_values(validation.get("groups", ""))
        }

        if validation.get("email", login) != login:
            values['login'] = validation["email"] 

        if validation.get("b64name", False) and b64decode(validation["b64name"]) != user.name:
           values['name'] = b64decode(validation["b64name"])

        user.write(values)
        user._after_group_update()
        return login

    def _after_group_update(self):
        for user in self:
            operating_unit_ids = self.env['operating.unit'].search([(1, '=', 1)])
            main_group_ids = operating_unit_ids.mapped('main_group_id.id') # get all existing main groups (groups that bind represent the OU direct members)
            
            # get all user main groups
            groups = self.env["res.groups"].search(
                [
                    ('id', 'in', main_group_ids), 
                    ('users.id', '=', user.id)
                ])

            # filter the OU in wich the user is direct member
            direct_ou = operating_unit_ids.filtered(lambda ou: ou.main_group_id in groups) # filter the
            user.operating_unit_ids = direct_ou.mapped('all_child_ids.id') # then add unit + all childs units as user units.

            if len(groups) > 0:  # add user to internal user
                user.write({'sel_groups_1_9_10': 1})
            else:  # add user to portal users
                user.write({'sel_groups_1_9_10': 9})


    def signup(self, values, token=None):
        """ signup a user, when a partner with ref = oauth_uid already exist else normal flow
        """
        if values.get('oauth_uid', False):
            # signup with a token: find the corresponding partner id
            partner = self.env['res.partner'].search([('ref', '=', values.get('oauth_uid'))])
            if partner and partner[0]:
                if partner.user_ids and partner.user_ids[0]:
                    _logger.error(
                        _(
                            "Signup trial for oauth_uid = %{oauth_uid}, user %{username} (%{userid}) already exists, may need manual action.",
                            values.get('oauth_uid'),
                            partner.user_ids[0].name,
                            partner.user_ids[0].id
                        )
                    )
    
                # avoid overwriting existing (presumably correct) values with geolocation data
                if partner.country_id or partner.zip or partner.city:
                    values.pop('city', None)
                    values.pop('country_id', None)
                if partner.lang:
                    values.pop('lang', None)
    
                values.update({
                    'name': partner.name,
                    'partner_id': partner.id,
                    'email': values.get('email') or values.get('login')
                })
                if partner.company_id:
                    values['company_id'] = partner.company_id.id
                    values['company_ids'] = [(6, 0, [partner.company_id.id])]

                partner_user = self._signup_create_user(values)
                return (self.env.cr.dbname, values.get('login'), values.get('password'))

        return super(User, self).signup(values, token)