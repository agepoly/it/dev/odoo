# Copyright 2014-2018 'AGEPoly'
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl)

{
    "name": "AGEPoly Custom users management",
    "version": "14.0.1.0.1",
    "category": "Tools",
    "development_status": "Production/Stable",
    "author": "AGEPoly",
    "maintainer": "AGEPoly",
    "website": "agepoly.ch",
    "depends": ["base", "web", "base_setup", "mail", "auth_oauth"],
    "license": "AGPL-3",
    'data': [
        'views/views.xml',
    ],
}
