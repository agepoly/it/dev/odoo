# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'POS Polyright',
    'version': '1.0',
    'category': 'Sales/Point Of Sale',
    'sequence': 6,
    'summary': 'Integrate your POS with a Polyright payment terminal',
    'description': 'For the return state of the pos to work, you need to have tampermonkey and to add the script in static/pos_returner.js (use kiwi browser on android)',
    'data': [
        'views/point_of_sale_assets.xml',
    ],
    'qweb': [
        #'static/src/xml/BalanceButton.xml',
        'static/src/xml/Chrome.xml',
    ],
    'depends': ['point_of_sale'],
    'installable': True,
    'license': 'LGPL-3',
}
