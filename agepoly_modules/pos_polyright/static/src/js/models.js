odoo.define('pos_polyright.models', function (require) {

var models = require('point_of_sale.models');
var PaymentPolyright = require('pos_polyright.payment');

models.register_payment_method('polyright', PaymentPolyright);
});
