odoo.define('pos_polyright.payment', function (require) {
"use strict";

const { Gui } = require('point_of_sale.Gui');
var core = require('web.core');
var PaymentInterface = require('point_of_sale.PaymentInterface');

var _t = core._t;

var PaymentPolyright = PaymentInterface.extend({

    //--------------------------------------------------------------------------
    // Public
    //--------------------------------------------------------------------------

    /**
     * @override
     */
    init: function () {
        this._super.apply(this, arguments);
        //this.enable_reversals();
        if ('launchQueue' in window) {
            console.log("launch queue enabled")
            window.launchQueue.setConsumer(launchParams => {
                console.log(launchParam)
                this._onTransactionComplete(launchParams);
            });
        }

        if ('serviceWorker' in navigator) {
            navigator.serviceWorker.addEventListener('message', function(event) {
                this._onTransactionComplete(event);
            });
        }

        const channel = new BroadcastChannel('polyright_pos_return_channel');
        channel.onmessage = (e) => {
            console.log(e)
            var urlParams = new URLSearchParams(decodeURI(e.data.url));
            this._onTransactionComplete(e, urlParams);
        }

    },

    /**
     * @override
     */
    send_payment_request: function (cid) {
        this._super.apply(this, arguments);
        this.pos.get_order().selected_paymentline.set_payment_status('waitingCard');
        return this._sendTransaction(cid);
    },

    /**
     * @override
     */
    send_payment_reversal: function (cid) {
        this._super.apply(this, arguments);
        this.pos.get_order().selected_paymentline.set_payment_status('reversing');
        return this._sendTransaction();
    },

    /**
     * @override
     */
    send_payment_cancel: function (order, cid) {
        this._super.apply(this, arguments);
        this.transactionResolve();
        return true;
    },
    //--------------------------------------------------------------------------
    // Private
    //--------------------------------------------------------------------------

    _onTransactionComplete: function (event, data) {
        if (data.has('error')) {
            this.transactionResolve();
        } else {
            this.transactionResolve(true);
        }
    },

    _sendTransaction: function (cid) {
        var amount = this.pos.get_order().selected_paymentline.amount;

        return new Promise((resolve) => {
            this.transactionResolve = resolve;
            window.open("pay://transaction?amount="+amount+"&externalId="+cid+"&paymentMode=epurse&successCallback="+window.origin+"/pos_polyright/static/src/return.html&errorCallback="+window.origin+"/pos_polyright/static/src/return.html?error&cancelCallback="+window.origin+"/pos_polyright/static/src/return.html?error"); //&externalId=3333
        })
    },
});

return PaymentPolyright;

});
