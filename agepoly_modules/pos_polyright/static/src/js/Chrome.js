odoo.define('pos_polyright.chrome', function (require) {
    'use strict';

    const Chrome = require('point_of_sale.Chrome');
    const Registries = require('point_of_sale.Registries');

    const PosPolyrightChrome = (Chrome) =>
        class extends Chrome {
            get balanceButtonIsShown() {
                return this.env.pos.payment_methods.some(pm => pm.use_payment_terminal === 'polyright'); 
            }
        };

    Registries.Component.extend(Chrome, PosPolyrightChrome);

    return Chrome;
});
