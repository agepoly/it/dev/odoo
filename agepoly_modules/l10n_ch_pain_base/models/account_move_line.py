# copyright 2016 Akretion (Alexis de Lattre <alexis.delattre@akretion.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, _
from stdnum import iso11649

from odoo.exceptions import UserError


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    def _prepare_payment_line_vals(self, payment_order):
        vals = super()._prepare_payment_line_vals(payment_order)

        # this line is used to have the payment_reference instead of the ref in payments
        vals["communication"] = self.name or self.move_id.payment_reference or self.move_id.ref or self.move_id.name

        if not self.partner_bank_id:
            raise UserError(
                    _(
                        "For pain.001.001.03.ch.02, a recipient bank account must be set, "
                        "move :  '%s'"
                    )
                    % (self.move_id.name)
            )


        elif self.partner_bank_id._is_qr_iban() and self.move_id._has_isr_ref(): # isr ref and qr ref are the same
            vals["communication_type"] = "qrr"
            vals["communication"] = vals["communication"].replace(" ", "")

        elif self.partner_bank_id._is_qr_iban():
            raise UserError(
                    _(
                        "For pain.001.001.03.ch.02, for QR-IBAN payments, "
                        "a QR-Reference is required on the move :  '%s'"
                    )
                    % (self.move_id.name)
            )

        elif self._is_creditor_reference(vals["communication"]):
            vals["communication_type"] = "scor"
            if vals["communication"]:
                vals["communication"] = self._validate_creditor_reference(vals["communication"].replace(" ", ""))

        return vals

    @staticmethod
    def _is_creditor_reference(ref):
    	return iso11649.is_valid(ref)

    @staticmethod
    def _validate_creditor_reference(ref):
    	return iso11649.validate(ref)
