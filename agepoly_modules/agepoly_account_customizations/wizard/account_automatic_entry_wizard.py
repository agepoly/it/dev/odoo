# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.tools.misc import format_date, formatLang

from collections import defaultdict
from itertools import groupby
import json

# we inherit this because we want the analytic accounts and OU to be kept
class AutomaticEntryWizard(models.TransientModel):
    _inherit = 'account.automatic.entry.wizard'

    def _get_move_dict_vals_change_period(self):
        # set the change_period account on the selected journal items
        accrual_account = self.revenue_accrual_account if self.account_type == 'income' else self.expense_accrual_account

        move_data = {'new_date': {
            'currency_id': self.journal_id.currency_id.id or self.journal_id.company_id.currency_id.id,
            'move_type': 'entry',
            'line_ids': [],
            'ref': _('Adjusting Entry'),
            'date': fields.Date.to_string(self.date),
            'journal_id': self.journal_id.id,
        }}
        # complete the account.move data
        for date, grouped_lines in groupby(self.move_line_ids, lambda m: m.move_id.date):
            grouped_lines = list(grouped_lines)
            amount = sum(l.balance for l in grouped_lines)
            move_data[date] = {
                'currency_id': self.journal_id.currency_id.id or self.journal_id.company_id.currency_id.id,
                'move_type': 'entry',
                'line_ids': [],
                'ref': self._format_strings(_('Adjusting Entry of {date} ({percent:.2f}% recognized on {new_date})'), grouped_lines[0].move_id, amount),
                'date': fields.Date.to_string(date),
                'journal_id': self.journal_id.id,
            }

        # compute the account.move.lines and the total amount per move
        for aml in self.move_line_ids:
            # account.move.line data
            reported_debit = aml.company_id.currency_id.round((self.percentage / 100) * aml.debit)
            reported_credit = aml.company_id.currency_id.round((self.percentage / 100) * aml.credit)
            reported_amount_currency = aml.currency_id.round((self.percentage / 100) * aml.amount_currency)

            move_data['new_date']['line_ids'] += [
                (0, 0, {
                    'name': aml.name or '',
                    'debit': reported_debit,
                    'credit': reported_credit,
                    'amount_currency': reported_amount_currency,
                    'currency_id': aml.currency_id.id,
                    'account_id': aml.account_id.id,
                    'partner_id': aml.partner_id.id,
                    'operating_unit_id': aml.operating_unit_id.id,
                    'analytic_account_id': aml.analytic_account_id.id
                }),
                (0, 0, {
                    'name': _('Adjusting Entry'),
                    'debit': reported_credit,
                    'credit': reported_debit,
                    'amount_currency': -reported_amount_currency,
                    'currency_id': aml.currency_id.id,
                    'account_id': accrual_account.id,
                    'partner_id': aml.partner_id.id,
                    'operating_unit_id': aml.operating_unit_id.id,
                    'analytic_account_id': aml.analytic_account_id.id
                }),
            ]
            move_data[aml.move_id.date]['line_ids'] += [
                (0, 0, {
                    'name': aml.name or '',
                    'debit': reported_credit,
                    'credit': reported_debit,
                    'amount_currency': -reported_amount_currency,
                    'currency_id': aml.currency_id.id,
                    'account_id': aml.account_id.id,
                    'partner_id': aml.partner_id.id,
                    'operating_unit_id': aml.operating_unit_id.id,
                    'analytic_account_id': aml.analytic_account_id.id
                }),
                (0, 0, {
                    'name': _('Adjusting Entry'),
                    'debit': reported_debit,
                    'credit': reported_credit,
                    'amount_currency': reported_amount_currency,
                    'currency_id': aml.currency_id.id,
                    'account_id': accrual_account.id,
                    'partner_id': aml.partner_id.id,
                    'operating_unit_id': aml.operating_unit_id.id,
                    'analytic_account_id': aml.analytic_account_id.id
                }),
            ]

        move_vals = [m for m in move_data.values()]
        return move_vals
