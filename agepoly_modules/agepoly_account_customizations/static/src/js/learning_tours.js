odoo.define("agepoly_account_customizations.ndf_learning_tour", function (require) 
{
     "use strict";    // Minimal requirements needed to create a tour
     var core = require('web.core');
     var tour = require('web_tour.tour');
     // Allows you to translate tour steps
     var _t = core._t;
     tour.register('ndf_learning_tour', 
        {
            url: "/web",    
        }, 
        [{
            trigger: '.o_main_navbar a', 
            content: _t('Lets open the menu.'),        
            position: 'bottom',    
        },{
            trigger: '.o_app[data-menu-xmlid="agepoly_account_customizations.agepoly_menu_finance_easy"]', 
            content: _t('Open the accounting app. Click Here'),        
            position: 'right',    
        },{
            trigger: '.o_list_button_add', 
            content: _t('Lets Create a NDF'),        
            position: 'bottom',    
        }, {
            trigger: '.o_input[name="ref"]', 
            content: _t('Give a meaningful short title !'),        
            position: 'bottom',  
        }, {
            trigger: '.o_section_and_note_list_view .o_field_x2many_list_row_add a', 
            content: _t('Create a first line, remember we need a line for each accounting account and VAT rate.'),        
            position: 'bottom',  
        }, {
            trigger: '.o_field_widget[name="account_id"] input', 
            content: _t('Select the correct accounting account. Use 4XXX or 6XXX !'),        
            position: 'bottom',  
        }]);
}); 
