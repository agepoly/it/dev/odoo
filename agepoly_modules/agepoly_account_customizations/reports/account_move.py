from odoo import api, models
import pprint
import base64
import io
import PyPDF2

class AGEPolyAccountMoveReports(models.AbstractModel):
    _name = 'report.agepoly_account_customizations.report_acc_move_archive'

    def _get_report_values(self, docids, data=None):
        # get the records selected for this rendering of the report
        docs = self.env["account.move"].browse(docids)

        return {
            'doc_ids': docids,
            'doc_model': self.env["account.move"],
            'docs': docs,
            'data': data,
        }
