# -*- coding: utf-8 -*-

from odoo import api, models, fields, _
from odoo.exceptions import UserError

from datetime import date
import re

class AccountBankStatement(models.Model):
    _inherit = 'account.bank.statement'

    # one should create an automated action that trigger this function ON GOOD RECORDS "narration", "like", "'RvslInd': 'true'"" AT THE RIGHT MOMENT (draft -> post, or just before reconcile)
    # maybe we should hook into the reconcile action
    def button_post(self):
        super(AccountBankStatement, self).button_post()
        for statement in self:
            for line in statement.line_ids:
                line._auto_match_payment_returns()

class AccountBankStatementLine(models.Model):
    _inherit = 'account.bank.statement.line'

    # one should create an automated action that trigger this function ON GOOD RECORDS "narration", "like", "'RvslInd': 'true'"" AT THE RIGHT MOMENT (draft -> post, or just before reconcile)
    # maybe we should hook into the reconcile action
    def _auto_match_payment_returns(self):

        if not self.narration:
            return

        if self.narration and self.narration.find("(RvslInd): true") == -1:
            return

        _, suspense_aml, _ = self._seek_for_lines()

        suspense_aml.write({'account_id': self.env.ref('agepoly_account_customizations.payment_return_account').id})

        e2eId = re.search("\(Refs\/EndToEndId\): (.*)", self.narration)
        if not e2eId or not e2eId.group(1):
            return

        bsl = self.env['account.bank.statement.line'].search([["narration", "like", "(Refs/EndToEndId): %s" % e2eId.group(1)], ['id', '!=', suspense_aml.id], ['amount', '=', -1 * self.amount], ['state', '=', 'posted']])
        if len(bsl) != 1: # we want to be sure of our match
            return

        # TODO : we may also want to reconcile with the invoice and let an open balance (so that the payment try are visible on the invoice)
        bsl[0].reconcile([{'id': suspense_aml.id}])
