from odoo import  models, fields, api

class MisBudgetByAccountItem(models.Model):
    _inherit = "mis.budget.by.account.item"

    # we add those pseudo fields for groupbys in mis budget item view
    analytic_account_group_id = fields.Many2one('account.analytic.group', "Analytic Group", store=True, related="analytic_account_id.group_id")
    analytic_account_parent_group_id = fields.Many2one('account.analytic.group', "Analytic Group (Parent)", store=True, related="analytic_account_id.group_id.parent_id")

    # we add a inverse balance for meaningful graph views and more understandable for non-accountants
    inverse_balance = fields.Monetary(string='Balance (I)', store=True,
        currency_field='company_currency_id',
        compute='_get_inverse_balances',
        inverse="_inverse_inverse_balance",
        help="Technical field holdind inverse of the debit - credit in order to open meaningful graph views from reports")

    @api.depends('balance')
    def _get_inverse_balances(self):
        for rec in self:
            rec.inverse_balance = rec.balance * -1

    #We give the inverse_balance at the balance and the mother class compute debit and credit
    def _inverse_inverse_balance(self):
        for rec in self:
            rec.balance = rec.inverse_balance * -1
            

