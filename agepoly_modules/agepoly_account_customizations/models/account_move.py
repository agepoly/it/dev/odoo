# -*- coding: utf-8 -*-

from odoo import api, models, fields, _
from odoo.exceptions import UserError

from datetime import date

import pprint

class AccountMove(models.Model):
    _inherit = 'account.move'

    # TODO : handle title differently
    # temporary release the constraint on duplicated supplier reference
    def _check_duplicate_supplier_reference(self):
        return True

    # override this method because messing up the "default_journal_id" given in context
    @api.onchange("operating_unit_id")
    def _onchange_operating_unit(self):
        for line in self.line_ids:
            line.operating_unit_id = self.operating_unit_id
            line._compute_analytic_account_id()
        
        for line in self.invoice_line_ids:
            line.operating_unit_id = self.operating_unit_id
            line._compute_analytic_account_id()

        return {'domain': {'money_requests_ids': [('operating_unit_id', '=', self.operating_unit_id.id), ('state', '=', 'given')]}}

    def _get_has_agepoly_advanced_account_user_group(self):
        return self.env.user.has_group('agepoly_advanced_account_user')

#    has_agepoly_advanced_account_user_group = fields.Boolean(compute=_get_has_agepoly_advanced_account_user_group, store=False)

#    def _get_all_attachements(self):
#        self.all_attachements_ids = self.env['ir.attachment'].search([('res_model','=','account.move'),('res_id','=', self.id )])
#    all_attachements_ids = fields.Many2many('ir.attachment', 'all attachments', store=False, compute=_get_all_attachements)

    def _get_default_partner(self):
        if self.env.context.get('is_agepoly_ndf', False):
            return self.env.user.partner_id
        else:
            return None

    partner_id = fields.Many2one(default=_get_default_partner)

    money_requests_ids = fields.Many2many(
        comodel_name="account.money_request", 
        string="Linked Money Request",
        relation="money_request_account_moves_rel",
        column1="account_move_id",
        column2="money_request_id",
    )

    @api.depends('payment_mode_id', 'journal_id')
    def _compute_show_linked_request_in_form(self):
        for move in self:
            if move.payment_mode_id == self.env.ref('agepoly_account_customizations.agepoly_payment_mode_cash_out') or move.payment_mode_id == self.env.ref('agepoly_account_customizations.agepoly_payment_mode_cash_in') or self.env.ref('agepoly_account_customizations.unit_cash_journal_group') in move.journal_id.journal_group_ids:
                move.show_linked_request_in_form = True
            else:
                move.show_linked_request_in_form = False

    show_linked_request_in_form = fields.Boolean(compute=_compute_show_linked_request_in_form, store=False)

    agepoly_status = fields.Selection(
        selection=[
            ('to_correct', 'To Correct'),
            ('draft', 'Draft'),
            ('waiting_unit_validation', 'Waiting Unit Validation'),
            ('waiting_agepoly_validation', 'Waiting Agepoly Validation'),
            ('waiting_cdd_validation', 'Waiting CDD Validation'),
            ('cancelled', 'Cancelled'),
            ('validated', 'Validated'),
            ('other', 'Other')
        ],
        string="Agepoly Status",
        readonly=True,
        compute="_compute_agepoly_status",
        store=True
    )

    @api.depends("review_ids", "review_ids.status", "state", "rejected")
    def _compute_agepoly_status(self):
        for move in self:
            #We are obliged to acces the reviews like that otherwhise the data is not updated
            reviews = self.env["tier.review"].search([["model", "=", "account.move"], ["res_id", "=", move.id]]) 
            nb_wait_for_validation = len(reviews.filtered(lambda r: r.status == "pending"))
            if move.state == "draft" and move.rejected:
                move.agepoly_status = "to_correct"
            elif move.state == "draft" and len(reviews) == 0:
                move.agepoly_status = "draft"
            elif move.state == "draft" and nb_wait_for_validation > 3:
                move.agepoly_status = "waiting_unit_validation"
            elif move.state == "draft" and nb_wait_for_validation > 2:
                move.agepoly_status = "waiting_agepoly_validation"
            elif move.state == "draft" and nb_wait_for_validation <= 2:
                move.agepoly_status = "waiting_cdd_validation"
            elif move.state == "cancel":
                move.agepoly_status = "cancelled"
            elif nb_wait_for_validation == 0:
                move.agepoly_status = "validated"
            else:
                move.agepoly_status = "other"
        
    def _agepoly_create_invoice_payment(self):
        for inv in self:
            if not inv.operating_unit_id:
                raise UserError(_("Operating Unit must be set to generate payment automaticaly"))
            if not inv.operating_unit_id.cash_journal_id:
                raise UserError(_("Operating Unit must have a Cash Journal Set"))
            
            available_lines = self.env['account.move.line']
            for line in self.line_ids:
                if line.account_internal_type not in ('receivable', 'payable'):
                    continue
                if line.currency_id:
                    if line.currency_id.is_zero(line.amount_residual_currency):
                        continue
                else:
                    if line.company_currency_id.is_zero(line.amount_residual):
                        continue
                available_lines |= line

            if not available_lines:
                raise UserError(_("You can't register a payment because there is nothing left to pay on the selected journal items."))
            if len(available_lines.company_id) > 1:
                raise UserError(_("You can't create payments for entries belonging to different companies."))
            if len(set(available_lines.mapped('account_internal_type'))) > 1:
                raise UserError(_("You can't register payments for journal items being either all inbound, either all outbound."))

            st_values = {
                'journal_id': self.operating_unit_id.cash_journal_id.id,
                'user_id': self.env.user.id,
                'name': "CASH PAY - {}".format(self.name),
                'line_ids': [(0, 0, {
                    'date': self.date,
                    'amount': sum(available_lines.mapped('amount_residual')),
                    'payment_ref': "CASH PAY - {}".format(self.name),
                    'journal_id': self.operating_unit_id.cash_journal_id.id,
                    #'counterpart_account_id': available_lines[0].account_id.id
                })],
            }

            ctx = dict(self.env.context)
            ctx['journal_id'] = self.operating_unit_id.cash_journal_id.id
            statement_id = self.env['account.bank.statement'].with_context(ctx).create(st_values)

            statement_id.mapped('line_ids.move_id').write({ 
                'ref':  "CASH PAY - {}".format(self.name),
            })  

            statement_id.write({'balance_end_real': statement_id.balance_end})
            statement_id.button_post()

            reconcile_lines = []
            for line in available_lines:
                reconcile_lines.append({
                                     'id': line.id,
                                   })
            statement_id.line_ids[0].reconcile(reconcile_lines)
            statement_id.button_validate()
        return True

    def _check_allow_write_under_validation(self, vals):
        # add some field to be editable during the whole process 
        return super(AccountMove, self)._check_allow_write_under_validation(vals)
       
    @api.model
    def _get_under_validation_exceptions(self):
        """Extend for more field exceptions."""
        return super(AccountMove, self)._get_under_validation_exceptions() + ["invoice_date", "agepoly_status"]

    def sudo_validate_tier(self):
        self.ensure_one()
        if not self.env.user.has_group("agepoly_account_customizations.agepoly_group_sudo_validation"):
            raise UserError(_("Not allowed to sudo validate"))

        all_reviews = self.review_ids.filtered(lambda r: r.status == "pending")

        # Include all reviews with approve_sequence = False
        sequences = all_reviews.filtered(lambda r: not r.approve_sequence).mapped(
            "sequence"
        )
        # Include only my_reviews with approve_sequence = True
        approve_sequences = all_reviews.filtered("approve_sequence").mapped("sequence")
        if approve_sequences:
            min_sequence = min(all_reviews.mapped("sequence"))
            sequences.append(min_sequence)

        reviews = self.review_ids.filtered(lambda l: l.sequence in sequences)

        if self.has_comment:
            return self._add_comment("validate", reviews)

        reviews.write(
            {
                "status": "approved",
                "done_by": self.env.user.id,
                "reviewed_date": fields.Datetime.now(),
            }
        )

        self._update_counter()

        self._tier_validation_hook()

        for review in reviews:
            rec = self.env[review.model].browse(review.res_id)

            post = "message_post"
            if hasattr(rec, post):
            # Notify state change
                getattr(rec, post)(
                    subtype_xmlid="sudo_post_used",
                    body=_("A sudo validation has been done by {}.".format(self.env.user.name))
                )

    # force comment on rejected review
    def reject_tier(self):
        self.ensure_one()
        sequences = self._get_sequences_to_approve(self.env.user)
        reviews = self.review_ids.filtered(lambda l: l.sequence in sequences)
        return self._add_comment("reject", reviews)

    # automatically confirm when the last review is done
    # hook to know when a review is accepted
    # if it's the last, we switch to the next state
    def _validate_tier(self, tiers=False):
        super(AccountMove, self)._validate_tier(tiers)
        self._tier_validation_hook()

    def _tier_validation_hook(self):
        # recompute reviews if they depend on previous one (validation cdd)
        for review in self.review_ids:
            if hasattr(review, '_compute_python_reviewer_ids'):
                review._compute_python_reviewer_ids()
            review._compute_reviewer_ids()

        if self.state == 'draft' and self.validated:
            self.action_post()

    # automatically add payment for cash receipts (in/out)
    def _post(self, soft=True):
        for move in self:
            # set invoice date same as move date if invoice date is not present
            if move.is_invoice(include_receipts=True) and not move.invoice_date:
                move.write({'invoice_date': move.date})

        posted_moves = super(AccountMove, self)._post(soft)
        posted_moves.filtered(
            lambda m: m.move_type == 'in_invoice' and m.payment_mode_id == self.env.ref('agepoly_account_customizations.agepoly_payment_mode_cash_out') and not m.currency_id.is_zero(m.amount_total)
        )._agepoly_create_invoice_payment()

        posted_moves.filtered(
            lambda m: m.move_type == 'out_invoice' and m.payment_mode_id == self.env.ref('agepoly_account_customizations.agepoly_payment_mode_cash_in') and not m.currency_id.is_zero(m.amount_total)
        )._agepoly_create_invoice_payment()

        return posted_moves

    def button_draft(self):
        for move in self:
            # forbid back to draft for non accountants
            if move.state == "posted" and not self.env.user.has_group("account.group_account_manager"):
                raise UserError(_("Not allowed to back to draft"))

            # auto cancel payment on cash invoice back to draft
            if (move.journal_id.type == 'purchase' or move.journal_id.type == 'sale') and (move.payment_mode_id == self.env.ref('agepoly_account_customizations.agepoly_payment_mode_cash_in') or move.payment_mode_id == self.env.ref('agepoly_account_customizations.agepoly_payment_mode_cash_out')):
                statements = move.line_ids.filtered(lambda line: line.account_id.user_type_id.type in ('receivable', 'payable')).mapped('matched_debit_ids.debit_move_id.move_id.line_ids.statement_id')
                statements |= move.line_ids.filtered(lambda line: line.account_id.user_type_id.type in ('receivable', 'payable')).mapped('matched_credit_ids.credit_move_id.move_id.line_ids.statement_id')

                try:
                    statements.button_reprocess()
                except:
                    pass

                statements.button_reopen()

                statements.unlink()
               
        super(AccountMove, self).button_draft()

    def _get_invoice_in_payment_state(self):
        #the state of an invoice is in payement when a payement order is sent to the bank
        return 'in_payment'

    @api.depends("partner_id", "company_id")
    def _compute_payment_mode(self):
        super(AccountMove, self)._compute_payment_mode()
        # if the payment mode is not set by another way and the context request a default payment_mode_id, we apply it      
        for move in self:
            if not move.payment_mode_id:
                move.payment_mode_id = self.env.context.get('default_payment_mode_id', False)

    @api.depends("partner_id", "payment_mode_id")
    def _compute_partner_bank(self):
        super(AccountMove, self)._compute_partner_bank()
        for move in self:
           # if the partner bank is not set by another way and the context request a default partner_bank_id, we apply it      
           if not move.partner_bank_id:
                move.partner_bank_id = self.env.context.get('default_partner_bank_id', False)


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'
    has_agepoly_advanced_account_user_group = fields.Boolean(compute="_get_has_agepoly_advanced_account_user_group", store=False)

    inverse_balance = fields.Monetary(string='Balance (I)', store=True,
        currency_field='company_currency_id',
        compute='_get_inverse_balances',
        help="Technical field holdind inverse of the debit - credit in order to open meaningful graph views from reports")
    inverse_cumulated_balance = fields.Monetary(string='Cumulated Balance (I)', store=False,
        currency_field='company_currency_id',
        compute='_get_inverse_balances',
        help="Cumulated inverse of balance depending on the domain and the order chosen in the view.")

    # ensure the analytic account is in the OU
    analytic_account_id = fields.Many2one(domain="[('operating_unit_ids', '=', operating_unit_id)]")
 
    # we add those pseudo fields for groupbys in ma compta
    analytic_account_group_id = fields.Many2one('account.analytic.group', "Groupe de Compte Analytique", store=True, related="analytic_account_id.group_id")
    analytic_account_parent_group_id = fields.Many2one('account.analytic.group', "Groupe de Compte Analytique Parent", store=True, related="analytic_account_id.group_id.parent_id")

    # when no ou is set but an analytic account is, set the ou from the analytic account (occur for some automatic moves)
    def _maybe_set_OU_from_analytic_account(self):
        for record in self:
            if not record.operating_unit_id and record.analytic_account_id and len(record.analytic_account_id.operating_unit_ids) >= 1:
                record.operating_unit_id = record.analytic_account_id.operating_unit_ids[0]
    def create(self, vals):
        res = super(AccountMoveLine, self).create(vals)
        if 'analytic_account_id' in vals:
            res._maybe_set_OU_from_analytic_account()
        return res

    def write(self, vals):
        res = super(AccountMoveLine, self).write(vals)
        if 'analytic_account_id' in vals:
            # Refresh analytic lines on analytic acccount change (to allow analytic account changes after post)
            for record in self:
                record.analytic_line_ids.unlink()
                record.create_analytic_lines()
            self._maybe_set_OU_from_analytic_account()
        return res

    @api.depends('balance', 'cumulated_balance')
    def _get_inverse_balances(self):
        for line in self:
            line.inverse_balance = line.balance * -1
            line.inverse_cumulated_balance = line.cumulated_balance * -1

    def _get_has_agepoly_advanced_account_user_group(self):
        return self.env.user.has_group('agepoly_advanced_account_user')

    @api.onchange('product_id', 'account_id', 'partner_id', 'date', 'operating_unit_id')
    def _compute_analytic_account_id(self):
        #super(AccountMoveLine, self)._compute_analytic_account_id() 
        for record in self:
            # set the default analytic_account on move line depending on OU and OU default analytic if no analytic account has been selected previously
            if not record.operating_unit_id:
                break

            if record.operating_unit_id in record.analytic_account_id.operating_unit_ids: 
                break

            if record.exclude_from_invoice_tab and not self.env.context.get('force_analytic_account_from_operating_unit', False):
                break

            if record.operating_unit_id.default_analytic_account_id != False:
                record.analytic_account_id = record.operating_unit_id.default_analytic_account_id

    def action_edit_popup(self):
        self.ensure_one()
        view = self.env.ref('agepoly_account_customizations.view_account_move_agepoly_ma_compta_form')
        return {
            'name': _('%s') % self.name,
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_model': 'form',
            'res_model': 'account.move.line',
            'views': [(view.id, 'form')],
            'view_id': view.id,
            'target': 'new',
            'res_id': self.id,
            'context': dict(self.env.context),
        }


class TierReview(models.Model):
    _inherit = ["tier.review"]
    
    def write(self, vals):
        res = super(TierReview, self).write(vals)
        for review in self:
            if review.model == "account.move":
                move = self.env[review.model].browse(review.res_id)
                move._compute_agepoly_status()
        return res

    def create(self, vals):
        records = super(TierReview, self).create(vals)
        self.flush()
        for review in records:
            if review.model == "account.move":
                print("Create has been triggered")
                move = self.env[review.model].browse(review.res_id)
                move._compute_agepoly_status()

        return records
