# -*- coding: utf-8 -*-

from . import models
from . import reports
from . import wizard

from odoo import api, SUPERUSER_ID


def accounting_prepare_pre_init(cr):
    env = api.Environment(cr, SUPERUSER_ID, {})
    env['account.journal'].search([('type','!=', 'general')]).unlink()
    env["account.account"].search([('code', 'not in', ["1100", "2000", "3200", "4200"])]).unlink()
