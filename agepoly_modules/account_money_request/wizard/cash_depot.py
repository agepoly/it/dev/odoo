# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError

from datetime import date

class AccoutCashDepot(models.TransientModel):
    _name = 'account.cash_depot'
    _description = 'Depôt Cash'

    # == Business fields ==

    amount = fields.Monetary(currency_field='currency_id', store=True, readonly=False)
    currency_id = fields.Many2one('res.currency', string='Currency', store=True, readonly=False,
        compute='_get_default_currency',
        help="Currency.")

    @api.model
    def _get_default_currency(self):
        ''' Get the default currency from the default company. '''
        return self.env.company.currency_id

    depot_type = fields.Selection(
        selection=[
            ('to_bank', 'Banque'),
            ('to_available_money', 'Retour dans les retrait cash en attente')
        ],
        string="Type de depot",
        default="to_bank",
    )

    operating_unit_id = fields.Many2one(
        comodel_name="operating.unit",
        domain="[('user_ids', '=', uid)]",
        required=True,
    )

    money_requests_ids = fields.Many2many(
        comodel_name="account.money_request", 
        string="Linked Money Request",
        relation="money_request_cash_depot_rel",
        column1="account_move_id",
        column2="money_request_id",
        domain="[('state', '=', 'given')]"
    )

    @api.onchange('operating_unit_id')
    def _update_money_request_domain(self):
        return {'domain': {'money_requests_ids': [('operating_unit_id', '=', self.operating_unit_id.id), ('state', '=', 'given')]}}


    attachments_ids = fields.Many2many('ir.attachment', string="Ticket de l'automate", required=True)
    # -------------------------------------------------------------------------
    # HELPERS
    # -------------------------------------------------------------------------
    def action_create_move(self):
        if self.money_requests_ids:
            money_request = [req.id for req in self.money_requests_ids]
        else:
            money_request = []
                      
        if self.depot_type == "to_bank": # will be reconciled later with the bank line
            ct_account = self.env.ref('agepoly_account_customizations.liquidity_transfer').id,
        else:
            ct_account = self.env.ref('account_money_request.cash_draw_trans_account').id,

        st_values = {
            'journal_id': self.operating_unit_id.cash_journal_id.id,
            'user_id': self.env.user.id,
            'name': "DEPOT CASH - {}".format(date.today()),
            'line_ids': [(0, 0, {
                'date': date.today(),
                'amount': - self.amount,
                'payment_ref': "DEPOT CASH - {}".format(date.today()),
                'journal_id': self.operating_unit_id.cash_journal_id.id,
                'counterpart_account_id': ct_account,
            })],

        }

        ctx = dict(self.env.context)
        ctx['journal_id'] = self.operating_unit_id.cash_journal_id.id
        statement_id = self.env['account.bank.statement'].sudo().with_context(ctx).create(st_values)
        
        statement_id.mapped('line_ids.move_id').write({ 
            'ref': "DEPOT CASH - {}".format(date.today()),
            'operating_unit_id': self.operating_unit_id.id, 
            'money_requests_ids': [(6, 0, money_request)]
        })     

        statement_id.sudo().write({'balance_end_real': statement_id.balance_end})
        statement_id.sudo().button_post()

        statement_id.sudo().button_validate()

        # transfer attachments
        self.attachments_ids.write({   
            'res_model': 'account.move',
            'res_id': statement_id.mapped('line_ids.move_id.id')[0]
        })

        return {'return':True, 'type':'ir.actions.act_window_close'}