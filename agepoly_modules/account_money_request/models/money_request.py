from odoo import api, fields, models, _
from odoo.exceptions import RedirectWarning, UserError, ValidationError, AccessError

from datetime import date

class MoneyRequest(models.Model):
    _name = "account.money_request"
    _inherit = ['mail.thread', 'mail.activity.mixin', 'tier.validation']
    _state_from = ['draft']
    _state_to = ['in_prepare', 'ready', 'given']
    _state_cancel = 'cancel'
    _description = "Money Request"
    _order = "start_date desc,state,name"


    name = fields.Char(required=True, index=True)
    explanation = fields.Text()

    amount = fields.Monetary(string="The amount requested", currency_field="currency_id", tracking=True, index=True)

    @api.model
    def _get_default_currency(self):
        ''' Get the default currency from the default company. '''
        return self.env.company.currency_id

    currency_id = fields.Many2one(
        comodel_name="res.currency",
        store=True,
        readonly=True,
        required=True,
        states={'draft': [('readonly', False)]},
        string='Currency',
        default=_get_default_currency
    )

    responsible_user_id = fields.Many2one(
        comodel_name="res.users",
        default=lambda self: self.env.user,
        required=True,
        index=True
    )

    operating_unit_id = fields.Many2one(
        comodel_name="operating.unit",
        domain="[('user_ids', '=', uid)]",
        required=True,
        readonly=True,
        states={'draft': [('readonly', False)]},
    )

    state = fields.Selection(
        selection=[
            ('draft', 'Draft'),
            ('in_prepare', 'In Preparation'),
            ('ready', 'Ready'),
            ('given', 'Given'),
            ('cancel', 'Annulé'),
            ('archived', 'Archivé')
        ],
        string="Status",
        readonly=True,
        copy=False,
        default="draft",
        tracking=True
    )

    method = fields.Selection(
        tracking=True,
        required=True,
        selection=[
            ('cash', 'Cash'),
            ('card', 'Card'),
        ])

    start_date = fields.Date(required=True, tracking=True)
    end_date = fields.Date()

    related_moves_ids = fields.Many2many(
        comodel_name="account.move",
        string="Linked Moves",
        relation="money_request_account_moves_rel",
        column1="money_request_id",
        column2="account_move_id"
    )

    def _recursive_get_all_related_move_lines(self, requests, exclusion_list, allow_draft=False):
        #self.ensure_one()
        move_lines = self.env['account.move.line'].sudo()  # create empty recordset
        for req in requests:
            if req.id not in exclusion_list:
                exclusion_list += [req.id]
                for move in req.related_moves_ids.sudo():
                    move_lines |= move.mapped('line_ids')
                    move_lines |= move.line_ids.filtered(lambda line: line.account_id.user_type_id.type in ('receivable', 'payable')).mapped('matched_debit_ids.debit_move_id.move_id.line_ids')
                    move_lines |= move.line_ids.filtered(lambda line: line.account_id.user_type_id.type in ('receivable', 'payable')).mapped('matched_credit_ids.credit_move_id.move_id.line_ids')
                    move_lines |= self._recursive_get_all_related_move_lines(move.money_requests_ids, exclusion_list)
        return move_lines.filtered(lambda line: line.move_id.state == "posted" or (allow_draft and line.move_id.state == "draft"))

    def _get_all_related_move_lines(self):
        #self.all_related_move_lines_ids = self.env['account.move'].browse(list(set(MoneyRequest._recursive_get_all_related_move_lines([self], []))))
        for req in self.sudo():
            req.all_related_move_lines_ids = req._recursive_get_all_related_move_lines([req], [])


    # this field is used to retrieve all moves linked to any request linked to the moves of this request
    all_related_move_lines_ids = fields.Many2many(
        comodel_name="account.move.line",
        string="All Linked Move Lines",
        store=False,
        compute=_get_all_related_move_lines
    )

    def _get_all_related_move_lines_draft(self):
        #self.all_related_move_lines_ids = self.env['account.move'].browse(list(set(MoneyRequest._recursive_get_all_related_move_lines([self], []))))
        for req in self.sudo():
            req.all_related_move_lines_draft_ids = req._recursive_get_all_related_move_lines([req], [], True)

    all_related_move_lines_draft_ids = fields.Many2many(
        comodel_name="account.move.line",
        string="All Linked Move Lines (including drafts)",
        store=False,
        compute=_get_all_related_move_lines_draft
    )


    def _get_solde(self):
        for req in self.sudo():
            credit = 0
            debit = 0
            for line in req.all_related_move_lines_ids:
                if line.account_id == req._get_assets_account():
                    credit += line.credit
                    debit += line.debit
            req.solde = debit - credit

    solde = fields.Monetary(string="The amount still available", currency_field="currency_id", store=False, compute=_get_solde)

    def _get_draft_solde(self):
        for req in self:
            credit = 0
            debit = 0
            for line in req.all_related_move_lines_draft_ids.sudo():
                if line.account_id == req._get_assets_account():
                    credit += line.credit
                    debit += line.debit
                if line.move_id.state == 'draft' and line.account_id.user_type_id.type in ('receivable', 'payable'): # pre add the draft line directly (they do not have a matched payment line yet)
                    credit += line.credit
                    debit += line.debit
            req.draft_solde = debit - credit

    draft_solde = fields.Monetary(string="The amount still available (including not yet validated lines)", currency_field="currency_id", store=False, compute=_get_draft_solde)

    @api.model_create_multi
    def create(self, vals_list):
        if any('state' in vals and vals.get('state') != 'draft' for vals in vals_list):
            raise UserError(_('You cannot create a money request already not in the draft state. Please create a draft request and post it after.'))

        rslt = super(MoneyRequest, self).create(vals_list)
        return rslt

    def unlink(self):
        for move in self:
            if move.state != 'draft':
                raise UserError(_("You cannot delete a money request that is not in draft."))
        return super(MoneyRequest, self).unlink()

    def write(self, vals):
        for req in self:
            # switch to draft
            if req.state != 'draft' and 'state' in vals and vals.get('state') == 'draft':
                if not self.user_has_groups('account_money_request.group_money_request_manager') and req.state not in ['draft']:
                    raise UserError(_('Only Manager can reset to draft after Preparation started'))
                if req.state not in ['draft', 'in_prepare', 'ready']:
                    raise UserError(_('You can reset to draft only before money is given'))
                req.restart_validation()
                req._state_change_action('draft')

            if req.state != 'in_prepare' and 'state' in vals and vals.get('state') == 'in_prepare':
                if req.state != 'draft':
                    raise UserError(_('Do not Skip state'))
                req._state_change_action('in_prepare')

            if req.state != 'ready' and 'state' in vals and vals.get('state') == 'ready':
                if req.state != 'in_prepare':
                    raise UserError(_('Do not Skip state'))
                req._state_change_action('ready')

            if req.state != 'given' and 'state' in vals and vals.get('state') == 'given':
                if req.state == 'ready':
                    req._state_change_action('given')
                elif req.state == 'archived':
                    req._state_change_action('unarchive') # we triger another event for rollback to given
                else:
                    raise UserError(_('Do not Skip state'))

            if req.state != 'cancel' and 'state' in vals and vals.get('state') == 'cancel':
                if not self.user_has_groups('account_money_request.group_money_request_manager') and req.state not in ['draft']:
                    raise UserError(_('Only Manager can cancel after Preparation started, put a message instead'))

                if req.state not in ['draft', 'in_prepare', 'ready']:
                    raise UserError(_('You can cancel only before money is given'))

                req._state_change_action('cancel')

            if req.state != 'archived' and 'state' in vals and vals.get('state') == 'archived':
                if req.state not in ["given"]:
                    raise UserError(_('You can archive only given or canceled money request'))
                req._state_change_action('archive')

        return super(MoneyRequest, self).write(vals)

    def ready(self):
        self.write({'state': 'ready'})

    def given(self):
        self.write({'state': 'given'})

    def draft(self):
        self.write({'state': 'draft'})

    def cancel(self):
        self.write({'state': 'cancel'})

    def archive(self):
        self.write({'state': 'archived'})

    def unarchive(self):
        self.write({'state': 'given'})

    # force comment on rejected review
    def reject_tier(self):
        self.ensure_one()
        sequences = self._get_sequences_to_approve(self.env.user)
        reviews = self.review_ids.filtered(lambda l: l.sequence in sequences)
        return self._add_comment("reject", reviews)

    def request_and_valid_validation(self):
        for rec in self:
            rec.request_validation()

            # check if user has a validation to do and do it
            if rec.validated == True or rec.state not in ['draft'] or rec.rejected == True or rec.can_review == False:
                continue
            rec.validate_tier()

    # automatically confirm when the last review is done
    # hook to know when a review is accepted
    # if it's the last, we switch to the next state
    def _tier_validation_hook(self):
        # recompute reviews if they depend on previous one (validation cdd)
        for review in self.review_ids:
            if hasattr(review, '_compute_python_reviewer_ids'):
                review._compute_python_reviewer_ids()
            review._compute_reviewer_ids()

        if self.state == 'draft' and self.validated:
            self.state = 'in_prepare'

    def _validate_tier(self, tiers=False):
        super(MoneyRequest, self)._validate_tier(tiers)
        self._tier_validation_hook()


    def sudo_validate_tier(self):
        self.ensure_one()
        if not self.env.user.has_group("account_money_request.agepoly_group_sudo_validation_money_request"):
            raise UserError(_("Not allowed to sudo validate"))

        all_reviews = self.review_ids.filtered(lambda r: r.status == "pending")

        # Include all reviews with approve_sequence = False
        sequences = all_reviews.filtered(lambda r: not r.approve_sequence).mapped(
            "sequence"
        )
        # Include only my_reviews with approve_sequence = True
        approve_sequences = all_reviews.filtered("approve_sequence").mapped("sequence")
        if approve_sequences:
            min_sequence = min(all_reviews.mapped("sequence"))
            sequences.append(min_sequence)

        reviews = self.review_ids.filtered(lambda l: l.sequence in sequences)

        if self.has_comment:
            return self._add_comment("validate", reviews)

        reviews.write(
            {
                "status": "approved",
                "done_by": self.env.user.id,
                "reviewed_date": fields.Datetime.now(),
            }
        )

        self._update_counter()

        self._tier_validation_hook()

        for review in reviews:
            rec = self.env[review.model].browse(review.res_id)

            post = "message_post"
            if hasattr(rec, post):
            # Notify state change
                getattr(rec, post)(
                    subtype_xmlid="sudo_post_used",
                    body=_("A sudo validation has been done by {}.".format(self.env.user.name))
                )

    def _get_assets_account(self):
        self.ensure_one()
        func = getattr(self, '_get_assets_account_{}'.format(self.method), False)
        if func:
            return func()
        return False

    def _get_assets_account_cash(self):
        return self.operating_unit_id.cash_journal_id.default_account_id

    def _get_assets_account_card(self):
        return False  # TODO

    def _state_change_action(self, state):
        self.ensure_one()
        func = getattr(self, '_{}_{}'.format(state, self.method), False)
        if func:
            func()

## Buisness action on state change
    def _given_cash(self):
        self.ensure_one()
        st_values = {
            'journal_id': self.operating_unit_id.cash_journal_id.id,
            'user_id': self.env.user.id,
            'name': "DMD - {} - {}".format(self.method.capitalize(), self.name),
            'line_ids': [(0, 0, {
                'date': date.today(),
                'amount': self.amount,
                'payment_ref': "DMD - {} - {}".format(self.method.capitalize(), self.name),
                'journal_id': self.operating_unit_id.cash_journal_id.id,
                'counterpart_account_id': self.env.ref('account_money_request.cash_draw_trans_account').id,
                }
            )],

        }

        ctx = dict(self.env.context)
        ctx['journal_id'] = self.operating_unit_id.cash_journal_id.id
        statement_id = self.env['account.bank.statement'].with_context(ctx).create(st_values)

        statement_id.mapped('line_ids.move_id').write({
            'ref': "DMD - {} - {}".format(self.method.capitalize(), self.name),
            'operating_unit_id': self.operating_unit_id.id,
            'money_requests_ids': [(4, self.id)]
        })

        statement_id.write({'balance_end_real': statement_id.balance_end})
        statement_id.button_post()

        statement_id.button_validate()

    def action_show_linked_moves_real_only(self):
        action = self.env['ir.actions.act_window']._for_xml_id('account.action_account_moves_all_a')
        ids = self.all_related_move_lines_ids.filtered(lambda m: m.account_id == self._get_assets_account()).mapped('id')
        action['domain'] = [('id', 'in', ids)]
        return action

    def action_show_linked_moves_all(self):
        action = self.env['ir.actions.act_window']._for_xml_id('account.action_account_moves_all_a')
        ids = self.all_related_move_lines_ids.filtered(lambda m: m.account_id == self._get_assets_account()).mapped('id') # take the cash lines
        ids += self.all_related_move_lines_draft_ids.filtered(lambda m: m.move_id.state == 'draft' and m.account_id.user_type_id.type in ('receivable', 'payable')).mapped('id') # add the draft lines
        action['domain'] = [('id', 'in', ids)]
        action['context'] = {'journal_type':'general', 'search_default_group_by_move': 1, 'search_default_posted': 0, 'name_groupby': 1, 'create':0}
        return action

    def action_show_linked_moves_all_except_payment(self):
        action = self.env['ir.actions.act_window']._for_xml_id('account.action_account_moves_all_a')
        ids = self.all_related_move_lines_draft_ids.filtered(lambda m: (m.move_id.is_invoice(True) and m.account_id.user_type_id.type in ('receivable', 'payable')) or (m.account_id == self._get_assets_account() and not any([aml_acc_type in ['payable', 'receivable'] for aml_acc_type in m.move_id.line_ids.mapped('account_id.user_type_id.type')]))).mapped('id')
        action['domain'] = [('id', 'in', ids)]
        action['context'] = {'search_default_group_by_move': 1, 'name_groupby': 1, 'create': 0}
        return action