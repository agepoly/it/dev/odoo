# -*- coding: utf-8 -*-
{
    'name': 'Base module for money request',
    'version': '14.0.1.0.0',
    'license': 'AGPL-3',
    'summary': """dd""",
    'description': """dd""",
    'author': 'Téo Goddet, AGEPoly',
    'category': 'l10n',
    'depends': ['account', 'l10n_ch'],
    'data': [
        'data/data.xml',
        'security/account_money_request_security.xml',
        'views/money_request.xml',
        'wizard/cash_depot.xml'
    ],
    'installable': True,
    'auto_install': False,
}
