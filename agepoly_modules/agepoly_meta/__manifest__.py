# Copyright 2014-2018 'AGEPoly'
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl)

{
    "name": "AGEPoly Meta Package",
    "summary": "Install All AGEPoly required modules",
    "version": "14.0.1.0.0",
    "category": "Tools",
    "development_status": "Production/Stable",
    "author": "Téo Goddet, AGEPoly",
    "maintainer": "AGEPoly",
    "website": "agepoly.ch",
    "depends": ["board", "agepoly_account_customizations", "agepoly_units", "agepoly_users"],
    "data": ["document_layout.xml"],
    "license": "AGPL-3",
}
