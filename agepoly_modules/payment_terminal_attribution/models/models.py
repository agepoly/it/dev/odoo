 #-*- coding: utf-8 -*-#
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

import datetime

#class AccountMoveLine(models.Model):
#    _inherit = 'account.move.line'
#    terminal_attrib_id = fields.Many2one('payment.terminal.attribution', 'Terminal Att', required=False)

class PaymentTerminalAttribution(models.Model):
    _name = 'payment.terminal.attribution'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char(required=True)

    ## these are the field fo identify the transactions
    # these are fields to generate the domain
    terminal_id = fields.Many2one('payment.terminal', 'Terminal', required=True)
    start_date = fields.Datetime(required=True)
    end_date = fields.Datetime(required=True)

    extra_reference = fields.Char(required=False)


    ## these are the field use to account the transactions
    mode = fields.Selection(
        [('internal', 'Interne'),
         ('external', 'Externe')],
        default='internal'
       )
    partner_id = fields.Many2one('res.partner', 'Responsible Entity')

    operating_unit_id = fields.Many2one('operating.unit', 'Operating Unit')
    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic Account', domain=['operating_unit_ids', 'in', operating_unit_id])

    trx_destination_account = fields.Many2one('account.account', 'Fund destination account')
    fee_destination_account = fields.Many2one('account.account', 'Fee destination account')

    detail_level = fields.Selection(
        [('batch', 'Batch'),
         ('per_trx', 'Per Transaction')],  # not implemented
        default='batch'
       )
          
    @api.constrains('start_date', 'end_date')
    def _check_expiration_date(self):
        if self.start_date >= self.end_date:
            raise ValidationError('End Date should be after Start Date')

    def _find_transactions(self):
        self.ensure_one()
        domain = [('account_id', '=', self.env.ref('payment_terminal_attribution.account_decomptes_acquereurs').id), ('reconciled', '=', False), ('parent_state', '=', 'posted')]

        domain += [('date', '>=', self.start_date), ('date', '<=', self.end_date)]

        if self.terminal_id.brand == 'six':
            domain += [('move_id.narration', 'ilike', self.terminal_id.terminal_id)]
        if self.extra_reference:
            domain += [('move_id.narration', 'ilike', self.extra_reference)]
        return self.env["account.move.line"].search(domain)

    # all amounts are signed here (positive when we earn money, negative when we loose)
    def merge_transactions(self):
        self.ensure_one()
        self.terminal_id.journal_id.action_open_reconcile()
        transactions = self._find_transactions()
        if len(transactions) == 0:
            return True
        for trx in transactions: #remove partial reconcile
            trx.remove_move_reconcile()
        fees = transactions._calculate_total_fees(self)
        gross = transactions._calculate_gross_total_amount(self)
        if gross == 0 and fees == 0:
            return
        net = gross + fees
        move_values = {
            'ref': "Attribution  %s (%s)" % (self.name, self.terminal_id.name),
            'journal_id': self.terminal_id.journal_id.id,
            'date': self.end_date,
            'line_ids': [(0, 0, {
                    'name': "Gross Amount for Attribution  %s (%s)" % (self.name, self.terminal_id.name),
                    'account_id': self.env.ref('payment_terminal_attribution.account_decomptes_acquereurs').id,
                    'debit': gross if gross > 0 else 0,
                    'credit': 0 if gross > 0 else -gross
                }),
                (0, 0, {
                    'name': "Fees for Attribution  %s (%s)" % (self.name, self.terminal_id.name),
                    'account_id': self.env.ref('payment_terminal_attribution.account_decomptes_acquereurs').id,
                    'debit': fees if fees > 0 else 0,
                    'credit': 0 if fees > 0 else -fees
                }),
                (0, 0, {
                    'name': "Gross %s_(%s)" % (self.name, self.terminal_id.name),
                    'account_id': self.env.ref('payment_terminal_attribution.partner_acquereurs').property_account_receivable_id.id if self.mode == 'internal' else self.trx_destination_account.id,
                    'partner_id': self.partner_id.id or self.env.ref('payment_terminal_attribution.partner_acquereurs').id,
                    'debit': 0 if gross > 0 else -gross,
                    'credit': gross if gross > 0 else 0
                }),
                (0, 0, {
                    'name': "Fee %s_(%s)" % (self.name, self.terminal_id.name),
                    'account_id': self.env.ref('payment_terminal_attribution.partner_acquereurs').property_account_receivable_id.id if self.mode == 'internal' else self.fee_destination_account.id,
                    'partner_id': self.partner_id.id or self.env.ref('payment_terminal_attribution.partner_acquereurs').id,
                    'debit': 0 if fees > 0 else -fees,
                    'credit': fees if fees > 0 else 0
                })]
        }
        move = self.env["account.move"].create(move_values)
        move.post()

        line_to_reconcile = transactions + move.line_ids.filtered(lambda l: l.account_id == self.env.ref('payment_terminal_attribution.account_decomptes_acquereurs'))
        line_to_reconcile.reconcile()

        if self.mode == 'internal' and self.trx_destination_account and self.fee_destination_account:
            # create final invoice
            final_move_values = {
                'ref': "Recette  %s (%s)" % (self.name, self.terminal_id.name),
                'payment_reference': "%s_(%s)" % (self.name, self.terminal_id.name),                
                'partner_id': self.partner_id.id or self.env.ref('payment_terminal_attribution.partner_acquereurs').id,
                'operating_unit_id': self.operating_unit_id.id,
                'move_type': 'out_invoice',
                #'journal_id': self.terminal_id.journal_id.id,
                'invoice_line_ids': [(0, 0, {
                        'name': "Recette %s (%s)" % (self.name, self.terminal_id.name),
                        'account_id': self.trx_destination_account.id,
                        'analytic_account_id': self.analytic_account_id.id,
                        'quantity': 1,
                        'price_unit': gross
                    }),
                    (0, 0, {
                        'name': "Frais %s (%s)" % (self.name, self.terminal_id.name),
                        'account_id': self.fee_destination_account.id,
                        'analytic_account_id': self.analytic_account_id.id,
                        'quantity': 1,
                        'price_unit': fees
                    })
                ]
            }
            final_move = self.env["account.move"].create(final_move_values)
            final_move.message_subscribe(partner_ids=self.operating_unit_id.tresorerie_group_id.users.mapped('partner_id.id'))
            final_move.message_post(
                body=_("Recette du terminal %s (Attribution %s) recu avant le %s. Merci de définir la TVA et la répartition des fonds.") % (self.terminal_id.name, self.name, datetime.datetime.now().strftime('%Y-%m-%d'))
            )

class PaymentTerminal(models.Model):
    _name = 'payment.terminal'

    name = fields.Char(required=True)
    terminal_id = fields.Char('ID Terminal')
    brand = fields.Selection(
       [('six', 'Six'),
        ('camipro', 'Camipro')],
        required=True
       )
    journal_id = fields.Many2one('account.journal', 'Accounting Journal', help="accounting bank journal where the trx are received")
    additional_terminal_info = fields.Char()

    _sql_constraints = [
        ('name_uniq', 'unique(type, mode)', 'The name should be Unique!'),
        ('id_uniq', 'unique(brand, terminal_id)', 'Two terminal of the same brand cannot have the same terminal ID !'),
        ]

class AccountMoveLine(models.Model):
    _inherit="account.move.line"

    def _calculate_total_fees(self, payment_terminal_attribution):
        fees = 0
        for trx in self:
                fees = fees + trx._calculate_fee(payment_terminal_attribution)
        return fees

    # should return signed amount of fee (-0.20 if we loose that amount)
    def _calculate_fee(self, payment_terminal_attribution):
        self.ensure_one()
        if self.statement_line_id and payment_terminal_attribution.terminal_id.brand == 'six' and 'COMM-' in self.statement_line_id.transaction_type:
            return -1 * self.balance
        return 0

    def _calculate_gross_total_amount(self, payment_terminal_attribution):
        amount = 0
        for trx in self:
                amount = amount + trx._calculate_gross_amount(payment_terminal_attribution)
        return amount

    # should return signed amount of transaction (-XXX for a reimbursement)
    def _calculate_gross_amount(self, payment_terminal_attribution):
        self.ensure_one()
        if self.statement_line_id and payment_terminal_attribution.terminal_id.brand == 'six' and 'TRX-' in self.statement_line_id.transaction_type:
            return -1 * self.balance
        return 0
