# -*- coding: utf-8 -*-

from odoo import api, fields, models

class AccountPaymentLineCreate(models.TransientModel):
    _inherit = 'account.payment.line.create'

    allow_already_in_payment = fields.Boolean(string="Allow Moves that are already in payment")

    def _prepare_move_line_domain(self):
    	domain = super(AccountMove, self)._prepare_move_line_domain(soft)

    	if not self.allow_already_in_payment

    	move_lines_ids = self.env["account.move.line"].search(
            [
                ("move_id.payment_state", "in", ("in_payment")),
            ]
        )
        if move_lines_ids:
            domain += [("id", "not in", move_lines_ids)]
        return domain