# -*- coding: utf-8 -*-
{
    'name': 'In Payment for Payment Order',
    'version': '14.0.1.0.0',
    'license': 'AGPL-3',
    'summary': """Use status in payment for invoices in an uploaded payment order'""",
    'description': """
    """,
    'author': 'Téo Goddet, AGEPoly',
    'category': 'l10n',
    'depends': ['account_payment_order'],
    'installable': True,
    'auto_install': False,
}
