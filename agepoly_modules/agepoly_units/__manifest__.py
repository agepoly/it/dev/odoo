# Copyright 2014-2018 'AGEPoly'
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl)

{
    "name": "AGEPoly Units Definition",
    "summary": "Defines AGEPoly Units and associated rights, groups, tier definition, access rules",
    "version": "14.1.1.0.1",
    "category": "Tools",
    "development_status": "Production/Stable",
    "author": "Téo Goddet, AGEPoly",
    "maintainer": "AGEPoly",
    "website": "agepoly.ch",
    "depends": ["base", "web", "base_setup", "agepoly_users"],
    "license": "AGPL-3",
    'data': [
        'views/operating_unit.xml',
    ],
}
