# -*- coding: utf-8 -*-

from odoo import api, models, fields

import re
import unicodedata

class OperatingUnit(models.Model):
    _inherit = 'operating.unit'
    _parent_name = 'parent_id'
    _parent_store = True

    ROLES = ["presidence", "tresorerie", "secretariat"]

    def _get_normalized_name(self):
        self.normalized_name = self._normalize_text(self.name)
        return self.normalized_name

    parent_id = fields.Many2one('operating.unit', string='Parent Operating Unit', index=True)
    parent_path = fields.Char(index=True)

    def _get_all_parents(self):
        self.all_parent_ids = self
        current = self
        while current.parent_id:
            self.all_parent_ids = self.all_parent_ids + current.parent_id
            current = current.parent_id
        return self.all_parent_ids
    all_parent_ids = fields.One2many('operating.unit', store=False, compute=_get_all_parents)

    child_ids = fields.One2many('operating.unit', 'parent_id', string='Childs Operating Unit')

    def _recursive_add_childs(self):
        for child in self.child_ids:
            self = self + child._recursive_add_childs()
        return self

    def _get_all_childs(self):
        childs = self.env["operating.unit"]
        self.all_child_ids = self._recursive_add_childs()
        return self.all_child_ids

    all_child_ids = fields.One2many('operating.unit', store=False, compute=_get_all_childs)

    partner_id = fields.Many2one(required=False)

    normalized_name = fields.Char(compute=_get_normalized_name, store=False)

    no_update_fields_ids = fields.Many2many(
        comodel_name="ir.model.fields",
        string="Champs avec mise a jour automatique bloqué"
    )

    cash_journal_id = fields.Many2one('account.journal') #, domain="[('company_id', '=', company_id), ('type', '=', 'cash')]")

    default_analytic_account_id = fields.Many2one('account.analytic.account', store=True, readonly=False, domain="[('id', 'in', analytic_account_ids)]")
    analytic_group_id = fields.Many2one('account.analytic.group')

    analytic_account_ids = fields.Many2many(
        comodel_name="account.analytic.account",
        string="Comptes Analytiques",
        relation="analytic_account_operating_unit_rel",
        column1="operating_unit_id",
        column2="analytic_account_id",
        store=True,
        compute="_compute_analytic_account_ids",
        inverse="_inverse_analytic_account_ids"
    )

    @api.depends("analytic_group_id")
    def _compute_analytic_account_ids(self):
        for rec in self:
            rec.analytic_account_ids = self.env["account.analytic.account"].search([('group_id.id', '=', rec.analytic_group_id.id)])

    def _inverse_analytic_account_ids(self):
        for rec in self:
            rec.analytic_account_ids.write({'group_id': rec.analytic_group_id.id})

    # this group should contains all unit members, ou.user_id are set to the users of this group
    main_group_id = fields.Many2one('res.groups', store=True, readonly=False, required=True)

    # this method and associated computed stored fields are use in rules were we need to allow
    # an action to all the units role_x + all the parents units role_x
    _depends_compute_allowed_user = ['{}_group_id.users'.format(role) for role in ROLES]
    @api.depends(*_depends_compute_allowed_user)
    def _compute_allowed_user(self):
        for ou in self:
            for role in self.ROLES:
                try:
                    self['all_{}_allowed_user_ids'.format(role)] = ou._get_all_parents().mapped('{}_group_id.users'.format(role))
                except:
                    continue

    presidence_group_id = fields.Many2one('res.groups', store=True, readonly=False)

    tresorerie_group_id = fields.Many2one('res.groups', store=True, readonly=False)
    all_tresorerie_allowed_user_ids = fields.Many2many('res.users', relation='operating_unit_tresorerie_allowed_user_rel', store=True, compute=_compute_allowed_user)

    secretariat_group_id = fields.Many2one('res.groups', store=True, readonly=False)
    all_secretariat_allowed_user_ids = fields.Many2many('res.users', relation='operating_unit_secretariat_allowed_user_rel', store=True, compute=_compute_allowed_user)

    disable_unit_validation = fields.Boolean(default=False)

    @api.model
    def create(self, values):
        OU = super(OperatingUnit, self).create(values)
        # we apply all the specificities after creation
        for ou in self:
            if ou.name and ou.code:
                pass  # ou.ensure_unit_updated()
        return OU

    def write(self, values):
        OU = super(OperatingUnit, self).write(values)
        # we ensure everything is setup as a normal unit
        for ou in self:
            if ou.name and ou.code:
                pass#ou.ensure_unit_updated()
        return OU

    def ensure_unit_updated(self):
        for rec in self:
            self._compute_analytic_account_ids()
            if rec.env.context.get("in_unit_update", False):
                return
            rec = rec.with_context(in_unit_update=True)
    
            partner = {
                'name': rec.name,
                'company_id': None,
                'is_company': True
            }
            rec.create_or_update_single_rel('partner_id', 'res.partner', partner)
            
            code = "1002.{}".format(rec.code)
            cash_account = {
                'name': "1002.{} - Cash {}".format(rec.code, rec.name),
                'code': code,
                'user_type_id': self.env.ref('account.data_account_type_current_assets').id,
                'currency_id': self.env.ref('base.CHF').id,
            }
            # cash account is linked to the journal, so more complex manipulation
            if not rec.cash_journal_id or not rec.cash_journal_id.default_account_id:
                cash_account_id = self.env['account.account'].search([('code', '=', code)], limit=1).id or False
                if not cash_account_id: 
                    cash_account_id = self.env['account.account'].create(cash_account).id
            else:
                #self.env['account.account'].browse(self.cash_journal_id.default_account_id.id).write(values)
                rec.cash_journal_id.default_account_id.write(cash_account)
                cash_account_id = rec.cash_journal_id.default_account_id.id
            
            cash_journal = {
                'name': "{} - Cash Journal".format(rec.name),
                'code': "CA.{}".format(rec.code),
                'default_account_id': cash_account_id,
                'operating_unit_id': rec.id,
                'type': 'cash',
                'sequence': 30, 
                'payment_debit_account_id': cash_account_id,
                'payment_credit_account_id': cash_account_id,
                'journal_group_ids': [(4, self.env.ref('agepoly_account_customizations.unit_cash_journal_group').id)],
                'suspense_account_id': self.env.ref('agepoly_account_customizations.cash_suspense_account').id,
                'profit_account_id': self.env.ref('agepoly_account_customizations.cash_profit').id,
                'loss_account_id': self.env.ref('agepoly_account_customizations.cash_loss').id,
                'outbound_payment_method_ids': [(4, self.env.ref('account.account_payment_method_manual_out').id)],
                'inbound_payment_method_ids': [(4, self.env.ref('account.account_payment_method_manual_in').id)],
            }
            rec.create_or_update_single_rel('cash_journal_id', 'account.journal', cash_journal)
            
            analytic_account_group = {
                'name': "{} - ALL ACCOUNTS".format(rec.code),
            }
            rec.create_or_update_single_rel('analytic_group_id', 'account.analytic.group', analytic_account_group)


            analytic_account = {
                'name': "{} - Main Analytic Account".format(rec.name),
                'code': "{}".format(rec.code),
            }
            rec.create_or_update_single_rel('default_analytic_account_id', 'account.analytic.account', analytic_account)
            # need to add/delete also in the list
            #self.write({'analytic_account_ids': [(4, self.default_analytic_account_id.id)]})
            rec.analytic_account_ids += rec.default_analytic_account_id
    
    
            self.flush()

    def _get(self, attr):
        return getattr(self, attr)

    def _set(self, attr, val):
        return setattr(self, attr, val)

    def create_or_update_single_rel(self, prop, obj_type, values, force=False):
        if prop in self.no_update_fields_ids and not force:
            pass
        elif self._get(prop):
            self.env[obj_type].browse(self._get(prop).id).write(values)
        else:
            Obj = self.env[obj_type].create(values)
            self._set(prop, Obj.id)

        return self._get(prop).id
        
    def delete_single_rel(self, prop, obj_type, force=False):
        if prop in self.no_update_fields_ids and not force:
            pass
        elif self._get(prop):
            Obj = self._get(prop)
            self._set(prop, False)
            Obj.unlink()

    def replace_single_rel(self, prop, obj_type, values, force=False):
        if prop in self.no_update_fields_ids and not force:
            pass
        elif self._get(prop):
            self.delete_single_rel(prop, obj_type)
            self.create_or_update_single_rel(prop, obj_type, values)

    def delete_single_rel(self, prop, obj_type, force=False):
        if prop in self.no_update_fields_ids and not force:
            pass
        elif self._get(prop):
            Obj = self._get(prop)
            self._set(prop, False)
            Obj.unlink()

    def add_to_many_rel(self, prop, obj_type, values, force=False):
        if prop in self.no_update_fields_ids and not force:
            pass
        ## TODO : use external ids for the next check
        elif not values.has('name'):
            raise UserError()
        elif not oneisTrue(values._get('name') == obj.name for obj in self._get(props)): # TODO fix sinytax
            Obj = self.env[obj_type].create(values)
        #    self._get(prop) += Obj # TODO fix wrong syntax

    def create_or_update_many_rel(self, prop, obj_type, values, force=False, old_name=False):
        if old_name:
            name = old_name
        elif values.has('name'):
            name = values._get('name')
        else:
            raise UserError()
        
        target_record = False
        for obj in self._get(props):
            if obj.name == name:
                target_record = obj
        
        if prop in self.no_update_fields_ids and not force:
            pass
        ## TODO : use external ids for the next check
        elif not target_record:
            target_record = self.env[obj_type].create(values)
        #    self._get(prop) += target_record  # TODO fix wrong syntax
        else:
            self.env[obj_type].browse(target_record.id).write(values)

    def delete_many_rel(self, prop, obj_type, name, force=False):
        target_record = False
        for obj in self._get(props):
            if obj.name == name:
                target_record = obj
        
        if prop in self.no_update_fields_ids and not force:
            pass
        ## TODO : use external ids for the next check
        elif target_record:
        #    self._get(prop) -= target_record  # TODO fix wrong syntax
            target_record.unlink()

    @staticmethod
    def _normalize_text(text):
        """
        Convert input text to its ASCII normalized version.
    
        :param text: The input string.
        :type text: String.
    
        :returns: The processed String.
        :rtype: String.
        """
        text = text.lower()

        try:
            text = unicode(text, 'utf-8')
        except (TypeError, NameError): # unicode is a default on python 3 
            pass
        text = unicodedata.normalize('NFD', text)
        text = text.encode('ascii', 'ignore')
        text = text.decode("utf-8")
    
        text = re.sub('[ ]+', '_', text)
        text = re.sub('[^0-9a-zA-Z_-]', '', text)
        return text