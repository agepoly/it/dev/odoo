# -*- coding: utf-8 -*-

from odoo import api, models, fields, _
from odoo.exceptions import UserError
import logging

_logger = logging.getLogger()


class User(models.Model):
    _inherit = 'res.users'
